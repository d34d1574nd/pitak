<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Соглашение</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
        <h1 class="mt-4">Соглашение
            <a href="/agreement/add" class="btn btn-primary" style="float: right;">Создать</a>
        </h1>
        <div class="container">
          <table id="users" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr style="text-align: center;">
                <th>№</th>
                <th>Текст</th>
                <th>Actions</th>
            </tr>
            </thead>
            <c:forEach items="${agreements}" var="agreement">
              <tr>
                <td>${agreement.id}</td>
                <td style="height:250px;position: relative;overflow: scroll;">${agreement.responseHtml}</td>
                <td width="15%">
                  <div class="d-inline">
                      <a href="/agreement/edit/${agreement.id}" class="btn btn-outline-info">Ред.</a>
                      <form action="${pageContext.request.contextPath}/agreements" method="post" style="display: contents;">
                        <input type="hidden" name="agreementId" value="${agreement.id}"/>
                        <input type="hidden" name="action" value="delete"/>
                        <button type="submit" class="btn btn-outline-danger">Удал.</button>
                      </form>
                  </div>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
  <script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>