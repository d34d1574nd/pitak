<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <title>Log in with your account</title>
      <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
      <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
      <link href="${contextPath}/resources/css/style.css" rel="stylesheet">
  </head>

  <body>
    <sec:authorize access="isAuthenticated()">
      <% response.sendRedirect("/"); %>
    </sec:authorize>
    <div class="container">
      <form method="POST" action="/login" class="form-signin">
        <h2 class="form-heading">Вход в систему</h2>
        <div>
          <input name="username" type="text" class="form-control" placeholder="Username" autofocus="true"
                 autofocus="true"/>
          <input name="password" type="password" class="form-control" placeholder="Password"/>
          <button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button>
        </div>
      </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  </body>
</html>
