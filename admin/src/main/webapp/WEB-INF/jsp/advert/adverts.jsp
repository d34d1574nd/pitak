<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Объявления</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
          <div style="display: flex; justify-content: space-between">
              <h1 class="mt-4">Объявления</h1>
              <a href="/advert/add" class="btn btn-outline-info" style="height: 40px; margin-top: 30px">Создать</a>
          </div>
        <div class="container">
          <table id="listAdverts" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr style="text-align: center;">
                <th>№</th>
                <th>Заголовок</th>
                <th>Сумма</th>
                <th>Активно</th>
                <th>Actions</th>
            </tr>
            </thead>
            <c:forEach items="${allAdverts}" var="advert">
              <tr>
                <td><a href="/adverts/show/${advert.id}" class="btn btn-outline-info">${advert.id}</a></td>
                <td>${advert.title}</td>
                <td>${advert.amountPayment}</td>
                <td>
                    <input id="checkAdvert" disabled="disabled" type="checkbox" class="form-control"
                    ${(advert.enabled) ? "checked" : ""} style="width: 10%; margin:auto;"/>
                </td>
                <td width="15%">
                  <div class="d-inline">
                      <a href="/advert/edit/${advert.id}" class="btn btn-outline-info">Ред.</a>
                      <c:if test="${not empty pageContext.request.userPrincipal}">
                          <form action="${pageContext.request.contextPath}/adverts" method="post" style="display: contents;">
                              <input type="hidden" name="advertId" value="${advert.id}"/>
                              <input type="hidden" name="action" value="delete"/>
                              <button type="submit" class="btn btn-outline-danger">Удал.</button>
                          </form>
                      </c:if>
                  </div>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
  <script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>