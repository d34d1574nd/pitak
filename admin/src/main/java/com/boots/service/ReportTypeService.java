package com.boots.service;

import com.boots.models.ReportTypeModel;

import java.util.List;

public interface ReportTypeService {
    List<ReportTypeModel> getAll();

    ReportTypeModel getById(Long reportTypeId);

    ReportTypeModel saveReportType(ReportTypeModel reportTypeModel);

    Boolean removeReportType(Long reportTypeId);
}
