package com.boots.service;

import com.boots.models.ReportAdvertModel;

import java.util.List;

public interface ReportAdvertService {
    List<ReportAdvertModel> getAll();

    ReportAdvertModel getById(Long reportAdvertId);

    Boolean solvedReportAdvert(Long reportAdvertId);
    Boolean blockedReportAdvert(Long reportAdvertId);
}
