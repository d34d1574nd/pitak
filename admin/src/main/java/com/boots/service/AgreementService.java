package com.boots.service;

import com.boots.entity.dictionary.AgreementSetting;
import com.boots.models.AgreementModel;

import java.util.List;

public interface AgreementService {
    List<AgreementSetting> listAgreements();
    AgreementModel getAgreementById(Long id);
    void removeAgreement(Long id);
    AgreementModel saveAgreement(AgreementModel agreement);
}
