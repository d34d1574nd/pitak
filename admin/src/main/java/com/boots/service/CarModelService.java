package com.boots.service;

import com.boots.models.CarModelModel;

import java.util.List;

public interface CarModelService {
    List<CarModelModel> getAll();

    CarModelModel getById(Long carModelId);

    CarModelModel saveCarModel(CarModelModel carModelModel);

    Boolean removeCarModel(Long carModelId);
}
