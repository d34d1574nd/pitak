package com.boots.service;

import com.boots.models.CarBrandModel;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CarBrandService {
    List<CarBrandModel> getAll();

    CarBrandModel getById(Long carBrandId);

    CarBrandModel saveCarBrand(CarBrandModel carBrandModel);

    Boolean removeCarBrand(Long carBrandId);
}
