package com.boots.service;

import com.boots.models.TypeServiceModel;

import java.util.List;

public interface TypeServiceService {
    List<TypeServiceModel> getAll();

    TypeServiceModel getById(Long typeServiceId);

    TypeServiceModel saveTypeService(TypeServiceModel typeServiceModel);

    Boolean removeTypeService(Long typeServiceId);
}
