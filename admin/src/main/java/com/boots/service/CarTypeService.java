package com.boots.service;

import com.boots.models.CarTypeModel;

import java.util.List;

public interface CarTypeService {
    List<CarTypeModel> getAll();

    CarTypeModel getById(Long carTypeId);

    CarTypeModel saveCarType(CarTypeModel carTypeModel);

    Boolean removeCarType(Long carTypeId);
}
