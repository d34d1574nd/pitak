package com.boots.service.impl;

import com.boots.entity.dictionary.CarBrand;
import com.boots.models.CarBrandModel;
import com.boots.repository.CarBrandRepository;
import com.boots.service.CarBrandService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarBrandServiceImpl implements CarBrandService {
    private final CarBrandRepository carBrandRepository;

    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    public List<CarBrandModel> getAll() {
        return carBrandRepository.getAllByRemovedDateIsNullOrderByName().stream().map(CarBrand::toModel).collect(Collectors.toList());
    }

    @Override
    public CarBrandModel getById(Long carBrandId){
        return carBrandRepository.getOne(carBrandId).toModel();
    }

    @Override
    public CarBrandModel saveCarBrand(CarBrandModel carBrandModel){
        CarBrand newCarBrand = new CarBrand();
        if(carBrandModel.getId()!=null) {
            newCarBrand = carBrandRepository.getOne(carBrandModel.getId());
            newCarBrand.setRemovedDate(carBrandModel.getRemovedDate() != null ? carBrandModel.getRemovedDate() : null);
        }
        newCarBrand.setName(carBrandModel.getName());
        newCarBrand = carBrandRepository.save(newCarBrand);
        return newCarBrand.toModel();
    }

    @Override
    public Boolean removeCarBrand(Long carBrandId) {
        try {
            CarBrand carBrand = carBrandRepository.getOne(carBrandId);
            if(carBrand!=null){
                carBrand.setRemovedDate(LocalDateTime.now());
                carBrand = carBrandRepository.save(carBrand);
            }
            return carBrand!=null;
        }catch (Exception ex){
            throw ex;
        }
    }
}
