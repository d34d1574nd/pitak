package com.boots.service.impl;

import com.boots.entity.*;
import com.boots.entity.dictionary.TypeService;
import com.boots.enums.AdvertStatus;
import com.boots.models.AppAdvertAttachmentModel;
import com.boots.models.AppAdvertModel;
import com.boots.models.AppFileModel;
import com.boots.repository.AdvertAttachmentRepository;
import com.boots.repository.AppAdvertRepository;
import com.boots.repository.TypeServiceRepository;
import com.boots.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AppAdvertServiceImpl {
    private final AppAdvertRepository appAdvertRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final TypeServiceRepository typeServiceRepository;
    private final AdvertAttachmentRepository advertAttachmentRepository;

    private final static Logger logger = LoggerFactory.getLogger(AppAdvertServiceImpl.class);

    public List<AppAdvert> getAllAdverts(){
        return appAdvertRepository.findAllAndRemoveDateIsNullByOrderByIdDesc();
    }

    public AppAdvertServiceImpl(AppAdvertRepository appAdvertRepository, UserRepository userRepository, UserService userService, TypeServiceRepository typeServiceRepository, AdvertAttachmentRepository advertAttachmentRepository) {
        this.appAdvertRepository = appAdvertRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.typeServiceRepository = typeServiceRepository;
        this.advertAttachmentRepository = advertAttachmentRepository;
    }

    public AppAdvertModel saveAdvert(AppAdvertModel appAdvertModel) {
        AppAdvertModel appAdvertModelSave = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<Role> listRole = (List<Role>) authentication.getAuthorities();

        boolean hasAdmin = listRole.stream().anyMatch(f->f.getName().equals(ERole.ROLE_ADMIN));
        try {
            AppAdvert appAdvert = appAdvertModel.getId() != null ? appAdvertRepository.getOne(appAdvertModel.getId()) : null;
            TypeService typeService = appAdvertModel.getTypeServiceId() != null ? typeServiceRepository.getOne(appAdvertModel.getTypeServiceId()) : null;

            if (appAdvert != null) {
                if(hasAdmin){
                    appAdvert.setTitle(appAdvertModel.getTitle());
                    appAdvert.setText(appAdvertModel.getText());
                    appAdvert.setTypeService(typeService);
                    appAdvert.setAmountPayment(appAdvertModel.getAmountPayment());
                    appAdvert.setFromPlace(appAdvertModel.getFromPlace());
                    appAdvert.setToPlace(appAdvertModel.getToPlace());
                    appAdvert.setRemoveDate(appAdvertModel.getRemoveDate());
                    if(appAdvertModel.getRemoveDate()!=null){
                        appAdvertModel.setEnabled(false);
                        appAdvert.setAdvertStatus(AdvertStatus.BLOCKED);
                    }
                }
                if (appAdvertModel.getEnabled() != null && appAdvertModel.getEnabled()) {
                    if(appAdvertModel.getCheckedTime() == null)
                        appAdvert.setCheckedTime(LocalDateTime.now());
                    appAdvert.setAdvertStatus(AdvertStatus.APPROVED);
                } else {
                    appAdvert.setCheckedTime(null);
                    appAdvert.setAdvertStatus(AdvertStatus.MODERATION);
                }
                appAdvert.setEnabled(appAdvertModel.getEnabled());
                appAdvertModelSave = appAdvertRepository.save(appAdvert).toModel();
            } else {
                appAdvertModelSave = createNewAdvert(appAdvertModel);
            }
        } catch (Exception ex) {
            logger.error(ex.toString());
        }
        return appAdvertModelSave;
    }

    @NotNull
    public AppAdvertModel createNewAdvert(AppAdvertModel appAdvert) {
        TypeService typeService = appAdvert.getTypeServiceId() != null ? typeServiceRepository.getOne(appAdvert.getTypeServiceId()) : null;
        AppAdvert newAdvert = new AppAdvert();
        newAdvert.setTitle(appAdvert.getTitle());
        newAdvert.setText(appAdvert.getText());
        newAdvert.setTypeService(typeService);
        newAdvert.setAmountPayment(appAdvert.getAmountPayment());
        newAdvert.setFromPlace(appAdvert.getFromPlace());
        newAdvert.setToPlace(appAdvert.getToPlace());
        newAdvert.setNumberOfSeat(appAdvert.getNumberOfSeat());
        appAdvert.setAdvertStatus(appAdvert.getAdvertStatus());
        if (appAdvert.getEnabled() != null && appAdvert.getEnabled()) {
            newAdvert.setEnabled(appAdvert.getEnabled());
            newAdvert.setCheckedTime(LocalDateTime.now());
        } else {
            newAdvert.setEnabled(false);
        }
        Optional<User> createdBy = userRepository.findById(appAdvert.getCreatedBy());
        if(createdBy.isPresent())
            newAdvert.setCreatedBy(createdBy.get());
        newAdvert = appAdvertRepository.save(newAdvert);
        return newAdvert.toModel();
    }

    public AppAdvertModel getById(Long advertId) {
        Optional<AppAdvert> appAdvertOptional = appAdvertRepository.findById(advertId);
        List<AppAdvertAttachment> files = advertAttachmentRepository.findAllByAdvert(appAdvertOptional.get());
        List<AppFile> appFiles = null;
        for (AppAdvertAttachment el: files
             ) {
            if(appFiles==null)
                appFiles = new ArrayList<>();
            appFiles.add(el.getAppFile());
        }
        System.out.println(appFiles);
        AppAdvertModel model = appAdvertOptional.get().toModel();
        if(appFiles!=null)
            model.setAppFiles(appFiles.stream().map(AppFile::toModel).collect(Collectors.toList()));
        return model;
    }

    public void removeAdvert(Long advertId) {
        AppAdvert advert = appAdvertRepository.getOne(advertId);
        if (advert != null){
            advert.setEnabled(false);
            advert.setRemoveDate(LocalDateTime.now());
            advert.setAdvertStatus(AdvertStatus.BLOCKED);
            appAdvertRepository.save(advert);
        }
    }
}
