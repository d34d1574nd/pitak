package com.boots.service.impl;

import com.boots.entity.dictionary.City;
import com.boots.models.CityModel;
import com.boots.repository.CityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CityServiceImpl {
    private final CityRepository cityRepository;

    private final Logger logger = LoggerFactory.getLogger(CityServiceImpl.class);

    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public List<City> listCities() {
        return cityRepository.getAllByRemovedDateIsNullOrderByName();
    }

    public CityModel getCityById(Long id) {
        return cityRepository.getOne(id).toModel();
    }

    public void removeCity(Long id) {
        try {
            City city = cityRepository.getOne(id);
            if(city!=null){
                city.setRemovedDate(LocalDateTime.now());
                cityRepository.save(city);
            }
        }catch (Exception ex){
            throw ex;
        }
    }

    public CityModel saveCity(CityModel cityModel) {
        CityModel model = null;
        try {
            City city = cityModel.getId() != null ? cityRepository.getOne(cityModel.getId()) : null;
            if (city != null) {
                city.setName(cityModel.getName());
                city.setCountry(cityModel.getCountry());
                city.setRemovedDate(cityModel.getRemovedDate() != null ? cityModel.getRemovedDate() : null);
            } else {
                city = new City();
                city.setName(cityModel.getName());
                city.setCountry(cityModel.getCountry());
            }
            model = cityRepository.save(city).toModel();
        } catch (Exception ex) {
            logger.error(ex.toString());
        }
        return model;
    }
}
