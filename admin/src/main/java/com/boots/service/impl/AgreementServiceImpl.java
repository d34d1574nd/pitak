package com.boots.service.impl;

import com.boots.entity.dictionary.AgreementSetting;
import com.boots.models.AgreementModel;
import com.boots.repository.AgreementSettingRepository;
import com.boots.service.AgreementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgreementServiceImpl implements AgreementService {
    private final AgreementSettingRepository agreementSettingRepository;

    public AgreementServiceImpl(AgreementSettingRepository agreementSettingRepository) {
        this.agreementSettingRepository = agreementSettingRepository;
    }

    private final Logger logger = LoggerFactory.getLogger(AgreementServiceImpl.class);

    public List<AgreementSetting> listAgreements() {
        return agreementSettingRepository.findAll();
    }

    public AgreementModel getAgreementById(Long id) {
        return agreementSettingRepository.getOne(id).toModel();
    }

    public void removeAgreement(Long id) {
        try {
            agreementSettingRepository.deleteById(id);
        }catch (Exception ex){
            throw ex;
        }
    }

    public AgreementModel saveAgreement(AgreementModel agreement) {
        AgreementModel agreementModel = null;
        try {
            AgreementSetting agreementSetting = agreement.getId() != null ? agreementSettingRepository.getOne(agreement.getId()) : null;
            if (agreementSetting != null) {
                agreementSetting.setResponseHtml(agreement.getResponseHtml());
            } else {
                agreementSetting = new AgreementSetting();
                agreementSetting.setResponseHtml(agreement.getResponseHtml());
            }
            agreementModel = agreementSettingRepository.save(agreementSetting).toModel();
        } catch (Exception ex) {
            logger.error(ex.toString());
        }
        return agreementModel;
    }
}