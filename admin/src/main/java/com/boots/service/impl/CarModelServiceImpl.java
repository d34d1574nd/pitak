package com.boots.service.impl;

import com.boots.entity.dictionary.CarBrand;
import com.boots.entity.dictionary.CarModel;
import com.boots.models.CarModelModel;
import com.boots.repository.CarBrandRepository;
import com.boots.repository.CarModelRepository;
import com.boots.service.CarModelService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarModelServiceImpl implements CarModelService {
    private final CarModelRepository carModelRepository;
    private final CarBrandRepository carBrandRepository;
    public CarModelServiceImpl(CarModelRepository carModelRepository, CarBrandRepository carBrandRepository) {
        this.carModelRepository = carModelRepository;
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    public List<CarModelModel> getAll() {
        return carModelRepository.getAllByRemovedDateIsNullOrderByName().stream().map(CarModel::toModel).collect(Collectors.toList());
    }

    @Override
    public CarModelModel getById(Long carModelId) {
        return carModelRepository.getOne(carModelId).toModel();
    }

    @Override
    public CarModelModel saveCarModel(CarModelModel carModelModel) {
        CarModel newCarModel = new CarModel();

        if(carModelModel.getId()!=null) {
            newCarModel = carModelRepository.getOne(carModelModel.getId());
        }
        if(carModelModel.getParentId()!=null){
            CarBrand carBrand = carBrandRepository.getOne(carModelModel.getParentId());
            newCarModel.setCarBrand(carBrand);
            newCarModel.setRemovedDate(carModelModel.getRemovedDate() != null ? carModelModel.getRemovedDate() : null);
        }
        
        newCarModel.setName(carModelModel.getName());
        newCarModel = carModelRepository.save(newCarModel);
        return newCarModel.toModel();
    }

    @Override
    public Boolean removeCarModel(Long carModelId) {
        try {
            CarModel carModel = carModelRepository.getOne(carModelId);
            if(carModel!=null){
                carModel.setRemovedDate(LocalDateTime.now());
                carModel = carModelRepository.save(carModel);
            }
            return carModel!=null;
        }catch (Exception ex){
            throw ex;
        }
    }
}
