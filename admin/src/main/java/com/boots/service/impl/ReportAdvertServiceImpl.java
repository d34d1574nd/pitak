package com.boots.service.impl;

import com.boots.entity.AppAdvert;
import com.boots.entity.ReportAdvert;
import com.boots.entity.dictionary.ReportType;
import com.boots.enums.AdvertStatus;
import com.boots.models.ReportAdvertModel;
import com.boots.repository.AppAdvertRepository;
import com.boots.repository.ReportAdvertRepository;
import com.boots.service.ReportAdvertService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportAdvertServiceImpl implements ReportAdvertService {
    private final ReportAdvertRepository reportadvertRepository;
    private final AppAdvertRepository appAdvertRepository;

    public ReportAdvertServiceImpl(ReportAdvertRepository reportadvertRepository, AppAdvertRepository appAdvertRepository) {
        this.reportadvertRepository = reportadvertRepository;
        this.appAdvertRepository = appAdvertRepository;
    }

    @Override
    public List<ReportAdvertModel> getAll() {
        return reportadvertRepository.findAll().stream()
                .sorted(Comparator.comparingLong(ReportAdvert::getId)
                        .reversed()).map(ReportAdvert::toModel).collect(Collectors.toList());
    }

    @Override
    public ReportAdvertModel getById(Long reportadvertId) {
        return reportadvertRepository.getOne(reportadvertId).toModel();
    }

    @Override
    public Boolean solvedReportAdvert(Long reportadvertId) {
        try {
            ReportAdvert reportadvert = reportadvertRepository.getOne(reportadvertId);
            if(reportadvert!=null){
                reportadvert.setSolved(true);
                reportadvert = reportadvertRepository.save(reportadvert);
            }
            return reportadvert!=null;
        }catch (Exception ex){
            throw ex;
        }
    }

    @Override
    public Boolean blockedReportAdvert(Long reportadvertId) {
        try {
            ReportAdvert reportadvert = reportadvertRepository.getOne(reportadvertId);
            AppAdvert appAdvert = appAdvertRepository.getOne(reportadvert.getAdvert().getId());
            if(reportadvert!=null && appAdvert!=null){
                reportadvert.setSolved(true);
                appAdvert.setAdvertStatus(AdvertStatus.BLOCKED);
                appAdvert.setCheckedTime(LocalDateTime.now());
                appAdvert.setEnabled(false);
                reportadvert = reportadvertRepository.save(reportadvert);
            }
            return reportadvert!=null;
        }catch (Exception ex){
            throw ex;
        }
    }
}
