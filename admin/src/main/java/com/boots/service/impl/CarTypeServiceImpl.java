package com.boots.service.impl;

import com.boots.entity.dictionary.CarType;
import com.boots.models.CarTypeModel;
import com.boots.repository.CarTypeRepository;
import com.boots.service.CarTypeService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarTypeServiceImpl implements CarTypeService {
    private final CarTypeRepository carTypeRepository;

    public CarTypeServiceImpl(CarTypeRepository carTypeRepository) {
        this.carTypeRepository = carTypeRepository;
    }

    @Override
    public List<CarTypeModel> getAll() {
        return carTypeRepository.getAllByRemovedDateIsNullOrderByName().stream().map(CarType::toModel).collect(Collectors.toList());
    }

    @Override
    public CarTypeModel getById(Long carTypeId) {
        return carTypeRepository.getOne(carTypeId).toModel();
    }

    @Override
    public CarTypeModel saveCarType(CarTypeModel carTypeModel) {
        CarType newCarType = new CarType();
        if(carTypeModel.getId()!=null) {
            newCarType = carTypeRepository.getOne(carTypeModel.getId());
            newCarType.setRemovedDate(carTypeModel.getRemovedDate() != null ? carTypeModel.getRemovedDate() : null);
        }
        newCarType.setName(carTypeModel.getName());
        newCarType = carTypeRepository.save(newCarType);
        return newCarType.toModel();
    }

    @Override
    public Boolean removeCarType(Long carTypeId) {
        try {
            CarType carType = carTypeRepository.getOne(carTypeId);
            if(carType!=null){
                carType.setRemovedDate(LocalDateTime.now());
                carType = carTypeRepository.save(carType);
            }
            return carType!=null;
        }catch (Exception ex){
            throw ex;
        }
    }
}
