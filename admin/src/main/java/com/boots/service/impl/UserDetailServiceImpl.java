package com.boots.service.impl;

import com.boots.entity.User;
import com.boots.enums.UserType;
import com.boots.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    public UserDetailServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<UserType> userTypes = Arrays.asList(UserType.ADMIN,UserType.MODERATOR);
        User user = userRepository.findByUsernameAndUserTypeIn(username,userTypes);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }
}
