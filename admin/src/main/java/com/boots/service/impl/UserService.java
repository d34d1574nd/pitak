package com.boots.service.impl;

import com.boots.entity.ERole;
import com.boots.entity.Role;
import com.boots.entity.User;
import com.boots.entity.dictionary.CarBrand;
import com.boots.entity.dictionary.CarModel;
import com.boots.entity.dictionary.CarType;
import com.boots.enums.UserType;
import com.boots.models.AppUserModel;
import com.boots.repository.*;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class UserService  {
    @PersistenceContext
    private EntityManager em;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;
    private final CarBrandRepository carBrandRepository;
    private final CarModelRepository carModelRepository;
    private final CarTypeRepository carTypeRepository;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder, CarBrandRepository carBrandRepository, CarModelRepository carModelRepository, CarTypeRepository carTypeRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.carBrandRepository = carBrandRepository;
        this.carModelRepository = carModelRepository;
        this.carTypeRepository = carTypeRepository;
    }

    public User findUserById(Long userId) {
        Optional<User> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new User());
    }

    public List<User> allUsers() {
        return userRepository.findAll();
    }

    public boolean saveUser(User user) {
        User userFromDB = userRepository.findByUsername(user.getUsername());

        if (userFromDB != null) {
            return false;
        }

        user.setRoles(Collections.singleton(new Role(1, ERole.ROLE_PASSENGER)));
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        userRepository.save(user);
        return true;
    }

    public boolean deleteUser(Long userId) {
        if (userRepository.findById(userId).isPresent()) {
            //userRepository.deleteById(userId);
            return true;
        }
        return false;
    }

    public List<User> usergtList(Long idMin) {
        return em.createQuery("SELECT u FROM User u WHERE u.id > :paramId", User.class)
                .setParameter("paramId", idMin).getResultList();
    }

    public List<User> getAppUsers() {
        List<User> usersList = null;
        try {
            usersList = userRepository.findAll();
        } catch (Exception ex){
            System.out.println(ex);
        }
        return usersList;
    }

    public void removeUser(Long userId) {
        User users = userRepository.getOne(userId);
        if (users != null){
            users.setEnabled(false);
            users.setRemoveDatetime(LocalDateTime.now());
            userRepository.save(users);
        }
    }

    public AppUserModel getAppUserByUsername(String username) {
        AppUserModel user = null;
        try {
            user = userRepository.findByUsername(username).toModel();
        } catch (Exception ex){
            System.out.println(ex);
        }
        return user;
    }

    public AppUserModel getAppUserById(Long userId) {
        AppUserModel appUserModel = null;
        try {
            appUserModel = userRepository.getOne(userId).toModel();
        } catch (Exception ex) {}
        return appUserModel;
    }

    public String getCurrentUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            return currentUserName;
        }
        return "";
    }

    public AppUserModel saveUser(AppUserModel user) {
        AppUserModel appUserModel = null;
        try {
            User users = user.getId() != null ? userRepository.getOne(user.getId()) : null;
            if (users != null) {
                users.setUsername(user.getUsername());
                users.setName(user.getName());
                users.setSurname(user.getSurname());
                users.setPatronymic(user.getPatronymic());
                users.setEmail(user.getEmail());
                users.setEnabled(user.getEnabled());
                if(user.getCarCommonModel()!=null){
                    CarBrand carBrand = carBrandRepository.getOne(user.getCarCommonModel().getCarBrand().getId());
                    CarModel carModel = carModelRepository.getOne(user.getCarCommonModel().getCarModel().getId());
                    CarType carType = carTypeRepository.getOne(user.getCarCommonModel().getCarType().getId());
                    if(carBrand!=null)
                        users.setCarBrand(carBrand);
                    if(carModel!=null)
                        users.setCarModel(carModel);
                    if(carType!=null)
                        users.setCarType(carType);
                    users.setCarNumber(user.getCarCommonModel().getCarNumber());
                    users.setCarryCapacity(user.getCarCommonModel().getCarryCapacity());
                }

                users.setRemoveDatetime(user.getRemoveDatetime());
                appUserModel = userRepository.save(users).toModel();
            } else {
                appUserModel = createNewUser(user);// пока что  не надо создавать пользваотеля
            }
        } catch (Exception ex) {
            throw ex;
        }
        return appUserModel;
    }

    @NotNull
    public AppUserModel createNewUser(AppUserModel users) {
        User saved = null;
        try{
            User user = new User();
            String accessToken = "sdfkj3kfw3fsk3hfhshfhs3hfh4sh4fhfhsfsfd";
            LocalDateTime localDateTime = LocalDateTime.now();
            user.setUsername(users.getUsername());
            user.setSurname(users.getSurname());
            user.setName(users.getName());
            user.setPatronymic(users.getPatronymic());
            user.setEmail(users.getEmail());
            user.setPassword(encoder.encode(users.getPassword()));
            user.setEditorId(users.getEditorId());
            user.setEnabled(users.getEnabled());
            user.setUserType(users.getUserType());
            user.setToken(accessToken);
            user.serCts(localDateTime);
            user.serUts(localDateTime);
            if(users.getUserType()!=null){
                Role role = null;
                if(users.getUserType().equals(UserType.DRIVER)){
                    role = roleRepository.findByName(ERole.ROLE_DRIVER);
                    Set<Role> roles = new HashSet<>();
                    roles.add(role);
                    user.setRoles(roles);
                } else if(users.getUserType().equals(UserType.PASSENGER)){
                    role = roleRepository.findByName(ERole.ROLE_PASSENGER);
                    Set<Role> roles = new HashSet<>();
                    roles.add(role);
                    user.setRoles(roles);
                }
            }
            saved = userRepository.save(user);
        } catch (Exception ex) {
            throw ex;
        }

        return saved.toModel();
    }

    public UserDetails getCurrentUser() {
        UserDetails userDetails = null;
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (!(auth instanceof AnonymousAuthenticationToken))
                userDetails = (UserDetails) auth.getPrincipal();
        } catch (Exception ex) {
            throw ex;
        }
        return userDetails;
    }
}