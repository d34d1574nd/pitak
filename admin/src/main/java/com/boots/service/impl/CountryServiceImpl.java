package com.boots.service.impl;

import com.boots.entity.dictionary.Country;
import com.boots.models.CountryModel;
import com.boots.repository.CountryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CountryServiceImpl {
    private final CountryRepository countryRepository;

    private final Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);

    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public List<Country> listCountries() {
        return countryRepository.getAllByRemovedDateIsNullOrderByName();
    }

    public CountryModel getCountryById(Long id) {
        return countryRepository.getOne(id).toModel();
    }

    public void removeCountry(Long id) {
        try {
            Country country = countryRepository.getOne(id);
            if(country!=null){
                country.setRemovedDate(LocalDateTime.now());
                countryRepository.save(country);
            }
        }catch (Exception ex){
            throw ex;
        }
    }

    public CountryModel saveCountry(CountryModel country) {
        CountryModel countryModel = null;
        try {
            Country country1 = country.getId() != null ? countryRepository.getOne(country.getId()) : null;
            if (country1 != null) {
                country1.setName(country.getName());
                country1.setRemovedDate(country.getRemovedDate() != null ? country.getRemovedDate() : null);
            } else {
                country1 = new Country();
                country1.setName(country.getName());
            }
            countryModel = countryRepository.save(country1).toModel();
        } catch (Exception ex) {
            logger.error(ex.toString());
        }
        return countryModel;
    }
}
