package com.boots.service.impl;

import com.boots.models.TypeServiceModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToTypeServiceModelConverter implements Converter<String, TypeServiceModel> {
    @Override
    public TypeServiceModel convert(String s) {
        TypeServiceModel typeServiceModel = new TypeServiceModel();
        String id = s.substring(20, 22);
        String name = s.substring(29, 51);
        typeServiceModel.setId(Long.parseLong(id));
        typeServiceModel.setName(name);
        typeServiceModel.setRemovedDate(null);

        return typeServiceModel;
    }
}