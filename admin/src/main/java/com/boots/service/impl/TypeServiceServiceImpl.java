package com.boots.service.impl;

import com.boots.entity.dictionary.TypeService;
import com.boots.models.TypeServiceModel;
import com.boots.repository.TypeServiceRepository;
import com.boots.service.TypeServiceService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeServiceServiceImpl implements TypeServiceService {
    private final TypeServiceRepository typeServiceRepository;

    public TypeServiceServiceImpl(TypeServiceRepository typeServiceRepository) {
        this.typeServiceRepository = typeServiceRepository;
    }

    @Override
    public List<TypeServiceModel> getAll() {
        return typeServiceRepository.getAllByRemovedDateIsNullOrderByName().stream().map(TypeService::toModel).collect(Collectors.toList());
    }

    @Override
    public TypeServiceModel getById(Long typeServiceId) {
        return typeServiceRepository.getOne(typeServiceId).toModel();
    }

    @Override
    public TypeServiceModel saveTypeService(TypeServiceModel typeServiceModel) {
        TypeService newTypeService = new TypeService();
        if(typeServiceModel.getId()!=null) {
            newTypeService = typeServiceRepository.getOne(typeServiceModel.getId());
            newTypeService.setRemovedDate(typeServiceModel.getRemovedDate() != null ? typeServiceModel.getRemovedDate() : null);
        }
        newTypeService.setName(typeServiceModel.getName());
        newTypeService = typeServiceRepository.save(newTypeService);
        return newTypeService.toModel();
    }

    @Override
    public Boolean removeTypeService(Long typeServiceId) {
        try {
            TypeService typeService = typeServiceRepository.getOne(typeServiceId);
            if(typeService!=null){
                typeService.setRemovedDate(LocalDateTime.now());
                typeService = typeServiceRepository.save(typeService);
            }
            return typeService!=null;
        }catch (Exception ex){
            throw ex;
        }
    }
}
