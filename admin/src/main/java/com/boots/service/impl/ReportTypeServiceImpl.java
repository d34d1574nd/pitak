package com.boots.service.impl;

import com.boots.entity.dictionary.ReportType;
import com.boots.models.ReportTypeModel;
import com.boots.repository.ReportTypeRepository;
import com.boots.service.ReportTypeService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportTypeServiceImpl implements ReportTypeService {
    private final ReportTypeRepository reportTypeRepository;

    public ReportTypeServiceImpl(ReportTypeRepository reportTypeRepository) {
        this.reportTypeRepository = reportTypeRepository;
    }

    @Override
    public List<ReportTypeModel> getAll() {
        return reportTypeRepository.getAllByRemovedDateIsNullOrderByName().stream().map(ReportType::toModel).collect(Collectors.toList());
    }

    @Override
    public ReportTypeModel getById(Long reportTypeId) {
        return reportTypeRepository.getOne(reportTypeId).toModel();
    }

    @Override
    public ReportTypeModel saveReportType(ReportTypeModel reportTypeModel) {
        ReportType newReportType = new ReportType();
        if(reportTypeModel.getId()!=null) {
            newReportType = reportTypeRepository.getOne(reportTypeModel.getId());
            newReportType.setRemovedDate(reportTypeModel.getRemovedDate() != null ? reportTypeModel.getRemovedDate() : null);
        }
        newReportType.setName(reportTypeModel.getName());
        newReportType = reportTypeRepository.save(newReportType);
        return newReportType.toModel();
    }

    @Override
    public Boolean removeReportType(Long reportTypeId) {
        try {
            ReportType reportType = reportTypeRepository.getOne(reportTypeId);
            if(reportType!=null){
                reportType.setRemovedDate(LocalDateTime.now());
                reportType = reportTypeRepository.save(reportType);
            }
            return reportType!=null;
        }catch (Exception ex){
            throw ex;
        }
    }
}
