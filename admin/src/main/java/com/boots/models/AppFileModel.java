package com.boots.models;

import com.boots.enums.FileType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppFileModel {
    private Long id;
    private String name;
    private String path;
    private FileType fileType;
    private LocalDateTime cts;
    private String url;

//    public void getUrl() {
//        Constants constants = new Constants();
//        if(path.contains("profile"))
//            return Constants.HOSTNAME + ":" + Constants.SERVER_PORT + Constants.PROFILE_IMG_PATH + "/" + this.name;
//        else if(path.contains("advert"))
//            return Constants.HOSTNAME + ":" + Constants.SERVER_PORT + Constants.ADVERT_IMG_PATH + "/" + this.name;
//        else if(path.contains("car"))
//            return Constants.HOSTNAME + ":" + Constants.SERVER_PORT + Constants.CAR_IMG_PATH + "/" + this.name;
//        else
//            return "Не удалось распознать тип файла для сохрананения";
//    }
}
