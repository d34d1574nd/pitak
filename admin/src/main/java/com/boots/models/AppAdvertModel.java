package com.boots.models;


import com.boots.entity.dictionary.TypeService;
import com.boots.enums.AdvertStatus;
import com.boots.util.DateTimeUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppAdvertModel {
    private Long id;
    @NotNull
    private String title;
    @NotNull
    private String text;
    @NotNull
    private Integer amountPayment;
    @NotNull
    private Integer numberOfSeat;
    @NotNull
    private String fromPlace;
    @NotNull
    private String toPlace;
    @NotNull
    private Long typeServiceId;
    @NotNull
    private TypeServiceModel typeService;
    private Long createdBy;
    private Boolean enabled;
    private String mobileNumber;
    private CarCommonModel carCommonModel;
    private List<MultipartFile> fileList;
    private List<AppFileModel> appFiles;
    private LocalDateTime cts;
    private LocalDateTime uts;
    private LocalDateTime removeDate;
    private String removeDateStr;
    private LocalDateTime checkedTime;
    @NotNull
    private LocalDateTime sendDateTime;
    private LocalDateTime arrivalDateTime;
    private String responseFcm;
    private AdvertStatus advertStatus;

    public String getRemoveDateTimeStr(){
        if(this.removeDate==null)
            return "";
        else
            return DateTimeUtil.convertDateTimeToString(this.removeDate);
    }

    public void setRemoveDateTimeStr(String removeDateStr){
        if(removeDateStr.isEmpty())
            this.removeDate = null;
        else
            this.removeDate = DateTimeUtil.convertStringToDateTime(removeDateStr);
    }
}
