package com.boots.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarAttachmentModel {
    @JsonIgnore
    private Long id;

    @JsonIgnore
    private CarCommonModel carCommonModel;

    private AppFileModel appFile;
}
