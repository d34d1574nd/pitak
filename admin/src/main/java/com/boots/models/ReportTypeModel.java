package com.boots.models;


import com.boots.util.DateTimeUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportTypeModel {
    private Long id;
    private String name;
    private LocalDateTime removedDate;

    public String getRemovedDateStr(){
        if(this.removedDate==null)
            return "";
        else
            return DateTimeUtil.convertDateTimeToString(this.removedDate);
    }

    public void setRemovedDateStr(String removeDateStr){
        if(removeDateStr.isEmpty())
            this.removedDate = null;
        else
            this.removedDate = DateTimeUtil.convertStringToDateTime(removeDateStr);
    }
}
