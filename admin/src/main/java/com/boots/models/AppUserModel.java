package com.boots.models;

import com.boots.entity.Role;
import com.boots.enums.UserType;
import com.boots.util.DateTimeUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppUserModel {
    private Long id;
    private String surname;
    private String name;
    private String patronymic;
    private String username;
    private String email;
    private String token;
    private String password;
    private UserType userType;
    private Set<Role> roles;
    private LocalDateTime cts;
    private LocalDateTime uts;
    private Long editorId;
    private LocalDateTime removeDatetime;
    private String removeDateTimeStr;
    private Boolean enabled;
    private AppFileModel profilePhoto;
    private CarCommonModel carCommonModel;

    public String getRemoveDateTimeStr(){
        if(this.removeDatetime==null)
            return "";
        else
            return DateTimeUtil.convertDateTimeToString(this.removeDatetime);
    }

    public void setRemoveDateTimeStr(String removeDateStr){
        if(removeDateStr.isEmpty())
            this.removeDatetime = null;
        else
            this.removeDatetime = DateTimeUtil.convertStringToDateTime(removeDateStr);
    }
}
