package com.boots.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PushNotificationModel {
    private String to;
    private Notification notification;

    @Getter
    @Setter
    @JsonIgnoreProperties
    public class Notification{
        private String title;
        private String body;
    }
}
