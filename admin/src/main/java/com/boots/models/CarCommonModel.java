package com.boots.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarCommonModel {
    private Long id;
    @NotNull
    private CarBrandModel carBrand;
    @NotNull
    private CarModelModel carModel;
    @NotNull
    private CarTypeModel carType;
    @NotNull
    private Integer carryCapacity;
    @NotNull
    private String carNumber;
    private Long userId;
    private LocalDateTime uts;
    private LocalDateTime cts;
    private LocalDateTime removeDatetime;
    private List<MultipartFile> fileList;
    private List<AppFileModel> carFiles;
}
