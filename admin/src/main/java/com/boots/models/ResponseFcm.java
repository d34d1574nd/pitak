package com.boots.models;

import lombok.Data;

@Data
public class ResponseFcm {
    private Long message_id;
}
