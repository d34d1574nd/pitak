package com.boots.models;


import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ReportAdvertModel {
    private Long id;
    private AppUserModel createdBy;
    private AppAdvertModel advert;
    private ReportTypeModel reportType;
    private String additionalDescription;
    private LocalDateTime cts;
    private Boolean solved;
}
