package com.boots.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Value("${upload.advert.img.path}")
    private String uploadPathAdvert;
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/adverts").setViewName("advert/adverts");
        registry.addViewController("/agreements").setViewName("agreements/agreements");
        registry.addViewController("/countries").setViewName("country/countries");
        registry.addViewController("/cities").setViewName("city/cities");
        registry.addViewController("/carbrands").setViewName("carbrand/carbrands");
        registry.addViewController("/carmodels").setViewName("carmodel/carmodels");
        registry.addViewController("/cartypes").setViewName("cartype/cartypes");
        registry.addViewController("/typeservices").setViewName("typeservice/typeservices");
        registry.addViewController("/reporttypes").setViewName("reporttype/reporttypes");
        registry.addViewController("/reportadvert").setViewName("reportadvert/reportadvert");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/img/adverts/**")
                //.addResourceLocations("file:/"+uploadPathAdvert + "/"); //todo for windows
                .addResourceLocations("file://"+uploadPathAdvert + "/");//todo for linux
    }
}
