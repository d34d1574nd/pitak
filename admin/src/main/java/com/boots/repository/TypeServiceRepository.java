package com.boots.repository;

import com.boots.entity.dictionary.TypeService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypeServiceRepository extends JpaRepository<TypeService,Long>, JpaSpecificationExecutor<TypeService> {
    List<TypeService> getAllByRemovedDateIsNullOrderByName();
}
