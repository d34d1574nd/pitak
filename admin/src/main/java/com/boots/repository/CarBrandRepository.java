package com.boots.repository;

import com.boots.entity.dictionary.CarBrand;
import com.boots.entity.dictionary.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarBrandRepository extends JpaRepository<CarBrand,Long>, JpaSpecificationExecutor<CarBrand> {
    List<CarBrand> getAllByRemovedDateIsNullOrderByName();
}
