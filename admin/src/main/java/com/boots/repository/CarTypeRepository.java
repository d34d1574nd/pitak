package com.boots.repository;

import com.boots.entity.dictionary.CarType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarTypeRepository extends JpaRepository<CarType,Long>, JpaSpecificationExecutor<CarType> {
    List<CarType> getAllByRemovedDateIsNullOrderByName();
}
