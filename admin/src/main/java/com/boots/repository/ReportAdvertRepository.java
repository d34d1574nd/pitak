package com.boots.repository;

import com.boots.entity.ReportAdvert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportAdvertRepository extends JpaRepository<ReportAdvert,Long>, JpaSpecificationExecutor<ReportAdvert> {
}
