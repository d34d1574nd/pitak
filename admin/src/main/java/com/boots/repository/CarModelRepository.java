package com.boots.repository;

import com.boots.entity.dictionary.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarModelRepository extends JpaRepository<CarModel,Long>, JpaSpecificationExecutor<CarModel> {
    List<CarModel> getAllByRemovedDateIsNullOrderByName();
}
