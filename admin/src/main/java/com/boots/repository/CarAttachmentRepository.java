package com.boots.repository;

import com.boots.entity.AppFile;
import com.boots.entity.Car;
import com.boots.entity.CarAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarAttachmentRepository extends JpaRepository<CarAttachment, Long>, JpaSpecificationExecutor<CarAttachment> {
    CarAttachment findFirstByCarAndAppFile(Car car, AppFile file);

    List<CarAttachment> findAllByCar(Car car);
}
