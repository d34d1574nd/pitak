package com.boots.repository;

import com.boots.entity.AppFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends JpaRepository<AppFile,Long>, JpaSpecificationExecutor<AppFile> {

}
