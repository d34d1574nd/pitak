package com.boots.repository;

import com.boots.entity.AppAdvert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppAdvertRepository extends JpaRepository<AppAdvert,Long>, JpaSpecificationExecutor<AppAdvert> {
    List<AppAdvert> findAllByIdIn(List<Long> advertId);
    List<AppAdvert> findAllAndRemoveDateIsNullByOrderByIdDesc();

}
