package com.boots.repository;

import com.boots.entity.AppAdvert;
import com.boots.entity.AppAdvertAttachment;
import com.boots.entity.AppFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdvertAttachmentRepository extends JpaRepository<AppAdvertAttachment, Long>, JpaSpecificationExecutor<AppAdvertAttachment> {
    AppAdvertAttachment findFirstByAdvertAndAppFile(AppAdvert advert, AppFile file);

    List<AppAdvertAttachment> findAllByAdvert(AppAdvert advert);
}
