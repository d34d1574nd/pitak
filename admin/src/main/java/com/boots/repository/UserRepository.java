package com.boots.repository;

import com.boots.entity.User;
import com.boots.enums.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByUsernameAndUserTypeIn(String username, List<UserType> userTypeList);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
