package com.boots.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateTimeUtil {
    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static String convertDateTimeToString(LocalDateTime localDateTime){
        return localDateTime.format(formatter);
    }

    public static LocalDateTime convertStringToDateTime(String localDateTimeStr){
        return LocalDateTime.parse(localDateTimeStr,formatter);
    }
}
