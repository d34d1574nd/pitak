package com.boots.controller;

import com.boots.entity.User;
import com.boots.models.AppUserModel;
import com.boots.service.CarBrandService;
import com.boots.service.CarModelService;
import com.boots.service.CarTypeService;
import com.boots.service.impl.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class AdminController {
    private final UserService userService;
    private final CarBrandService carBrandService;
    private final CarModelService carModelService;
    private final CarTypeService carTypeService;

    private final Logger logger = LoggerFactory.getLogger(AdminController.class);

    public AdminController( UserService userService, CarBrandService carBrandService, CarModelService carModelService, CarTypeService carTypeService) {
        this.userService = userService;
        this.carBrandService = carBrandService;
        this.carModelService = carModelService;
        this.carTypeService = carTypeService;
    }

    @GetMapping("/admin")
    public String userList(Model model) {
        model.addAttribute("allUsers", userService.getAppUsers());
        return "user/users";
    }

    @PostMapping("/admin")
    public String  deleteUser(@RequestParam(required = true, defaultValue = "" ) Long userId,
                              @RequestParam(required = true, defaultValue = "" ) String action,
                              Model model) {
        if (action.equals("delete")){
            userService.removeUser(userId);
        }

        return "redirect:/admin";
    }

    @GetMapping("/admin/add")
    public String showAddUserForm(Model model) {
        List<String> userType = new ArrayList<>(Arrays.asList("DRIVER", "PASSENGER"));
        model.addAttribute("create", true);
        model.addAttribute("userType", userType);
        model.addAttribute("carbrands", carBrandService.getAll());
        model.addAttribute("carmodels", carModelService.getAll());
        model.addAttribute("cartypes", carTypeService.getAll());

        return "user/userEdit";
    }

    @GetMapping("/admin/edit/{userId}")
    public String editUser(@PathVariable("userId") Long userId, Model model) {
        UserDetails userDetails = userService.getCurrentUser();
        Long editorId = (userDetails!=null) ? ((User) userDetails).getId() : null;
        model.addAttribute("create", false);
        model.addAttribute("carbrands",carBrandService.getAll());
        model.addAttribute("carmodels",carModelService.getAll());
        model.addAttribute("cartypes",carTypeService.getAll());
        model.addAttribute("editorId", editorId);
        model.addAttribute("user", userService.getAppUserById(userId));

        return "user/userEdit";
    }

    @PostMapping("/admin/save")
    public String saveUser(@ModelAttribute("userForm") AppUserModel user) {

        userService.saveUser(user);
        return "redirect:/admin";
    }

    @GetMapping("/admin/show/{userId}")
    public String gtUser(@PathVariable("userId") Long userId, Model model) {
        model.addAttribute("user", userService.getAppUserById(userId));

        return "user/userShow";
    }
}
