package com.boots.controller;

import com.boots.models.ReportAdvertModel;
import com.boots.service.ReportAdvertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class ReportAdvertController {
    private final ReportAdvertService reportAdvertService;

    private final Logger logger = LoggerFactory.getLogger(ReportAdvertController.class);

    public ReportAdvertController(ReportAdvertService reportAdvertService) {
        this.reportAdvertService = reportAdvertService;
    }

    @GetMapping("/reportadvert")
    public String getAll(Model model) {
        model.addAttribute("reportadverts", reportAdvertService.getAll());
        return "reportadvert/reportadvert";
    }

    @PostMapping("/reportadvert")
    public String  delete(@RequestParam(required = true, defaultValue = "" ) Long reportadvertId,
                                 @RequestParam(required = true, defaultValue = "" ) String action,
                                 Model model) {
        if (action.equals("solved")){
            reportAdvertService.solvedReportAdvert(reportadvertId);
        }
        if (action.equals("blocked")){
            reportAdvertService.blockedReportAdvert(reportadvertId);
        }
        return "redirect:/reportadvert";
    }
}
