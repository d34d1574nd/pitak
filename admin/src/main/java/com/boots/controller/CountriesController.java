package com.boots.controller;

import com.boots.models.CountryModel;
import com.boots.service.impl.CountryServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class CountriesController {
    @Autowired
    private CountryServiceImpl countryService;

    private final Logger logger = LoggerFactory.getLogger(CountriesController.class);

    @GetMapping("/countries")
    public String countryList(Model model) {
        model.addAttribute("countries", countryService.listCountries());
        return "country/countries";
    }

    @PostMapping("/countries")
    public String  deleteCountry(@RequestParam(required = true, defaultValue = "" ) Long countryId,
                              @RequestParam(required = true, defaultValue = "" ) String action,
                              Model model) {
        if (action.equals("delete")){
            countryService.removeCountry(countryId);
        }

        return "redirect:/countries";
    }

    @RequestMapping(value = "/countries/add", method = RequestMethod.GET)
    public String showAddCountryForm(Model model) {
        model.addAttribute("create", true);
        return "country/countryEdit";
    }

    @GetMapping("/countries/edit/{countryId}")
    public String countryUser(@PathVariable("countryId") Long countryId, Model model) {
        model.addAttribute("country", countryService.getCountryById(countryId));
        model.addAttribute("create", false);
        return "country/countryEdit";
    }

    @PostMapping("/countries/save")
    public String saveUser(@ModelAttribute("countryForm") CountryModel countryModel) {
        countryService.saveCountry(countryModel);
        return "redirect:/countries";
    }
}
