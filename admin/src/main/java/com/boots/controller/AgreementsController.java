package com.boots.controller;

import com.boots.models.AgreementModel;
import com.boots.service.AgreementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class AgreementsController {

    @Autowired
    private AgreementService agreementService;

    private final Logger logger = LoggerFactory.getLogger(AgreementsController.class);

    @GetMapping("/agreements")
    public String agreementList(Model model) {
        model.addAttribute("agreements", agreementService.listAgreements());
        return "agreements/agreements";
    }

    @PostMapping("/agreements")
    public String  deleteAgreement(@RequestParam(required = true, defaultValue = "" ) Long agreementId,
                                 @RequestParam(required = true, defaultValue = "" ) String action,
                                 Model model) {
        if (action.equals("delete")){
            agreementService.removeAgreement(agreementId);
        }

        return "redirect:/agreements";
    }

    @RequestMapping(value = "/agreement/add", method = RequestMethod.GET)
    public String addAgreementForm(Model model) {
        model.addAttribute("create", true);
        return "agreements/agreementEdit";
    }

    @GetMapping("/agreement/edit/{agreementId}")
    public String agreementEdit(@PathVariable("agreementId") Long agreementId, Model model) {
        model.addAttribute("agreement", agreementService.getAgreementById(agreementId));
        model.addAttribute("create", false);
        return "agreements/agreementEdit";
    }

    @PostMapping("/agreement/save")
    public String saveUser(@ModelAttribute("agreementForm") AgreementModel agreementModel) {
        agreementService.saveAgreement(agreementModel);
        return "redirect:/agreements";
    }
}
