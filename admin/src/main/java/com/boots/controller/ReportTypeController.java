package com.boots.controller;

import com.boots.models.ReportTypeModel;
import com.boots.service.ReportTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class ReportTypeController {
    private final ReportTypeService reportTypeService;

    private final Logger logger = LoggerFactory.getLogger(ReportTypeController.class);

    public ReportTypeController(ReportTypeService reportTypeService) {
        this.reportTypeService = reportTypeService;
    }

    @GetMapping("/reporttypes")
    public String getAll(Model model) {
        model.addAttribute("reporttypes", reportTypeService.getAll());
        return "reporttype/reporttypes";
    }

    @GetMapping("/reporttypes/edit/{reporttypeId}")
    public String edit(@PathVariable("reporttypeId") Long reportTypeId, Model model) {
        model.addAttribute("reporttype", reportTypeService.getById(reportTypeId));
        model.addAttribute("create", false);
        return "reporttype/reporttypeEdit";
    }

    @RequestMapping(value = "/reporttypes/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("create", true);
        return "reporttype/reporttypeEdit";
    }

    @PostMapping("/reporttypes/save")
    public String save(@ModelAttribute("reporttypeForm") ReportTypeModel reportTypeModel) {
        reportTypeService.saveReportType(reportTypeModel);
        return "redirect:/reporttypes";
    }

    @PostMapping("/reporttypes")
    public String  delete(@RequestParam(required = true, defaultValue = "" ) Long reporttypeId,
                                 @RequestParam(required = true, defaultValue = "" ) String action,
                                 Model model) {
        if (action.equals("delete")){
            reportTypeService.removeReportType(reporttypeId);
        }
        return "redirect:/reporttypes";
    }
}
