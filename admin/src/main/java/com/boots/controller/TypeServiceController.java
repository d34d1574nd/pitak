package com.boots.controller;

import com.boots.models.TypeServiceModel;
import com.boots.service.TypeServiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class TypeServiceController {
    private final TypeServiceService typeService;

    private final Logger logger = LoggerFactory.getLogger(TypeServiceController.class);

    public TypeServiceController(TypeServiceService typeService) {
        this.typeService = typeService;
    }

    @GetMapping("/typeservices")
    public String getAll(Model model) {
        model.addAttribute("typeservices", typeService.getAll());
        return "typeservice/typeservices";
    }

    @GetMapping("/typeservices/edit/{typeserviceId}")
    public String edit(@PathVariable("typeserviceId") Long typeServeiceId, Model model) {
        model.addAttribute("typeservice", typeService.getById(typeServeiceId));
        model.addAttribute("create", false);
        return "typeservice/typeserviceEdit";
    }

    @RequestMapping(value = "/typeservices/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("create", true);
        return "typeservice/typeserviceEdit";
    }

    @PostMapping("/typeservices/save")
    public String save(@ModelAttribute("typeserviceForm") TypeServiceModel typeServeiceModel) {
        typeService.saveTypeService(typeServeiceModel);
        return "redirect:/typeservices";
    }

    @PostMapping("/typeservices")
    public String  delete(@RequestParam(required = true, defaultValue = "" ) Long typeserviceId,
                                 @RequestParam(required = true, defaultValue = "" ) String action,
                                 Model model) {
        if (action.equals("delete")){
            typeService.removeTypeService(typeserviceId);
        }
        return "redirect:/typeservices";
    }
}
