package com.boots.controller;

import com.boots.models.CarModelModel;
import com.boots.service.CarBrandService;
import com.boots.service.CarModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class CarModelController {
    private final CarModelService carModelService;
    private final CarBrandService carBrandService;
    private final Logger logger = LoggerFactory.getLogger(CarBrandController.class);

    public CarModelController(CarModelService carModelService, CarBrandService carBrandService) {
        this.carModelService = carModelService;
        this.carBrandService = carBrandService;
    }

    @GetMapping("/carmodels")
    public String getAll(Model model) {
        model.addAttribute("carmodels", carModelService.getAll());
        return "carmodel/carmodels";
    }

    @GetMapping("/carmodels/edit/{carmodelId}")
    public String edit(@PathVariable("carmodelId") Long carModelId, Model model) {
        model.addAttribute("carmodel", carModelService.getById(carModelId));
        model.addAttribute("carbrands",carBrandService.getAll());
        model.addAttribute("create", false);
        return "carmodel/carmodelEdit";
    }

    @RequestMapping(value = "/carmodels/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("create", true);
        model.addAttribute("carbrands",carBrandService.getAll());
        return "carmodel/carmodelEdit";
    }

    @PostMapping("/carmodels/save")
    public String save(@ModelAttribute("carmodel") CarModelModel carModelModel) {
        carModelService.saveCarModel(carModelModel);
        return "redirect:/carmodels";
    }

    @PostMapping("/carmodels")
    public String  delete(@RequestParam(required = true, defaultValue = "" ) Long carmodelId,
                                 @RequestParam(required = true, defaultValue = "" ) String action,
                                 Model model) {
        if (action.equals("delete")){
            carModelService.removeCarModel(carmodelId);
        }
        return "redirect:/carmodels";
    }
}
