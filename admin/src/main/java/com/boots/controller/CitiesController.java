package com.boots.controller;

import com.boots.entity.dictionary.Country;
import com.boots.models.CityModel;
import com.boots.service.impl.CityServiceImpl;
import com.boots.service.impl.CountryServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class CitiesController {
    @Autowired
    private CityServiceImpl cityService;
    @Autowired
    private CountryServiceImpl countryService;

    private final Logger logger = LoggerFactory.getLogger(CitiesController.class);

    @GetMapping("/cities")
    public String cityList(Model model) {
        model.addAttribute("cities", cityService.listCities());
        return "city/cities";
    }

    @PostMapping("/cities")
    public String  deletecity(@RequestParam(required = true, defaultValue = "" ) Long cityId,
                              @RequestParam(required = true, defaultValue = "" ) String action,
                              Model model) {
        if (action.equals("delete")){
            cityService.removeCity(cityId);
        }
        return "redirect:/cities";
    }

    @RequestMapping(value = "/cities/add", method = RequestMethod.GET)
    public String showAddcityForm(Model model) {
        List<Country> countries = countryService.listCountries();
        model.addAttribute("countries", countries);
        model.addAttribute("create", true);
        return "city/cityEdit";
    }

    @GetMapping("/cities/edit/{cityId}")
    public String cityUser(@PathVariable("cityId") Long cityId, Model model) {
        model.addAttribute("city", cityService.getCityById(cityId));
        List<Country> countries = countryService.listCountries();
        model.addAttribute("countries", countries);
        model.addAttribute("create", false);
        return "city/cityEdit";
    }

    @PostMapping("/cities/save")
    public String saveUser(@ModelAttribute("cityForm") CityModel cityModel) {
        cityService.saveCity(cityModel);
        return "redirect:/cities";
    }
}
