package com.boots.controller;

import com.boots.entity.ERole;
import com.boots.enums.AdvertStatus;
import com.boots.enums.AdvertType;
import com.boots.models.AppAdvertModel;
import com.boots.service.TypeServiceService;
import com.boots.service.impl.AppAdvertServiceImpl;
import com.boots.service.impl.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AdvertController {
    @Autowired
    private final AppAdvertServiceImpl appAdvertService;
    private final TypeServiceService typeService;
    private final UserService userService;

    public AdvertController(AppAdvertServiceImpl appAdvertService, TypeServiceService typeService, UserService userService) {
        this.appAdvertService = appAdvertService;
        this.typeService = typeService;
        this.userService = userService;
    }

    private final Logger logger = LoggerFactory.getLogger(AdvertController.class);

    @GetMapping("/adverts")
    public String advertsList(Model model) {
        model.addAttribute("allAdverts", appAdvertService.getAllAdverts());
        return "advert/adverts";
    }

    @GetMapping("/advert/edit/{advertId}")
    public String editAdvert(@PathVariable("advertId") Long advertId, Model model) {
        System.out.println(appAdvertService.getById(advertId).getAppFiles());
        model.addAttribute("advert", appAdvertService.getById(advertId));
        model.addAttribute("create", false);
        model.addAttribute("createdUser", userService.getAppUsers());
        model.addAttribute("advertType", AdvertType.values());
        model.addAttribute("advertStatus", AdvertStatus.values());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean hasAdmin = authentication.getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals(ERole.ROLE_ADMIN.name()));

        return hasAdmin?"advert/advertEdit":"advert/advertEditByModer";
    }

    @RequestMapping(value = "/advert/add", method = RequestMethod.GET)
    public String showAddAdvertsForm(Model model) {
        model.addAttribute("create", true);
        model.addAttribute("advertType", AdvertType.values());
        model.addAttribute("advertStatus", AdvertStatus.values());
        model.addAttribute("typeService", typeService.getAll());
        model.addAttribute("createdUser", userService.getAppUsers());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean hasAdmin = authentication.getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals(ERole.ROLE_ADMIN.name()));

        return hasAdmin?"advert/advertEdit":"advert/advertEditByModer";
    }


    @PostMapping("/advert/save")
    public String saveAdvert(@ModelAttribute("advertForm") AppAdvertModel appAdvertModel) {
        appAdvertService.saveAdvert(appAdvertModel);
        return "redirect:/adverts";
    }

    @PostMapping("/adverts")
    public String  deleteAdvert(@RequestParam(required = true, defaultValue = "" ) Long advertId,
                              @RequestParam(required = true, defaultValue = "" ) String action,
                              Model model) {
        if (action.equals("delete")){
            appAdvertService.removeAdvert(advertId);
        }
        return "redirect:/adverts";
    }

    @GetMapping("/adverts/show/{advertId}")
    public String gtAdvert(@PathVariable("advertId") Long advertId, Model model) {
        model.addAttribute("advert", appAdvertService.getById(advertId));
        model.addAttribute("createdUser", userService.getAppUsers());
        model.addAttribute("advertType", AdvertType.values());
        return "advert/advertShow";
    }
}
