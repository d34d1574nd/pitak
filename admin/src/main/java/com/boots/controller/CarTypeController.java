package com.boots.controller;

import com.boots.models.CarBrandModel;
import com.boots.models.CarTypeModel;
import com.boots.service.CarBrandService;
import com.boots.service.CarTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class CarTypeController {
    private final CarTypeService carTypeService;

    private final Logger logger = LoggerFactory.getLogger(CarTypeController.class);

    public CarTypeController(CarTypeService carTypeService) {
        this.carTypeService = carTypeService;
    }

    @GetMapping("/cartypes")
    public String getAll(Model model) {
        model.addAttribute("cartypes", carTypeService.getAll());
        return "cartype/cartypes";
    }

    @GetMapping("/cartypes/edit/{cartypeId}")
    public String edit(@PathVariable("cartypeId") Long carTypeId, Model model) {
        model.addAttribute("cartype", carTypeService.getById(carTypeId));
        model.addAttribute("create", false);
        return "cartype/cartypeEdit";
    }

    @RequestMapping(value = "/cartypes/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("create", true);
        return "cartype/cartypeEdit";
    }

    @PostMapping("/cartypes/save")
    public String save(@ModelAttribute("cartypeForm") CarTypeModel carTypeModel) {
        carTypeService.saveCarType(carTypeModel);
        return "redirect:/cartypes";
    }

    @PostMapping("/cartypes")
    public String  delete(@RequestParam(required = true, defaultValue = "" ) Long cartypeId,
                                 @RequestParam(required = true, defaultValue = "" ) String action,
                                 Model model) {
        if (action.equals("delete")){
            carTypeService.removeCarType(cartypeId);
        }
        return "redirect:/cartypes";
    }
}
