package com.boots.controller;

import com.boots.models.CarBrandModel;
import com.boots.service.CarBrandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class CarBrandController {
    private final CarBrandService carBrandService;

    private final Logger logger = LoggerFactory.getLogger(CarBrandController.class);

    public CarBrandController(CarBrandService carBrandService) {
        this.carBrandService = carBrandService;
    }

    @GetMapping("/carbrands")
    public String getAll(Model model) {
        model.addAttribute("carbrands", carBrandService.getAll());
        return "carbrand/carbrands";
    }

    @GetMapping("/carbrands/edit/{carbrandId}")
    public String edit(@PathVariable("carbrandId") Long carBrandId, Model model) {
        model.addAttribute("carbrand", carBrandService.getById(carBrandId));
        model.addAttribute("create", false);
        return "carbrand/carbrandEdit";
    }

    @RequestMapping(value = "/carbrands/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("create", true);
        return "carbrand/carbrandEdit";
    }

    @PostMapping("/carbrands/save")
    public String save(@ModelAttribute("carbrandForm") CarBrandModel carBrandModel) {
        carBrandService.saveCarBrand(carBrandModel);
        
        return "redirect:/carbrands";
    }

    @PostMapping("/carbrands")
    public String  delete(@RequestParam(required = true, defaultValue = "" ) Long carbrandId,
                                 @RequestParam(required = true, defaultValue = "" ) String action,
                                 Model model) {
        if (action.equals("delete")){
            carBrandService.removeCarBrand(carbrandId);
        }

        return "redirect:/carbrands";
    }
}
