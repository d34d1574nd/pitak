package com.boots.entity;


import com.boots.models.AppAdvertAttachmentModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "APP_ADVERT_ATTACHMENT")
@Data
@NoArgsConstructor
public class AppAdvertAttachment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_ADVERT_SEQ")
    @SequenceGenerator(name = "APP_ADVERT_SEQ", sequenceName = "APP_ADVERT_SEQ", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FK_ADVERT_ID")
    private AppAdvert advert;

    @ManyToOne
    @JoinColumn(name = "FK_FILE_ID")
    private AppFile appFile;

    public AppAdvertAttachmentModel toModel() {
        return AppAdvertAttachmentModel.builder()
                .id(this.id)
                .advert(advert != null ? advert.toModel() : null)
                .appFile(appFile != null ? appFile.toModel() : null)
                .build();
    }
}
