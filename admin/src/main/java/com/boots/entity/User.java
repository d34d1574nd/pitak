package com.boots.entity;

import com.boots.entity.dictionary.CarBrand;
import com.boots.entity.dictionary.CarModel;
import com.boots.entity.dictionary.CarType;
import com.boots.enums.UserType;
import com.boots.models.AppUserModel;
import com.boots.models.CarCommonModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(	name = "users", 
		uniqueConstraints = { 
			@UniqueConstraint(columnNames = "username")
		})
public class User implements UserDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_USER_SEQ")
	@SequenceGenerator(name = "APP_USER_SEQ", sequenceName = "APP_USER_SEQ", allocationSize = 1)
	private Long id;

	@NotBlank
	@Size(max = 20)
	private String username;

	@Size(max = 50)
	@Email
	private String email;

	@Column(name = "token")
	private String token;

	@NotBlank
	@Size(max = 120)
	private String password;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(	name = "user_roles",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

	@Column(name = "SURNAME")
	private String surname;

	@Column(name = "NAME")
	private String name;

	@Column(name = "PATRONYMIC")
	private String patronymic;

	@Column(name = "TYPE")
	@Enumerated
	private UserType userType;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CAR_BRAND_ID", referencedColumnName = "id", nullable = true)
	private CarBrand carBrand;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CAR_MODEL_ID", referencedColumnName = "id", nullable = true)
	private CarModel carModel;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CAR_TYPE_ID", referencedColumnName = "id", nullable = true)
	private CarType carType;

	@Column(name = "CAR_NUMBER")
	private String carNumber;

	@Column(name = "CTS")
	private LocalDateTime cts;

	@Column(name = "UTS")
	private LocalDateTime uts;

	@Column(name="EDITOR_ID")
	private Long editorId;

	@Column(name = "REMOVE_DATETIME")
	private LocalDateTime removeDatetime;

	@Column(name = "enabled")
	private Boolean enabled;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "profile_photo_id")
	private AppFile profilePhoto;

	@Column(name = "CARRY_CAPACITY")
	private Integer carryCapacity;

	public AppUserModel toModel()  {
		return AppUserModel.builder()
				.id(this.id)
				.surname(this.surname)
				.name(this.name)
				.patronymic(this.patronymic)
				.username(this.username)
				.email(this.email)
				.token(this.token)
				.password(this.password)
				.userType(this.userType)
				.roles(this.roles)
				.cts(this.cts)
				.uts(this.uts)
				.editorId(this.editorId)
				.removeDatetime(this.removeDatetime)
				.enabled(this.enabled)
				.profilePhoto(this.profilePhoto!=null?this.profilePhoto.toModel():null)
				.build();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return getRoles();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void serCts(LocalDateTime localDateTime) {
		this.cts = localDateTime;
	}

	public void serUts(LocalDateTime localDateTime) {
		this.uts = localDateTime;
	}
}
