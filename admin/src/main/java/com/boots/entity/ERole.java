package com.boots.entity;

public enum ERole {
    ROLE_DRIVER,
    ROLE_PASSENGER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
