package com.boots.entity.dictionary;

import com.boots.models.CarBrandModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "DICT_CAR_BRAND")
@Data
public class CarBrand {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DICT_CAR_BRAND_SEQ")
    @SequenceGenerator(name = "DICT_CAR_BRAND_SEQ", sequenceName = "DICT_CAR_BRAND_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "REMOVED_DATE")
    private LocalDateTime removedDate;

    public CarBrandModel toModel() {
        return CarBrandModel.builder()
                .id(this.id)
                .name(this.name)
                .removedDate(this.removedDate!=null?this.removedDate:null)
                .build();
    }
}
