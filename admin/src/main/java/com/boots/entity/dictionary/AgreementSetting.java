package com.boots.entity.dictionary;

import com.boots.models.AgreementModel;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "AGREEMENT_SETTING")
@Data
public class AgreementSetting {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AGREEMENT_SETTING_SEQ")
    @SequenceGenerator(name = "AGREEMENT_SETTING_SEQ", sequenceName = "AGREEMENT_SETTING_SEQ", allocationSize = 1)
    private Long id;

    @Column(columnDefinition="TEXT")
    private String responseHtml;

    public AgreementModel toModel() {
        return AgreementModel.builder()
                .id(this.id)
                .responseHtml(this.responseHtml)
                .build();
    }
}
