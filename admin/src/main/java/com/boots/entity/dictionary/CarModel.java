package com.boots.entity.dictionary;

import com.boots.models.CarModelModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "DICT_CAR_MODEL")
@Data
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DICT_CAR_MODEL_SEQ")
    @SequenceGenerator(name = "DICT_CAR_MODEL_SEQ", sequenceName = "DICT_CAR_MODEL_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PARENT_ID")
    private CarBrand carBrand;

    @Column(name = "REMOVED_DATE")
    private LocalDateTime removedDate;

    public CarModelModel toModel() {
        return CarModelModel.builder()
                .id(this.id)
                .name(this.name)
                .parentId(this.carBrand!=null?carBrand.getId():null)
                .removedDate(this.removedDate!=null?this.removedDate:null)
                .build();
    }
}
