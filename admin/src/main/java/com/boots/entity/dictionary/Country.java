package com.boots.entity.dictionary;

import com.boots.models.CountryModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "DICT_COUNTRY", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COUNTRY_SEQ")
    @SequenceGenerator(name = "COUNTRY_SEQ", sequenceName = "COUNTRY_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "REMOVED_DATE")
    private LocalDateTime removedDate;

    public CountryModel toModel() {
        return CountryModel.builder()
                .id(this.id)
                .name(this.name)
                .removedDate(this.removedDate!=null?this.removedDate:null)
                .build();
    }
}
