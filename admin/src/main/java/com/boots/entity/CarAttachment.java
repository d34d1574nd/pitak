package com.boots.entity;


import com.boots.models.CarAttachmentModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "CAR_ATTACHMENT")
@Data
@NoArgsConstructor
public class CarAttachment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAR_SEQ")
    @SequenceGenerator(name = "CAR_SEQ", sequenceName = "CAR_SEQ", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FK_CAR_ID")
    private Car car;

    @ManyToOne
    @JoinColumn(name = "FK_FILE_ID")
    private AppFile appFile;

    public CarAttachmentModel toModel() {
        return CarAttachmentModel.builder()
                .id(this.id)
                .carCommonModel(car != null ? car.toModel() : null)
                .appFile(appFile != null ? appFile.toModel() : null)
                .build();
    }
}
