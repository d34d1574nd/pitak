package com.boots.entity;

import com.boots.entity.dictionary.CarBrand;
import com.boots.entity.dictionary.CarModel;
import com.boots.entity.dictionary.CarType;
import com.boots.models.CarCommonModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(	name = "CARS")
public class Car {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARS_SEQ")
	@SequenceGenerator(name = "CARS_SEQ", sequenceName = "CARS_SEQ", allocationSize = 1)
	private Long id;


	@OneToOne
	@JoinColumn(name = "CAR_BRAND_ID", referencedColumnName = "id", nullable = true)
	private CarBrand carBrand;

	@OneToOne
	@JoinColumn(name = "CAR_MODEL_ID", referencedColumnName = "id", nullable = true)
	private CarModel carModel;

	@OneToOne
	@JoinColumn(name = "CAR_TYPE_ID", referencedColumnName = "id", nullable = true)
	private CarType carType;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = true)
	private User user;

	@Column(name = "CAR_NUMBER")
	private String carNumber;

	@Column(name = "CARRY_CAPACITY")
	private Integer carryCapacity;

	@Column(name = "CTS")
	@CreationTimestamp
	private LocalDateTime cts;

	@Column(name = "UTS")
	@UpdateTimestamp
	private LocalDateTime uts;

	@Column(name = "REMOVE_DATETIME")
	private LocalDateTime removeDatetime;

	public CarCommonModel toModel()  {
		return CarCommonModel.builder()
				.id(this.id)
				.carBrand(this.carBrand!=null?this.carBrand.toModel():null)
				.carModel(this.carModel!=null?this.carModel.toModel():null)
				.carType(this.carType!=null?this.carType.toModel():null)
				.carNumber(this.carNumber)
				.userId(this.user!=null?this.user.getId():null)
				.carryCapacity(this.carryCapacity)
				.cts(this.cts)
				.uts(this.uts)
				.removeDatetime(this.removeDatetime)
				.build();
	}
}
