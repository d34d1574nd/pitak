package com.boots.enums;

public enum UserType {
    DRIVER,
    PASSENGER,
    MODERATOR,
    ADMIN
}
