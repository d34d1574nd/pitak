package com.boots.enums;

public enum AdvertType {
    PASSENGER,
    DRIVER,
    PACKAGE,
    SPECIAL_TECH,
    CARGO_TRANSPORTATION
}
