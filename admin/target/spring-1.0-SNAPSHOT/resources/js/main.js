$(document).ready(function() {
    $('#t_users').DataTable({
        "order": [[ 0, "desc" ]],
        stateSave: true
    });
    $('#listAdverts').DataTable({
        "order": [[ 0, "desc" ]],
        stateSave: true
    });
    $('#listReportAdvert').DataTable({
        "order": [[ 0, "desc" ]],
        stateSave: true
    });
    $('#listReportTypes').DataTable({
        "order": [[ 1, "asc" ]],
        stateSave: true
    });
    $('#listTypeServices').DataTable({
        "order": [[ 1, "asc" ]],
        stateSave: true
    });
    $('#listCountries').DataTable({
        "order": [[ 1, "asc" ]],
        stateSave: true
    });
    $('#listCities').DataTable({
        "order": [[ 2, "asc" ]],
        stateSave: true
    });
    $('#listCarTypes').DataTable({
        "order": [[ 1, "asc" ]],
        stateSave: true
    });
    $('#listCarBrands').DataTable({
        "order": [[ 1, "asc" ]],
        stateSave: true
    });
    $('#listCarModels').DataTable({
        "order": [[ 2, "asc" ]],
        stateSave: true
    });
    $('#userEnabled').change(function() {
        $('#userEnabled').val(this.checked);
    });
    $( "#removeDateTime" ).dblclick(function() {
        $("#removeDateTime").val("");
    });
    $("#removeDateTime").hover(function() {
        $(this).css('cursor','pointer').attr('title', 'Кликните 2 раза для очистки поля.');
    }, function() {
        $(this).css('cursor','auto');
    });
});