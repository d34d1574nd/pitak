<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Пользователи</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
        <h1 class="mt-4">Пользователь, ${user.name} ${user.surname} ${user.patronymic}.</h1>
        <div class="container">
          <div class="container py-2">
              <div class="row my-2">
                  <div class="col-lg-8 order-lg-1 personal-info">
                      <form role="form">
                        <input type="hidden" name="id" value="${user.id}" />
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Логин</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${user.username}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">ФИО</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${user.name} ${user.surname} ${user.patronymic}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Роль</label>
                              <div class="col-lg-9">
                                  <span class="form-control">
                                    <c:forEach items="${user.roles}" var="role">${role.name}</c:forEach>
                                  </span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Тип</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${user.userType}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Почта</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${user.email}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Активен</label>
                              <div class="col-lg-9">
                                  <input type="checkbox" class="form-control"
                                  ${(user.enabled) ? "checked" : ""} style="width: 5%;" disabled/>
                              </div>
                          </div>
                          <c:choose>
                              <c:when test="${(user.userType == 'DRIVER')}">
                                  <div class="form-group row">
                                      <label class="col-lg-3 col-form-label form-control-label">Марка</label>
                                      <div class="col-lg-9">
                                          <span class="form-control">${user.carCommonModel.carBrandModel.name}</span>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-lg-3 col-form-label form-control-label">Модель</label>
                                      <div class="col-lg-9">
                                          <span class="form-control">${user.carCommonModel.carModelModel.name}</span>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-lg-3 col-form-label form-control-label">Тип</label>
                                      <div class="col-lg-9">
                                          <span class="form-control">${user.carCommonModel.carTypeModel.name}</span>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-lg-3 col-form-label form-control-label">Гос.номер</label>
                                      <div class="col-lg-9">
                                          <span class="form-control">${user.carCommonModel.carNumber}</span>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-lg-3 col-form-label form-control-label">Грузоподъемность</label>
                                      <div class="col-lg-9">
                                          <span class="form-control">${user.carCommonModel.carryCapacity}</span>
                                      </div>
                                  </div>
                              </c:when>
                          </c:choose>

                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Дата удаления</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${user.removeDatetime}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Дата создания</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${user.cts}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Дата редактирования</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${user.uts}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <div class="col-lg-9 ml-auto text-left">
                                  <a href="/admin" class="btn btn-outline-secondary" >Назад</a>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
  <script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>