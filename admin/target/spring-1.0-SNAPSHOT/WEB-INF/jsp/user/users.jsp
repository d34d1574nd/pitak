<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Пользователи</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
          <div style="display: flex; justify-content: space-between">
              <h1 class="mt-4">Пользователи</h1>
              <a href="/admin/add" class="btn btn-outline-info" style="height: 40px; margin-top: 30px">Создать</a>
          </div>
        <div class="container">
          <table id="t_users" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr style="text-align: center;">
                <th>№</th>
                <th>Логин</th>
                <th>ФИО</th>
                <th>Роль</th>
                <th>Почта</th>
                <th>Actions</th>
            </tr>
            </thead>
            <c:forEach items="${allUsers}" var="user">
              <tr>
                <td><a href="/admin/show/${user.id}" class="btn btn-outline-info">${user.id}</a></td>
                <td>${user.username}</td>
                <td>${user.name} ${user.surname} ${user.patronymic}</td>
                <td>
                  <c:forEach items="${user.roles}" var="role">${role.name}</c:forEach>
                </td>
                <td>${user.email}</td>
                <td width="15%">
                  <div class="d-inline">
                      <a href="/admin/edit/${user.id}" class="btn btn-outline-info">Ред.</a>
                      <form action="${pageContext.request.contextPath}/admin" method="post" style="display: contents;">
                        <input type="hidden" name="userId" value="${user.id}"/>
                        <input type="hidden" name="action" value="delete"/>
                        <button type="submit" class="btn btn-outline-danger">Удал.</button>
                      </form>
                  </div>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
  <script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>