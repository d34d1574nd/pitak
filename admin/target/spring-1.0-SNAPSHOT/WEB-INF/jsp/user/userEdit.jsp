<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Пользователи</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
<div class="d-flex" id="wrapper">
    <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
        <div class="container-fluid">
            <c:choose>
                <c:when test="${(create == true)}">
                    <h1 class="mt-4">Создание пользователя.</h1>
                </c:when>
                <c:otherwise>
                    <h1 class="mt-4">Редактирование пользователя, ${user.name} ${user.surname}.</h1>
                </c:otherwise>
            </c:choose>
            <div class="container">
                <div class="container py-2">
                    <div class="row my-2">
                        <div class="col-lg-8 order-lg-1 personal-info">
                            <form role="form" modelAttribute="userForm" action="${pageContext.request.contextPath}/admin/save" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="${user.id}" />
                                <input type="hidden" name="editorId" value="${editorId}" />
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Логин</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="username" type="text" value="${user.username}"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Имя</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="name" type="text" value="${user.name}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Фамилия</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="surname" type="text" value="${user.surname}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Отчество</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="patronymic" type="text" value="${user.patronymic}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Почта</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="email" type="email" value="${user.email}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Активен</label>
                                    <div class="col-lg-9">
                                        <input id="userEnabled" type="checkbox" name="enabled" class="form-control"
                                        ${(user.enabled == true) ? 'checked' : ''} style="width: 5%;"/>
                                    </div>
                                </div>
                                <c:choose>
                                    <c:when test="${(create == true)}">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label form-control-label">Тип</label>
                                            <div class="col-lg-9">
                                                <select name="userType" class="form-control">
                                                    <option value="">Тип пользователя</option>
                                                    <c:forEach items="${userType}" var="type">
                                                        <option value="${type}">${type}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label form-control-label">Тип</label>
                                            <div class="col-lg-9">
                                                <input class="form-control" name="userType" type="text" value="${user.userType}" readonly/>
                                            </div>
                                        </div>
                                    </c:otherwise>
                                </c:choose>

                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Марка</label>
                                    <div class="col-lg-9">
                                        <select name="carCommonModel.carBrandId" class="form-control">
                                            <option value="null" selected disabled
                                            >Марка авто</option>
                                            <c:forEach items="${carbrands}" var="carbrand">
                                                <option value="${carbrand.id}"
                                                    ${(carbrand.equals(user.carCommonModel.carBrandModel)) ? "selected":""}
                                                >${carbrand.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Модель</label>
                                    <div class="col-lg-9">
                                        <select name="carCommonModel.carModelId" class="form-control">
                                            <option value="null" selected disabled
                                            >Модель авто</option>
                                            <c:forEach items="${carmodels}" var="carmodel">
                                                <option value="${carmodel.id}"
                                                    ${(carmodel.equals(user.carCommonModel.carModelModel)) ? "selected":""}
                                                >${carmodel.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Тип машины</label>
                                    <div class="col-lg-9">
                                        <select name="carCommonModel.carTypeId" class="form-control">
                                            <option value="null" selected disabled
                                            >Тип авто</option>
                                            <c:forEach items="${cartypes}" var="cartype">
                                                <option value="${cartype.id}"
                                                    ${(cartype.equals(user.carCommonModel.carTypeModel)) ? "selected":""}
                                                >${cartype.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Гос. номер</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" placeholder="Гос. номер авто" name="carCommonModel.carNumber" type="text" value="${user.carCommonModel.carNumber}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Грузоподъемность</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" placeholder="Грузоподъемоность" name="carCommonModel.carryCapacity" type="number" value="${user.carCommonModel.carryCapacity}" />
                                    </div>
                                </div>


                                <c:choose>
                                    <c:when test="${(create == true)}">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label form-control-label">Пароль</label>
                                            <div class="col-lg-9">
                                                <input class="form-control" name="password" type="password"
                                                       value="${user.password}"/>
                                            </div>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${(create == true)}">
                                    </c:when>
                                    <c:otherwise>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label form-control-label">Дата удаления</label>
                                            <div class="col-lg-9">
                                                <input id="removeDateTime" class="form-control" name="removeDateTimeStr" type="text"
                                                       value="${user.removeDateTimeStr}" alt="Кликните 2 раза для очистки поля" readonly/>
                                            </div>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                                <div class="form-group row">
                                    <div class="col-lg-9 ml-auto text-right">
                                        <a href="/admin" class="btn btn-outline-secondary" >Отмена</a>
                                        <input type="submit" class="btn btn-primary" value="Сохранить" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
<script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>