<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

${fn:substring(myVariable, 0, 5)}
<div class="bg-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading">Pitak.kg<h4>${pageContext.request.userPrincipal.name}</h4></div>
    <div class="list-group list-group-flush">
        <a href="/" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/index.jsp') ? 'active' : 'bg-light'}">Главная</a>
        <sec:authorize url="/admin">
        </sec:authorize>
        <c:if test="${not empty pageContext.request.userPrincipal}">
            <c:if test="${pageContext.request.isUserInRole('ADMIN')}">
                <a href="/admin" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/user') ? 'active' : 'bg-light'}">Пользователи</a>

            </c:if>
        </c:if>
        <a href="/adverts" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/advert') ? 'active' : 'bg-light'}">Объявления</a>
        <a href="/reportadvert" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/reportadvert') ? 'active' : 'bg-light'}">Список жалоб</a>
        <c:if test="${not empty pageContext.request.userPrincipal}">
            <c:if test="${pageContext.request.isUserInRole('ADMIN')}">
                <a href="/agreements" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/agreements') ? 'active' : 'bg-light'}">Соглашение</a>
                <a href="/countries" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/countries') ? 'active' : 'bg-light'}">Страны</a>
                <a href="/cities" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/cities') ? 'active' : 'bg-light'}">Города</a>
                <a href="/carbrands" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/carbrands') ? 'active' : 'bg-light'}">Марки машин</a>
                <a href="/carmodels" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/carmodels') ? 'active' : 'bg-light'}">Модель машин</a>
                <a href="/cartypes" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/cartypes') ? 'active' : 'bg-light'}">Тип транспорта</a>
                <a href="/typeservices" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/typeservices') ? 'active' : 'bg-light'}">Тип услуг</a>
                <a href="/reporttypes" class="list-group-item list-group-item-action ${fn:contains(pageContext.request.requestURI, '/reporttypes') ? 'active' : 'bg-light'}">Тип жалобы</a>
            </c:if>
        </c:if>
        <sec:authorize access="isAuthenticated()">
            <a href="/logout" class="list-group-item list-group-item-action bg-light">Выйти</a>
        </sec:authorize>
    </div>
</div>