<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Объявление</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 80%;
            margin: auto;
        }
    </style>
</head>
<body>
<div class="d-flex" id="wrapper">
    <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
        <div class="container-fluid">
            <c:choose>
                <c:when test="${(create == true)}">
                    <h1 class="mt-4">Создание объявления.</h1>
                </c:when>
                <c:otherwise>
                    <h1 class="mt-4">Редактирование объявления, ${advert.title}.</h1>
                </c:otherwise>
            </c:choose>
            <div class="container">
                <div class="container py-2">
                    <div class="row my-2">
                        <div class="col-lg-8 order-lg-1 personal-info">
                            <c:if test="${advert.appFiles.size()!=0}">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                        <c:forEach items="${advert.appFiles}" var="item" varStatus="loop">
                                            <c:choose>
                                                <c:when test="${loop.index=='0'}">
                                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                                </c:when>
                                                <c:otherwise>
                                                    <li data-target="#myCarousel" data-slide-to="${loop.index}"></li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach >

                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <c:forEach items="${advert.appFiles}" var="item" varStatus="loop">
                                            <c:choose>
                                                <c:when test="${loop.index=='0'}">
                                                    <div class="item active">
                                                        <img src="/img/adverts/${item.name}" alt="advert photo" width="400" height="285">
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="item">
                                                        <img src="/img/adverts/${item.name}" alt="advert photo" width="400" height="285">
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <br>
                                <br>
                            </c:if>
                            <%--@elvariable id="advertForm" type=""--%>
                            <form:form role="form" modelAttribute="advertForm" enctype="multipart/form-data" action="${pageContext.request.contextPath}/advert/save" method="post">
                                <input type="hidden" name="id" value="${advert.id}" />
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Название</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="title" type="text" value="${advert.title}"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Описание</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="text" type="text" value="${advert.text}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Сумма</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="amountPayment" type="text" value="${advert.amountPayment}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Количество мест</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="numberOfSeat" type="text" value="${advert.numberOfSeat}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Точка отправки</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="fromPlace" type="text" value="${advert.fromPlace}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Пункт назначения</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="toPlace" type="text" value="${advert.toPlace}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Активно</label>
                                    <div class="col-lg-9">
                                        <input id="userEnabled" type="checkbox" name="enabled" class="form-control"
                                            ${(advert.enabled == true) ? 'checked' : ''} style="width: 5%;"/>
                                    </div>
                                </div>
                                <c:choose>
                                    <c:when test="${(create == true)}">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label form-control-label">Тип</label>
                                            <div class="col-lg-9">
                                                <select name="typeServiceId" id="typeService" class="form-control">
                                                    <option value="Тип транспорта">Тип транспорта</option>
                                                    <c:forEach items="${typeService}" var="type">
                                                        <option id="${type}" name="${type}" value="${type.id}">${type.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label form-control-label">Статус</label>
                                            <div class="col-lg-9">
                                                <select name="advertStatus" class="form-control">
                                                    <c:forEach items="${advertStatus}" var="status">
                                                        <option value="${status}">${status.desc}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label form-control-label">Тип</label>
                                            <div class="col-lg-9">
                                                <input name="typeServiceId" type="hidden" value="${advert.typeService.id}"/>
                                                <input class="form-control" type="text" value="${advert.typeService.name}" readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label form-control-label">Статус</label>
                                            <div class="col-lg-9">
                                                <select name="advertStatus" class="form-control" readonly disabled>
                                                    <option value=""></option>
                                                    <c:forEach items="${advertStatus}" var="status">
                                                        <option value="${status}"
                                                            ${(status.equals(advert.advertStatus)) ? "selected":""}
                                                        >${status.desc}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Кем создано</label>
                                    <div class="col-lg-9">
                                        <select name="createdBy" class="form-control" ${(create == true) ? "" : "disabled"}>
                                            <option value="Пользователь">Пользователь</option>
                                            <c:forEach items="${createdUser}" var="user">
                                                <option value="${user.id}"
                                                    ${(user.id.equals(advert.createdBy)) ? "selected":""}
                                                >${user.name} ${user.surname}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <c:choose>
                                    <c:when test="${(create == true)}">
                                    </c:when>
                                    <c:otherwise>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label form-control-label">Дата удаления</label>
                                            <div class="col-lg-9">
                                                <input id="removeDateTime" class="form-control" name="removeDateTimeStr" type="text"
                                                       value="${advert.removeDateTimeStr}" alt="Кликните 2 раза для очистки поля" readonly/>
                                            </div>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                                <div class="form-group row">
                                    <div class="col-lg-9 ml-auto text-right">
                                        <a href="/adverts" class="btn btn-outline-secondary" >Отмена</a>
                                        <input type="submit" class="btn btn-primary" value="Сохранить" />
                                    </div>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
<script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>