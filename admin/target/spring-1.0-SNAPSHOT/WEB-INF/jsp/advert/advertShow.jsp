<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Объявление</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
        <h1 class="mt-4">Объявление, ${advert.title}.</h1>
        <div class="container">
          <div class="container py-2">
              <div class="row my-2">
                  <div class="col-lg-8 order-lg-1 personal-info">
                      <form role="form">
                        <input type="hidden" name="id" value="${advert.id}" />
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Заголовок</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${advert.title}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Описание</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${advert.text}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Тип</label>
                              <div class="col-lg-9">
                                  <input class="form-control" name="typeService" type="text" value="${advert.typeService.name}" readonly/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Статус</label>
                              <div class="col-lg-9">
                                  <input class="form-control" name="advertStatus" type="text" value="${advert.advertStatus.desc}" readonly/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Активно</label>
                              <div class="col-lg-9">
                                  <input type="checkbox" class="form-control"
                                  ${(advert.enabled) ? "checked" : ""} style="width: 5%;" disabled/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Откуда</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${advert.fromPlace}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Куда</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${advert.toPlace}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Колличество мест</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${advert.numberOfSeat}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Создатель</label>
                              <div class="col-lg-9">
                                  <select class="form-control" disabled>
                                        <option value=""></option>
                                      <c:forEach items="${createdUser}" var="user">
                                        <option value="${user.id}"
                                        ${(user.id.equals(advert.createdBy)) ? "selected":""}
                                        >${user.name} ${user.surname}</option>
                                      </c:forEach>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Дата проверки</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${advert.checkedTime}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Дата удаления</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${advert.removeDate}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Дата создания</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${advert.cts}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Дата редактирования</label>
                              <div class="col-lg-9">
                                  <span class="form-control">${advert.uts}</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <div class="col-lg-9 ml-auto text-left">
                                  <a href="/adverts" class="btn btn-outline-secondary" >Назад</a>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
  <script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>