<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Страна</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
      <c:choose>
        <c:when test="${(create == true)}">
            <h1 class="mt-4">Создание.</h1>
        </c:when>
        <c:otherwise>
            <h1 class="mt-4">Редактирование.</h1>
        </c:otherwise>
      </c:choose>
        <div class="container">
          <div class="container py-2">
              <div class="row my-2">
                  <div class="col-lg-8 order-lg-1 personal-info">
                      <form role="form" modelAttribute="countryForm" action="${pageContext.request.contextPath}/countries/save" method="post">
                          <input type="hidden" name="id" value="${country.id}" />
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Страна</label>
                              <div class="col-lg-9">
                                  <input class="form-control" name="name" type="text" value="${country.name}"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-lg-3 col-form-label form-control-label">Дата удаления</label>
                              <div class="col-lg-9">
                                  <input id="removeDateTime" class="form-control" name="removedDateStr" type="text"
                                         value="${country.removedDateStr}" alt="Кликните 2 раза для очистки поля" readonly/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <div class="col-lg-9 ml-auto text-right">
                                  <a href="/countries" class="btn btn-outline-secondary" >Отмена</a>
                                  <input type="submit" class="btn btn-primary" value="Сохранить" />
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
  <script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>