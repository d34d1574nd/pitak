<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Список жалоб</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
        <h1 class="mt-4">Тип</h1>
        <div class="container">
          <table id="listReportAdvert" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr style="text-align: center;">
                <th>№</th>
                <th>Заявитель</th>
                <th>Объявление</th>
                <th>Тип</th>
                <th>Решение</th>
                <th>Действие</th>
            </tr>
            </thead>
            <c:forEach items="${reportadverts}" var="reportadvert">
              <tr>
                <td>${reportadvert.id}</td>
                <c:if test="${not empty pageContext.request.userPrincipal}">
                  <c:if test="${pageContext.request.isUserInRole('ADMIN')}">
                      <td><a href="/admin/show/${reportadvert.createdBy.id}">
                              ${reportadvert.createdBy.name}<br/>${reportadvert.createdBy.surname}
                      </a></td>
                  </c:if>
                </c:if>
                <c:if test="${not empty pageContext.request.userPrincipal}">
                  <c:if test="${pageContext.request.isUserInRole('MODERATOR')}">
                    <td>${reportadvert.createdBy.name}<br/>${reportadvert.createdBy.surname}</td>
                  </c:if>
                </c:if>
                <td><a href="/adverts/show/${reportadvert.advert.id}">
                        ${reportadvert.advert.title}.
                    </a>
                    <br/><span style="color:red;font-weight: 800;">${reportadvert.advert.advertStatus.desc}</span>
                </td>
                <td>${reportadvert.reportType.name}</td>
                <td>${(reportadvert.solved.equals(true)) ? "Проверено":"Ожидает"}</td>
                <td width="15%">
                    <c:choose>
                        <c:when test="${(reportadvert.solved.equals(true))}">
                            <div class="d-inline">
                                <button type="submit" class="btn btn-outline-info" readonly disabled>Пропустить.</button>
                                <button type="submit" class="btn btn-outline-danger" readonly disabled>Заблокировать.</button>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="d-inline">
                                <form action="${pageContext.request.contextPath}/reportadvert" method="post" style="display: contents;">
                                    <input type="hidden" name="reportadvertId" value="${reportadvert.id}"/>
                                    <input type="hidden" name="action" value="solved"/>
                                    <button type="submit" class="btn btn-outline-info">Пропустить.</button>
                                </form>
                                <form action="${pageContext.request.contextPath}/reportadvert" method="post" style="display: contents;">
                                    <input type="hidden" name="reportadvertId" value="${reportadvert.id}"/>
                                    <input type="hidden" name="action" value="blocked"/>
                                    <button type="submit" class="btn btn-outline-danger">Заблокировать.</button>
                                </form>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
  <script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>