<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Главная</title>
    <link href="/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
        <h1 class="mt-4">Главная</h1>
        <p></p>
      </div>
    </div>
  </div>
  <script src="/resources/js/jquery-3.3.1.js"></script>
  <script src="/resources/js/bootstrap.min.js"></script>
  <script src="/resources/js/main.js"></script>
</body>
</html>