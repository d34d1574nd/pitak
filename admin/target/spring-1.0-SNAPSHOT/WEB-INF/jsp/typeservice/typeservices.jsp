<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Тип услуг</title>
    <link href="/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
        <h1 class="mt-4">Тип
            <a href="/typeservices/add" class="btn btn-primary" style="float: right;">Создать</a>
        </h1>
        <div class="container">
          <table id="listTypeServices" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr style="text-align: center;">
                <th>№</th>
                <th>Тип</th>
                <th>Дата удаления</th>
                <th>Действие</th>
            </tr>
            </thead>
            <c:forEach items="${typeservices}" var="typeservice">
              <tr>
                <td>${typeservice.id}</td>
                <td>${typeservice.name}</td>
                <td>${typeservice.removedDateStr}</td>
                <td width="15%">
                  <div class="d-inline">
                      <c:if test="${typeservice.id > 2}">
                          <a href="/typeservices/edit/${typeservice.id}" class="btn btn-outline-info">Ред.</a>
                          <form action="${pageContext.request.contextPath}/typeservices" method="post" style="display: contents;">
                            <input type="hidden" name="typeserviceId" value="${typeservice.id}"/>
                            <input type="hidden" name="action" value="delete"/>
                            <button type="submit" class="btn btn-outline-danger">Удал.</button>
                          </form>
                      </c:if>
                  </div>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script src="/resources/js/jquery-3.3.1.js"></script>
  <script src="/resources/js/bootstrap.min.js"></script>
  <script src="/resources/js/jquery.dataTables.min.js"></script>
  <script src="/resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="/resources/js/main.js"></script>
</body>
</body>
</html>