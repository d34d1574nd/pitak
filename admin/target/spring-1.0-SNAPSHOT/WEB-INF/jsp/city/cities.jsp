<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Города</title>
    <link href="${contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">
</head>
<body>
  <div class="d-flex" id="wrapper">
      <jsp:include page="../menu.jsp"/>
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom"></nav>
      <div class="container-fluid">
        <h1 class="mt-4">Города
            <a href="/cities/add" class="btn btn-primary" style="float: right;">Создать</a>
        </h1>
        <div class="container">
          <table id="listCities" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr style="text-align: center;">
                <th>№</th>
                <th>Страна</th>
                <th>Город</th>
                <th>Дата удаления</th>
                <th>Actions</th>
            </tr>
            </thead>
            <c:forEach items="${cities}" var="city">
              <tr>
                <td>${city.id}</td>
                <td>${city.country.name}</td>
                <td>${city.name}</td>
                <td>${city.removedDate}</td>
                <td width="15%">
                  <div class="d-inline">
                      <a href="/cities/edit/${city.id}" class="btn btn-outline-info">Ред.</a>
                      <form action="${pageContext.request.contextPath}/cities" method="post" style="display: contents;">
                        <input type="hidden" name="cityId" value="${city.id}"/>
                        <input type="hidden" name="action" value="delete"/>
                        <button type="submit" class="btn btn-outline-danger">Удал.</button>
                      </form>
                  </div>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script src="${contextPath}/resources/js/jquery-3.3.1.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
  <script src="${contextPath}/resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="${contextPath}/resources/js/main.js"></script>
</body>
</body>
</html>