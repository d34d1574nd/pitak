package kg.rest.backend.job;

import kg.rest.backend.dto.AppAdvertModel;
import kg.rest.backend.dto.PushNotificationModel;
import kg.rest.backend.dto.ResponseFcm;
import kg.rest.backend.dto.TypeServiceModel;
import kg.rest.backend.entities.dictionary.TypeService;
import kg.rest.backend.enums.AdvertType;
import kg.rest.backend.service.AppAdvertService;
import kg.rest.backend.service.TypeServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class NotifAboutNewAdvertJob {
    private final String title = "Новое объявление";
    private final AppAdvertService appAdvertService;
    private final TypeServiceService typeServiceService;
    @Value("${firebase.push.token}")
    private String token;
    @Value("${firebase.push.url}")
    private String url;

    @Value("${job.started}")
    private Boolean jobStarted;
    private final RestTemplate restTemplate;

    @Autowired
    public NotifAboutNewAdvertJob(AppAdvertService appAdvertService, TypeServiceService typeServiceService, RestTemplate restTemplate) {
        this.appAdvertService = appAdvertService;
        this.typeServiceService = typeServiceService;
        this.restTemplate = restTemplate;
    }

    @Scheduled(cron = "0/30 * * * * ?")
    public void sendPush(){
        if(!jobStarted)
            return;
        List<AppAdvertModel> appAdvertModels = appAdvertService.findAllByResponseFcmIsNullAndEnabled(true);
        if(!appAdvertModels.isEmpty()){
            System.out.println("START JOB " + NotifAboutNewAdvertJob.class);
            int countSend = 0;
            for (AppAdvertModel el: appAdvertModels) {
                PushNotificationModel pushModel = new PushNotificationModel();
                List<TypeServiceModel> typeServiceModels = typeServiceService.getAll();
                for (TypeServiceModel ts : typeServiceModels) {
                    if(el.getTypeService()!=null && el.getTypeService().equals(ts.getName()))
                        pushModel.setTo("/topics/" + ts.getName());
                }
                PushNotificationModel.Notification notification = pushModel.new Notification();
                notification.setTitle(title);
                notification.setBody(el.getTitle() + " " +el.getId());
                pushModel.setNotification(notification);
                HttpEntity<PushNotificationModel> request = getHeaders(pushModel);
                ResponseEntity<ResponseFcm> responseEntity = restTemplate.postForEntity(url,request, ResponseFcm.class);
                if(responseEntity.getStatusCodeValue()==200){
                    el.setResponseFcm(responseEntity.getBody().toString());
                    appAdvertService.updateFcmResponse(el);
                    countSend++;
                }
            }
            System.out.println("Отправлено всего: " + countSend);
            System.out.println("END JOB "+ NotifAboutNewAdvertJob.class);
        }

    }

    private HttpEntity<PushNotificationModel> getHeaders(PushNotificationModel model){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.set("Authorization",token);
        return new HttpEntity<>(model,headers);
    }
}
