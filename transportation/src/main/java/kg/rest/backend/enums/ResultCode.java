package kg.rest.backend.enums;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public enum ResultCode {
    SUCCESS, FAIL, EXCEPTION;

    private static ObjectMapper objectMapper = new ObjectMapper();
    public static ResultCode fromJSON(String jsonResponse) throws IOException {
        JsonNode actualObj = objectMapper.readTree(jsonResponse);
        return ResultCode.valueOf(actualObj.get("resultCode").asText());
    }
    public static ResultCode fromJSON(JsonNode jn){
        return ResultCode.valueOf(jn.get("resultCode").asText());
    }
}