package kg.rest.backend.enums;

public enum UploadType {
    ADVERT,
    PROFILE_PHOTO,
    CAR
}
