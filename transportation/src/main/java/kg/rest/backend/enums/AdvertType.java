package kg.rest.backend.enums;

public enum AdvertType {
    PASSENGER,
    DRIVER,
    PACKAGE,
    SPECIAL_TECH,
    CARGO_TRANSPORTATION
}
