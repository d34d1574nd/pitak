package kg.rest.backend.enums;

import lombok.Getter;

public enum AdvertStatus {
    MODERATION("На модерации"),
    APPROVED("Одобрено"),
    BLOCKED("Заблокировано")
    ;

    @Getter
    private String desc;

    private AdvertStatus(String desc){
        this.desc = desc;
    }
}
