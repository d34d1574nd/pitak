package kg.rest.backend.enums;

public enum UserType {
    DRIVER,
    PASSENGER,
    MODERATOR,
    ADMIN
}
