package kg.rest.backend;

public class TransporationsException extends Exception {
    public TransporationsException(String msg){
        super(msg);
    }
}
