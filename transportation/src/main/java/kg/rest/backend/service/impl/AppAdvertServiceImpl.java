package kg.rest.backend.service.impl;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppAdvertAttachmentModel;
import kg.rest.backend.dto.AppAdvertModel;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.entities.*;
import kg.rest.backend.enums.AdvertStatus;
import kg.rest.backend.enums.FileType;
import kg.rest.backend.enums.UploadType;
import kg.rest.backend.payload.request.AdvertSearchPatternRequest;
import kg.rest.backend.payload.request.AdvertWrapperRequest;
import kg.rest.backend.payload.response.AdvertWrapperResponse;
import kg.rest.backend.repository.*;
import kg.rest.backend.service.AppAdvertService;
import kg.rest.backend.service.FileService;
import kg.rest.backend.service.UserService;
import kg.rest.backend.specification.AdvertSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AppAdvertServiceImpl implements AppAdvertService {
    private final AppAdvertRepository appAdvertRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final FavoritesRepository favoritesRepository;
    private final FileService fileService;
    private final CarRepository carRepository;
    private final AdvertAttachmentRepository advertAttachmentRepository;

    public AppAdvertServiceImpl(AppAdvertRepository appAdvertRepository, UserRepository userRepository, UserService userService,
                                FavoritesRepository favoritesRepository, FileService fileService, CarRepository carRepository,
                                AdvertAttachmentRepository advertAttachmentRepository) {
        this.appAdvertRepository = appAdvertRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.favoritesRepository = favoritesRepository;
        this.fileService = fileService;
        this.carRepository = carRepository;
        this.advertAttachmentRepository = advertAttachmentRepository;
    }

    private AppAdvert create(AppAdvertModel model) throws TransporationsException {
        try {
            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            if (!currentUser.isPresent())
                throw new TransporationsException("Текущий пользователь не найден");
            Optional<User> createdBy = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());
            AppAdvert advert = new AppAdvert(model);
            if (createdBy.isPresent())
                advert.setCreatedBy(createdBy.get());
            return advert;
        } catch (Exception ex) {
            throw new TransporationsException("Ошибка создания: " + ex.getMessage());
        }

    }

    private AppAdvert update(AppAdvertModel appAdvertModel) throws TransporationsException {
        AppAdvert appAdvert = appAdvertRepository.getOne(appAdvertModel.getId());
        if(appAdvert==null)
            throw new TransporationsException("Такое объявление не найдено при обновлении");
        appAdvert.setUts(LocalDateTime.now());
        appAdvert.setTitle(appAdvertModel.getTitle());
        appAdvert.setText(appAdvertModel.getText());
        appAdvert.setTypeService(appAdvertModel.getTypeService());
        appAdvert.setFromPlace(appAdvertModel.getFromPlace());
        appAdvert.setSendDateTime(appAdvertModel.getSendDateTime());
        appAdvert.setToPlace(appAdvertModel.getToPlace());
        appAdvert.setArrivalDateTime(appAdvertModel.getArrivalDateTime());
        appAdvert.setAmountPayment(appAdvertModel.getAmountPayment());
        appAdvert.setNumberOfSeat(appAdvertModel.getNumberOfSeat());
        return appAdvert;
    }

    private List<AppAdvertAttachment> createAttachment(List<MultipartFile> files, AppAdvert appAdvert) throws TransporationsException {
        List<AppAdvertAttachment> attachments = new ArrayList<>();
        for (MultipartFile file : files
        ) {
            AppFile appFile = fileService.saveFile(file, FileType.IMG, UploadType.ADVERT);
            AppAdvertAttachment newAttachment = new AppAdvertAttachment();
            newAttachment.setAppFile(appFile);
            newAttachment.setAdvert(appAdvert);
            newAttachment = advertAttachmentRepository.save(newAttachment);
            attachments.add(newAttachment);
        }
        return attachments;
    }

    @Override
    public AdvertWrapperResponse save(AdvertWrapperRequest request) throws TransporationsException {
        AppAdvert advert;
        AdvertWrapperResponse response = new AdvertWrapperResponse();
        AppAdvertModel appAdvertModel = request.getAppAdvertModel();
        if (appAdvertModel.getId() == null) {
            advert = create(appAdvertModel);
        } else {
            advert = update(appAdvertModel);
        }
        advert.setAdvertStatus(AdvertStatus.MODERATION);
        advert.setEnabled(false);
        if (appAdvertModel.getCarCommonModel() != null && appAdvertModel.getCarCommonModel().getId() != null) {
            Car car = carRepository.getOne(appAdvertModel.getCarCommonModel().getId());
            advert.setCar(car);
        }

        AppAdvert saved = appAdvertRepository.save(advert);
        response.setAppAdvertModel(saved.toModel());
        if (saved.getId() != null) {
            if (request.getMultiUploadRequest() != null && !request.getMultiUploadRequest().getFileList().isEmpty()) {
                List<AppAdvertAttachment> attachments = createAttachment(request.getMultiUploadRequest().getFileList(), saved);
                response.setAttachmentModels(attachments.stream().map(AppAdvertAttachment::toModel).collect(Collectors.toList()));
            }
        }

        return response;
    }

    @Override
    public Page<AdvertWrapperResponse> search(AdvertSearchPatternRequest patternRequest, Pageable pageable) {
        AdvertSpecification specification = new AdvertSpecification(patternRequest);
        Page<AppAdvert> appAdvertPage = appAdvertRepository.findAll(specification, pageable);
        List<AdvertWrapperResponse> responseList = new ArrayList<>();
        for (AppAdvert app : appAdvertPage
        ) {
            AppAdvertModel model = app.toModel();
            User user = userRepository.getOne(model.getCreatedBy());
            model.setMobileNumber(user.getUsername());
            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            Boolean hasFavorite = favoritesRepository.existsAllByAdvertIdAndUserId(model.getId(), currentUser.get().getId());
            model.setIsFavorite(hasFavorite);

            responseList.add(createResponseByAdvertModel(model));
        }

        Page<AdvertWrapperResponse> modelPage = new PageImpl<>(responseList, appAdvertPage.getPageable(), appAdvertPage.getTotalElements());
        return modelPage;
    }

    @Override
    public Page<AdvertWrapperResponse> activeAdverts(AdvertSearchPatternRequest patternRequest, Pageable pageable) {
        AdvertSpecification specification = new AdvertSpecification(patternRequest);
        List<AdvertWrapperResponse> responseList = new ArrayList<>();
        Page<AppAdvert> appAdvertPage = appAdvertRepository.findAll(specification, pageable);
        for (AppAdvert app : appAdvertPage) {
            AppAdvertModel model = app.toModel();
            User user = userRepository.getOne(model.getCreatedBy());
            model.setMobileNumber(user.getUsername());
            model.setIsFavorite(false);
            responseList.add(createResponseByAdvertModel(model));
        }
        Page<AdvertWrapperResponse> modelPage = new PageImpl<>(responseList, appAdvertPage.getPageable(), appAdvertPage.getTotalElements());
        return modelPage;
    }

    @Override
    public Favorites createFavouriteAdvert(Long advertId) throws TransporationsException {
        Favorites favorites = null;
        if (advertId != null) {
            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            if (!currentUser.isPresent())
                throw new TransporationsException("Текущий пользователь не найден");
            Optional<User> user = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

            AppAdvert appAdvert = appAdvertRepository.getOne(advertId);
            if (appAdvert != null && user.isPresent()) {
                List<Favorites> favoritesList = favoritesRepository.findAllByAdvertIdAndUserId(appAdvert.getId(), user.get().getId());
                if (favoritesList.isEmpty()) {
                    favorites = new Favorites();
                    favorites.setAdvertId(appAdvert.getId());
                    favorites.setUserId(user.get().getId());
                    favorites = favoritesRepository.save(favorites);
                }
            }
        }

        return favorites;
    }

    @Override
    @Transactional
    public Boolean deleteFavouriteAdvert(Long advertId) throws TransporationsException {
        if (advertId != null) {
            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            if (!currentUser.isPresent())
                throw new TransporationsException("Текущий пользователь не найден");
            Optional<User> user = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

            Integer count = favoritesRepository.removeByAdvertIdAndUserId(advertId, user.get().getId());
            return count > 0;
        }
        return null;
    }

    @Override
    public List<AdvertWrapperResponse> getFavouriteAdvertList() throws TransporationsException {
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> user = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

        List<AdvertWrapperResponse> responseList = null;
        List<AppAdvertModel> appAdvertModels = new ArrayList<>();
        if (user.isPresent()) {
            List<Favorites> favorites = favoritesRepository.findAllByUserId(user.get().getId());
            if (!favorites.isEmpty()) {
                List<AppAdvert> appAdvertList = appAdvertRepository.findAllByIdIn(favorites.stream().map(f -> f.getAdvertId()).collect(Collectors.toList()));
                responseList = new ArrayList<>();
                for (AppAdvert app : appAdvertList
                ) {
                    AppAdvertModel model = app.toModel();
                    User userModel = userRepository.getOne(model.getCreatedBy());
                    model.setMobileNumber(userModel.getUsername());
                    model.setIsFavorite(true);
                    responseList.add(createResponseByAdvertModel(model));
                }
            }
        }
        return responseList;
    }

    @Override
    public AdvertWrapperResponse getById(Long advertId) throws TransporationsException {
        AdvertWrapperResponse response = null;
        Optional<AppAdvert> appAdvertOptional = appAdvertRepository.findById(advertId);
        if (appAdvertOptional.isPresent()) {
            AppAdvertModel advertModel = appAdvertOptional.get().toModel();
            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            if (!currentUser.isPresent())
                throw new TransporationsException("Текущий пользователь не найден");
            Optional<User> user = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

            User createdBy = userRepository.getOne(advertModel.getCreatedBy());

            final Boolean hasFavorite = favoritesRepository.existsAllByAdvertIdAndUserId(appAdvertOptional.get().getId(), user.get().getId());
            advertModel.setMobileNumber(createdBy.getUsername());
            advertModel.setIsFavorite(hasFavorite);
            response = createResponseByAdvertModel(advertModel);
        }
        return response;
    }

    @Override
    public AdvertWrapperResponse getByIdAndUserId(long advertId, long userId) {
        AdvertWrapperResponse response = null;

        User createdBy = userRepository.getOne(userId);
        AppAdvert myAdvert = appAdvertRepository.findFirstByIdAndCreatedBy(advertId, createdBy);
        if (myAdvert != null) {
            AppAdvertModel advertModel = myAdvert.toModel();
            final Boolean hasFavorite = favoritesRepository.existsAllByAdvertIdAndUserId(myAdvert.getId(), createdBy.getId());
            advertModel.setMobileNumber(createdBy.getUsername());
            advertModel.setIsFavorite(hasFavorite);
            response = createResponseByAdvertModel(advertModel);
        }
        return response;
    }

    @Override
    public AdvertWrapperResponse removeByIdAndUserId(long advertId, long userId) {
        User user = userRepository.getOne(userId);
        AppAdvert appAdvert = appAdvertRepository.findFirstByIdAndCreatedBy(advertId, user);
        appAdvert.setRemoveDate(LocalDateTime.now());
        appAdvert.setEnabled(false);
        appAdvert = appAdvertRepository.save(appAdvert);
        AdvertWrapperResponse response = new AdvertWrapperResponse();
        response.setAppAdvertModel(appAdvert.toModel());
        return response;
    }

    private AdvertWrapperResponse createResponseByAdvertModel(AppAdvertModel model) {
        AdvertWrapperResponse response = new AdvertWrapperResponse();
        AppAdvert exist = appAdvertRepository.getOne(model.getId());
        List<AppAdvertAttachmentModel> attachmentModels = advertAttachmentRepository.findAllByAdvert(exist).stream()
                .map(AppAdvertAttachment::toModel).collect(Collectors.toList());
        response = new AdvertWrapperResponse();
        response.setAppAdvertModel(model);
        response.setAttachmentModels(attachmentModels);
        return response;
    }

    @Override
    public AppAdvert getEntityById(Long advertId) {
        Optional<AppAdvert> appAdvertOptional = appAdvertRepository.findById(advertId);
        return appAdvertOptional.get();
    }

    @Override
    public Boolean existAdvert(Long advertId) {
        return appAdvertRepository.existsById(advertId);
    }

    @Override
    public Boolean existEnabledAdvert(Long advertId) {
        return appAdvertRepository.existsByIdAndEnabled(advertId,true);
    }

    @Override
    public Boolean isDeletedAd(Long advertId) {
        return appAdvertRepository.existsByIdAndRemoveDateIsNotNull(advertId);
    }

    @Override
    public List<AppAdvertModel> findAllByResponseFcmIsNullAndEnabled(Boolean isEnabled) {
        return appAdvertRepository.findAllByResponseFcmIsNullAndEnabled(isEnabled).stream().map(AppAdvert::toModel).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public AppAdvertModel updateFcmResponse(AppAdvertModel model) {
        AppAdvert appAdvert = appAdvertRepository.getOne(model.getId());
        appAdvert.setResponseFcm(model.getResponseFcm());
        return appAdvertRepository.save(appAdvert).toModel();
    }

    @Override
    @Transactional
    public Page<AdvertWrapperResponse> getSimilarAds(Long advertId, Pageable pageable) {
        AppAdvert curAdvert = appAdvertRepository.getOne(advertId);
        List<AdvertWrapperResponse> responseList = new ArrayList<>();
        Page<AppAdvert> appAdvertPage = appAdvertRepository.findAllByTypeServiceAndEnabled(curAdvert.getTypeService(), true, pageable);
        for (AppAdvert app : appAdvertPage
        ) {
            AppAdvertModel model = app.toModel();
            User user = userRepository.getOne(model.getCreatedBy());
            model.setMobileNumber(user.getUsername());
            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            Boolean hasFavorite = favoritesRepository.existsAllByAdvertIdAndUserId(model.getId(),currentUser.get().getId());
            model.setIsFavorite(hasFavorite);
            responseList.add(createResponseByAdvertModel(model));
        }

        Page<AdvertWrapperResponse> modelPage = new PageImpl<>(responseList, appAdvertPage.getPageable(), appAdvertPage.getTotalElements());
        return modelPage;
    }

    @Override
    public List<AdvertWrapperResponse> getMyFavouriteByTitle(String title) throws TransporationsException {
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> user = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

        List<AdvertWrapperResponse> responseList = new ArrayList<>();
        if (user.isPresent()) {
            List<Favorites> favorites = favoritesRepository.findAllByUserId(user.get().getId());
            if (!favorites.isEmpty()) {
                List<AppAdvert> appAdvertList = appAdvertRepository.findAllByTitleContainsAndIdIn(title, favorites.stream().map(f -> f.getAdvertId()).collect(Collectors.toList()));
                for (AppAdvert app : appAdvertList
                ) {
                    AppAdvertModel model = app.toModel();
                    User userModel = userRepository.getOne(model.getCreatedBy());
                    model.setMobileNumber(userModel.getUsername());
                    model.setIsFavorite(true);
                    responseList.add(createResponseByAdvertModel(model));
                }
                return responseList;
            }
        }
        return null;
    }
}
