package kg.rest.backend.service;


import kg.rest.backend.dto.ReportTypeModel;
import kg.rest.backend.entities.dictionary.ReportType;

import java.util.List;

public interface ReportTypeService {
    List<ReportTypeModel> getAllActiveReportType();
    ReportType getEntityReportType(Long reportTypeId);
}
