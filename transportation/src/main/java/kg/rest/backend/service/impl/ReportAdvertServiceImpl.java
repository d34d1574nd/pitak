package kg.rest.backend.service.impl;

import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.dto.ReportAdvertModel;
import kg.rest.backend.entities.AppAdvert;
import kg.rest.backend.entities.ReportAdvert;
import kg.rest.backend.entities.User;
import kg.rest.backend.entities.dictionary.ReportType;
import kg.rest.backend.payload.request.ReportAdvertRequest;
import kg.rest.backend.repository.AppAdvertRepository;
import kg.rest.backend.repository.ReportAdvertRepository;
import kg.rest.backend.repository.ReportTypeRepository;
import kg.rest.backend.repository.UserRepository;
import kg.rest.backend.service.ReportAdvertService;
import kg.rest.backend.service.UserService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class ReportAdvertServiceImpl implements ReportAdvertService {
    private final ReportAdvertRepository reportAdvertRepository;
    private final ReportTypeRepository reportTypeRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final AppAdvertRepository appAdvertRepository;

    public ReportAdvertServiceImpl(ReportAdvertRepository reportAdvertRepository, ReportTypeRepository reportTypeRepository, UserRepository userRepository, UserService userService, AppAdvertRepository appAdvertRepository) {
        this.reportAdvertRepository = reportAdvertRepository;
        this.reportTypeRepository = reportTypeRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.appAdvertRepository = appAdvertRepository;
    }

    @Override
    public ReportAdvertModel reportForAdvert(ReportAdvertRequest request) {
        ReportAdvert reportAdvert = null;
        User createdBy = null;
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if(currentUser.isPresent())
            createdBy = userRepository.getOne(currentUser.get().getId());
        AppAdvert advert = appAdvertRepository.getOne(request.getAdvertId());
        ReportType reportType = reportTypeRepository.getOne(request.getReportTypeId());
        if(createdBy!=null && advert!=null && reportType!=null){
            reportAdvert = new ReportAdvert();
            reportAdvert.setCreatedBy(createdBy);
            reportAdvert.setAdvert(advert);
            reportAdvert.setReportType(reportType);
            reportAdvert.setAdditionalDescription(request.getAdditionalDescription());
            reportAdvert.setCts(LocalDateTime.now());
            reportAdvert = reportAdvertRepository.save(reportAdvert);
        }

        return reportAdvert.toModel();
    }
}
