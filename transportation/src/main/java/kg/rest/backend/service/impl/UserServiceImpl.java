package kg.rest.backend.service.impl;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.*;
import kg.rest.backend.entities.AppFile;
import kg.rest.backend.entities.ERole;
import kg.rest.backend.entities.Role;
import kg.rest.backend.entities.User;
import kg.rest.backend.entities.dictionary.*;
import kg.rest.backend.enums.FileType;
import kg.rest.backend.enums.UploadType;
import kg.rest.backend.enums.UserType;
import kg.rest.backend.payload.request.FileUploadRequest;
import kg.rest.backend.payload.request.SignupRequest;
import kg.rest.backend.repository.CityRepository;
import kg.rest.backend.repository.CountryRepository;
import kg.rest.backend.repository.RoleRepository;
import kg.rest.backend.repository.UserRepository;
import kg.rest.backend.security.services.UserDetailsImpl;
import kg.rest.backend.service.*;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;
    private final CarBrandService carBrandService;
    private final CarModelService carModelService;
    private final CarTypeService carTypeService;
    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;
    private final FileService fileService;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder, CarBrandService carBrandService, CarModelService carModelService, CarTypeService carTypeService, CountryRepository countryRepository, CityRepository cityRepository, FileService fileService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.carBrandService = carBrandService;
        this.carModelService = carModelService;
        this.carTypeService = carTypeService;
        this.countryRepository = countryRepository;
        this.cityRepository = cityRepository;
        this.fileService = fileService;
    }

    @Override
    public AppUserModel createNewUser(SignupRequest signUpRequest) throws TransporationsException {
        AppFile newFile = null;

        if(signUpRequest.getUserType().equals(UserType.DRIVER) && (signUpRequest.getCarCommonModel()==null || !existsCarModel(signUpRequest.getCarCommonModel()))){
            throw new TransporationsException("Такие данные отсутствуют в справочниках для машин");
        }

        if(signUpRequest.getUserType().equals(UserType.DRIVER) && (!existCountryCity(signUpRequest.getCountryModel(),signUpRequest.getCityModel()))){
            throw new TransporationsException("Такие данные отсутствуют в справочниках стран и городов");
        }

        if(signUpRequest.getProfilePhoto()!=null && !signUpRequest.getProfilePhoto().isEmpty()){
            newFile = fileService.saveFile(signUpRequest.getProfilePhoto(),FileType.IMG, UploadType.PROFILE_PHOTO);
        }

        User user = new User(signUpRequest);
        user = setAddress(signUpRequest.getCountryModel().getId(),signUpRequest.getCityModel().getId(),user);
        user.setPassword(encoder.encode(signUpRequest.getPassword()));
        user.setProfilePhoto(newFile);

        if(signUpRequest.getUserType()!=null){
            Optional<Role> role;
            if(signUpRequest.getUserType().equals(UserType.DRIVER)){
                role = roleRepository.findByName(ERole.ROLE_DRIVER);
            }
            else if(signUpRequest.getUserType().equals(UserType.PASSENGER)){
                role = roleRepository.findByName(ERole.ROLE_PASSENGER);
            }
            else{
                throw new TransporationsException("Тип пользователя должен быть указан");
            }
            if(role.isPresent()){
                Set<Role> roles = new HashSet<>();
                roles.add(role.get());
                user.setRoles(roles);
            }
        }

        User saved = userRepository.save(user);
        return saved.toModel();
    }

    @Override
    public AppUserModel editUser(EditUserModel editUserModel) throws TransporationsException {
        Optional<User> editUserOpt = userRepository.findById(editUserModel.getId());
        if(editUserOpt.isPresent()){
            User editUser = editUserOpt.get();
            editUser.setUsername(editUserModel.getUsername());
            editUser.setSurname(editUserModel.getSurname());
            editUser.setName(editUserModel.getName());
            editUser.setPatronymic(editUserModel.getPatronymic());
            editUser = setAddress(editUserModel.getCountryModel().getId(),editUserModel.getCityModel().getId(),editUser);
            if(!editUserModel.getNewPassword().isEmpty()){
                editUser.setPassword(encoder.encode(editUserModel.getNewPassword()));
            }

            editUser.setEmail(editUserModel.getEmail());
            editUser.setUts(LocalDateTime.now());

            if(editUser.getUserType()!=null){
                Optional<Role> role = Optional.empty();
                if(editUser.getUserType().equals(UserType.DRIVER)){
                    role = roleRepository.findByName(ERole.ROLE_DRIVER);
                }
                else if(editUser.getUserType().equals(UserType.PASSENGER)){
                    role = roleRepository.findByName(ERole.ROLE_PASSENGER);
                }
                else{
                    throw new TransporationsException("При создании пользователя тип юзера должен быть заполнен");
                }
                if(role.isPresent()){
                    Set<Role> roles = new HashSet<>();
                    roles.add(role.get());
                    editUser.setRoles(roles);
                }
            }
            editUser = userRepository.save(editUser);
            return editUser.toModel();
        }

        return null;
    }

    @Override
    public AppUserModel getAppUserByUsernameAndType(String username, UserType userType) {
        Optional<User> userOptional = userRepository.findByUsernameAndUserType(username, userType);
        AppUserModel model = null;
        if (userOptional.isPresent())
            model = userOptional.get().toModel();

        return model;
    }


    @Override
    public Optional<AppUserModel> getCurrentUser(){
        Optional<AppUserModel> userModel = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            UserDetailsImpl currentUserDetails = (UserDetailsImpl) authentication.getPrincipal();
            Optional<User> currentUser = userRepository.findByUsernameAndUserType(currentUserDetails.getUsername(), currentUserDetails.getUserType());
            userModel = Optional.ofNullable(currentUser.get().toModel());
        }
        return userModel;
    }

    @Override
    public AppFileModel updateProfilePhoto(FileUploadRequest request) throws TransporationsException {
        Optional<AppUserModel> currentUser = getCurrentUser();
        AppFile uploadedFile = null;
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> user = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());
        if (user.isPresent()) {
            User usr = user.get();
            uploadedFile = fileService.saveFile(request.getFile(), FileType.IMG, UploadType.PROFILE_PHOTO);
            if (usr.getProfilePhoto() != null && uploadedFile != null) {
                Long oldProfilePhotoId = user.get().getProfilePhoto().getId();
                user.get().setProfilePhoto(null);
                user = Optional.of(userRepository.save(user.get()));
                if (user.isPresent() && user.get().getProfilePhoto() == null) {
                    fileService.deleteFile(oldProfilePhotoId);
                }
                else{
                    throw new TransporationsException("Не удалось удалить старую фотографию");
                }
            }

            if(uploadedFile!=null){
                usr.setProfilePhoto(uploadedFile);
                userRepository.save(usr);
            }
        }
        return uploadedFile!=null?uploadedFile.toModel():null;
    }

    @Override
    public Boolean removeProfilePhoto() throws TransporationsException {
        Optional<AppUserModel> currentUser = getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        boolean isDeleted = false;
        Optional<User> curUser = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());
        if (curUser.isPresent() && curUser.get().getProfilePhoto() != null) {
            Long fileId = curUser.get().getProfilePhoto().getId();
            curUser.get().setProfilePhoto(null);
            userRepository.save(curUser.get());
            isDeleted = fileService.deleteFile(fileId);
        }
        return isDeleted;
    }

    private boolean existsCarModel(CarCommonModel carCommonModel){
        return carBrandService.existById(carCommonModel.getCarBrand().getId())
        && carModelService.existById(carCommonModel.getCarModel().getId())
        && carTypeService.existById(carCommonModel.getCarType().getId());
    }

    private boolean existCountryCity(CountryModel countryModel, CityModel cityModel){
        return  countryModel!=null && cityModel!=null &&
                countryModel.getId()!=null && cityModel.getId()!=null &&
                countryRepository.existsById(countryModel.getId()) && cityRepository.existsById(cityModel.getId())
                ;
    }

    private User setAddress(Long countryId,Long cityId, User user){
        Country country = countryRepository.findById(countryId).get();
        City city = cityRepository.findById(cityId).get();
        user.setCountry(country);
        user.setCity(city);

        return user;
    }

}
