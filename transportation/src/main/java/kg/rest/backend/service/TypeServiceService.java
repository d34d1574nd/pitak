package kg.rest.backend.service;

import kg.rest.backend.dto.TypeServiceModel;
import kg.rest.backend.entities.dictionary.TypeService;

import java.util.List;

public interface TypeServiceService {
    List<TypeServiceModel> getAll();
    boolean existById(Long id);
    TypeService getEntityById(Long id);
    List<TypeService> getEntitiesByWithOutId(Long id);
}
