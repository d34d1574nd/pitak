package kg.rest.backend.service;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.CarAttachmentModel;
import kg.rest.backend.payload.request.FileMultiUploadRequest;

import java.util.List;

public interface CarAttachmentService {
    Boolean deleteByMyCar(Long carId, Long fileId) throws TransporationsException;
    List<CarAttachmentModel> addMyCarPhoto(FileMultiUploadRequest multiUploadRequest, Long carId) throws Exception;
}
