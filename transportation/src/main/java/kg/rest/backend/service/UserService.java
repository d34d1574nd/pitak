package kg.rest.backend.service;


import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppFileModel;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.dto.EditUserModel;
import kg.rest.backend.enums.UserType;
import kg.rest.backend.payload.request.FileUploadRequest;
import kg.rest.backend.payload.request.SignupRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public interface UserService {
    AppUserModel getAppUserByUsernameAndType(String username, UserType userType);

    Boolean removeProfilePhoto() throws TransporationsException;

    AppUserModel createNewUser(SignupRequest signupRequest) throws TransporationsException, IOException;
    AppUserModel editUser(EditUserModel editUserModel) throws TransporationsException;

    Optional<AppUserModel> getCurrentUser();
    AppFileModel updateProfilePhoto(FileUploadRequest request) throws TransporationsException;
}
