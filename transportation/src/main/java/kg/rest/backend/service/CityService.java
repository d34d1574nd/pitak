package kg.rest.backend.service;

import kg.rest.backend.dto.CityModel;

import java.util.List;

public interface CityService {
    List<CityModel> getByParent(Long parentId);
}
