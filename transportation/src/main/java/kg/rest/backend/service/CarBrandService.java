package kg.rest.backend.service;

import kg.rest.backend.dto.CarBrandModel;
import kg.rest.backend.entities.dictionary.CarBrand;

import java.util.List;

public interface CarBrandService {
    List<CarBrandModel> getAll();
    CarBrandModel getById(Long id);
    CarBrand getEntityById(Long id);
    boolean existById(Long id);
}
