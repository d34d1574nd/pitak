package kg.rest.backend.service;

import kg.rest.backend.enums.UserType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.io.Serializable;

public interface CustomUserDetailService extends Serializable {
    UserDetails loadUserByUsernameAndUserType(String username, UserType userType) throws UsernameNotFoundException;
}
