package kg.rest.backend.service;

import kg.rest.backend.dto.CarTypeModel;
import kg.rest.backend.entities.dictionary.CarType;

import java.util.List;

public interface CarTypeService {
    List<CarTypeModel> getAll();
    boolean existById(Long id);
    CarType getEntityById(Long id);
}
