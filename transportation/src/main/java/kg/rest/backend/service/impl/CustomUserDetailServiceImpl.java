package kg.rest.backend.service.impl;

import kg.rest.backend.entities.User;
import kg.rest.backend.enums.UserType;
import kg.rest.backend.repository.UserRepository;
import kg.rest.backend.security.services.UserDetailsImpl;
import kg.rest.backend.service.CustomUserDetailService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailServiceImpl implements CustomUserDetailService {
    private final UserRepository userRepository;

    public CustomUserDetailServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public UserDetails loadUserByUsernameAndUserType(String username, UserType userType) throws UsernameNotFoundException {
        User user = userRepository.findByUsernameAndUserType(username, userType)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        return UserDetailsImpl.build(user);
    }
}
