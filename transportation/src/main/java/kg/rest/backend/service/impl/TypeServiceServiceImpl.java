package kg.rest.backend.service.impl;

import kg.rest.backend.dto.TypeServiceModel;
import kg.rest.backend.entities.dictionary.TypeService;
import kg.rest.backend.repository.TypeServiceRepository;
import kg.rest.backend.service.TypeServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TypeServiceServiceImpl implements TypeServiceService {
    @Autowired
    private TypeServiceRepository typeServiceRepository;


    @Override
    public List<TypeServiceModel> getAll() {
        List<TypeServiceModel> list = new ArrayList<>();
        for (TypeService ts : typeServiceRepository.findAllByRemovedDateIsNullOrderByName()) {
            list.add(ts.toModel());
        }
        return list;
    }

    @Override
    public boolean existById(Long id) {
        return typeServiceRepository.existsById(id);
    }

    @Override
    public TypeService getEntityById(Long id) {
        return typeServiceRepository.getOne(id);
    }

    @Override
    public List<TypeService> getEntitiesByWithOutId(Long id) {
        return typeServiceRepository.findAllByIdIsNotIn(id);//.stream().map(TypeService::toModel).collect(Collectors.toList());
    }
}
