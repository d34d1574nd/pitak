package kg.rest.backend.service;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.CarCommonModel;
import kg.rest.backend.entities.User;
import kg.rest.backend.payload.request.CarWrapperRequest;
import kg.rest.backend.payload.response.CarWrapperResponse;

import java.util.List;

public interface CarService {
    CarWrapperResponse create(CarWrapperRequest carWrapperRequest) throws TransporationsException;
    CarWrapperResponse editCar(CarWrapperRequest carWrapperRequest) throws TransporationsException;
    CarWrapperResponse removeCar(Long carId) throws TransporationsException;
    List<CarWrapperResponse> getCarCommonModels(User user);
    CarWrapperResponse getMyCar(Long id, Long userId);
    CarCommonModel saveCarCommon(CarCommonModel carCommonModel);
}
