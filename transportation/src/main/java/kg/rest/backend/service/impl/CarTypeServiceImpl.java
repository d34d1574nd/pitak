package kg.rest.backend.service.impl;

import kg.rest.backend.dto.CarTypeModel;
import kg.rest.backend.entities.dictionary.CarType;
import kg.rest.backend.repository.CarTypeRepository;
import kg.rest.backend.service.CarTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarTypeServiceImpl implements CarTypeService {
    @Autowired
    private CarTypeRepository carTypeRepository;


    @Override
    public List<CarTypeModel> getAll() {
        List<CarTypeModel> list = new ArrayList<>();
        for (CarType ca: carTypeRepository.findAllByRemovedDateIsNullOrderByName()
             ) {
            list.add(ca.toModel());
        }
        return list;
    }

    @Override
    public boolean existById(Long id) {
        return carTypeRepository.existsById(id);
    }

    @Override
    public CarType getEntityById(Long id) {
        return carTypeRepository.getOne(id);
    }
}
