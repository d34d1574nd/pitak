package kg.rest.backend.service;


import kg.rest.backend.dto.ReportAdvertModel;
import kg.rest.backend.payload.request.ReportAdvertRequest;

public interface ReportAdvertService {
    ReportAdvertModel reportForAdvert(ReportAdvertRequest request);
}
