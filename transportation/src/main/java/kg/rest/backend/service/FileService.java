package kg.rest.backend.service;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.entities.AppFile;
import kg.rest.backend.enums.FileType;
import kg.rest.backend.enums.UploadType;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    AppFile saveFile(MultipartFile fileModel, FileType fileType, UploadType uploadType) throws TransporationsException;
    boolean deleteFile(long fileId) throws TransporationsException;
    }
