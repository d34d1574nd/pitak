package kg.rest.backend.service;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppAdvertModel;
import kg.rest.backend.entities.AppAdvert;
import kg.rest.backend.entities.Favorites;
import kg.rest.backend.payload.request.AdvertSearchPatternRequest;
import kg.rest.backend.payload.request.AdvertWrapperRequest;
import kg.rest.backend.payload.response.AdvertWrapperResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AppAdvertService {
    AdvertWrapperResponse save(AdvertWrapperRequest request) throws TransporationsException;

    Page<AdvertWrapperResponse> search(AdvertSearchPatternRequest patternRequest, Pageable pageable);

    Page<AdvertWrapperResponse> activeAdverts(AdvertSearchPatternRequest patternRequest, Pageable pageable);

    Favorites createFavouriteAdvert(Long advertId) throws TransporationsException;

    Boolean deleteFavouriteAdvert(Long advertId) throws TransporationsException;

    List<AdvertWrapperResponse> getFavouriteAdvertList() throws TransporationsException;

    AdvertWrapperResponse getById(Long advertId) throws TransporationsException;

    AdvertWrapperResponse getByIdAndUserId(long advertId, long userId);

    AdvertWrapperResponse removeByIdAndUserId(long advertId, long userId);

    AppAdvert getEntityById(Long advertId);

    Boolean existAdvert(Long advertId);

    Boolean existEnabledAdvert(Long advertId);

    Boolean isDeletedAd(Long advertId);

    List<AppAdvertModel> findAllByResponseFcmIsNullAndEnabled(Boolean isEnabled);

    AppAdvertModel updateFcmResponse(AppAdvertModel model);

    Page<AdvertWrapperResponse> getSimilarAds(Long advertId, Pageable pageable);

    List<AdvertWrapperResponse> getMyFavouriteByTitle(String title) throws TransporationsException;
}
