package kg.rest.backend.service.impl;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.dto.CarAttachmentModel;
import kg.rest.backend.dto.CarCommonModel;
import kg.rest.backend.entities.AppFile;
import kg.rest.backend.entities.Car;
import kg.rest.backend.entities.CarAttachment;
import kg.rest.backend.entities.User;
import kg.rest.backend.entities.dictionary.CarBrand;
import kg.rest.backend.entities.dictionary.CarModel;
import kg.rest.backend.entities.dictionary.CarType;
import kg.rest.backend.enums.FileType;
import kg.rest.backend.enums.UploadType;
import kg.rest.backend.payload.request.CarWrapperRequest;
import kg.rest.backend.payload.response.CarWrapperResponse;
import kg.rest.backend.repository.CarAttachmentRepository;
import kg.rest.backend.repository.CarRepository;
import kg.rest.backend.repository.UserRepository;
import kg.rest.backend.service.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;
    private final CarBrandService carBrandService;
    private final CarModelService carModelService;
    private final CarTypeService carTypeService;
    private final UserRepository userRepository;
    private final UserService userService;
    private final FileServiceImpl fileService;
    private final CarAttachmentRepository carAttachmentRepository;

    public CarServiceImpl(CarRepository carRepository, CarBrandService carBrandService, CarModelService carModelService, CarTypeService carTypeService, UserRepository userRepository, UserService userService, FileServiceImpl fileService, CarAttachmentRepository carAttachmentRepository) {
        this.carRepository = carRepository;
        this.carBrandService = carBrandService;
        this.carModelService = carModelService;
        this.carTypeService = carTypeService;
        this.userRepository = userRepository;
        this.userService = userService;
        this.fileService = fileService;
        this.carAttachmentRepository = carAttachmentRepository;
    }

    @Override
    public CarWrapperResponse create(CarWrapperRequest carWrapperRequest) throws TransporationsException {
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> curUser = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

        Car newCar = setCarDataEntity(carWrapperRequest.getCarCommonModel());
        curUser.ifPresent(newCar::setUser);
        return saveCarCommon(carWrapperRequest);
    }

    @Override
    public CarWrapperResponse editCar(CarWrapperRequest carWrapperRequest) throws TransporationsException {
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> curUser = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

        Car car = setCarDataEntity(carWrapperRequest.getCarCommonModel());
        curUser.ifPresent(car::setUser);
        return saveCarCommon(carWrapperRequest);
    }

    @Override
    public CarWrapperResponse removeCar(Long carId) throws TransporationsException {
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> curUser = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

        CarWrapperRequest carWrapperRequest = new CarWrapperRequest();
        Car carCommonModel1 = carRepository.findCarByIdAndUserIdAndRemoveDatetimeIsNull(carId, curUser.get().getId());
        if (carCommonModel1 != null) {
            carCommonModel1.setRemoveDatetime(LocalDateTime.now());
            carWrapperRequest.setCarCommonModel(carCommonModel1.toModel());
        }
        return saveCarCommon(carWrapperRequest);
    }

    @Override
    public List<CarWrapperResponse> getCarCommonModels(User user) {
        List<CarWrapperResponse> carWrapperResponseList = new ArrayList<>();
        List<Car> car = carRepository.findAllByUserAndRemoveDatetimeIsNullOrderByCarBrandAscCarModelAsc(user);
        if (car != null) {
            for (Car c : car) {
                carWrapperResponseList.add(createResponseByCarModel(c.toModel()));
            }
        }
        return carWrapperResponseList;
    }

    @Override
    public CarWrapperResponse getMyCar(Long id, Long userId) {
        CarWrapperResponse carWrapperResponse = new CarWrapperResponse();
        Optional<AppUserModel> user = userService.getCurrentUser();
        Car car = carRepository.findCarByIdAndUserIdAndRemoveDatetimeIsNull(id, user.get().getId());
        if (car != null) {
            CarCommonModel carCommonModel = car.toModel();
            carWrapperResponse = createResponseByCarModel(carCommonModel);
        }
        return carWrapperResponse;
    }

    @Override
    public CarCommonModel saveCarCommon(CarCommonModel carCommonModel) {
        Car car = setCarDataEntity(carCommonModel);
        return carRepository.save(car).toModel();
    }

    private Car setCarDataEntity(CarCommonModel carCommonModel){
        Car car = carCommonModel.getId() != null ? carRepository.getOne(carCommonModel.getId()) : new Car();
        CarBrand carBrand = carBrandService.getEntityById(carCommonModel.getCarBrand().getId());
        CarModel carModel = carModelService.getEntityById(carCommonModel.getCarModel().getId());
        CarType carType = carTypeService.getEntityById(carCommonModel.getCarType().getId());
        car.setCarBrand(carBrand);
        car.setCarModel(carModel);
        car.setCarType(carType);
        car.setCarNumber(carCommonModel.getCarNumber());
        car.setCarryCapacity(carCommonModel.getCarryCapacity());
        car.setUser(userRepository.getOne(carCommonModel.getUserId()));
        return car;
    }

    private List<CarAttachment> createAttachment(List<MultipartFile> files, Car car) throws TransporationsException {
        List<CarAttachment> attachments = new ArrayList<>();
        for (MultipartFile file : files) {
            AppFile appFile = fileService.saveFile(file, FileType.IMG, UploadType.CAR);
            CarAttachment newAttachment = new CarAttachment();
            newAttachment.setAppFile(appFile);
            newAttachment.setCar(car);
            newAttachment = carAttachmentRepository.save(newAttachment);
            attachments.add(newAttachment);
        }
        return attachments;
    }

    private CarWrapperResponse createResponseByCarModel(CarCommonModel model) {
        CarWrapperResponse response;
        Car exist = carRepository.getOne(model.getId());
        List<CarAttachmentModel> attachmentModels = carAttachmentRepository.findAllByCar(exist).stream()
                .map(CarAttachment::toModel).collect(Collectors.toList());
        response = new CarWrapperResponse();
        response.setCarCommonModel(model);
        response.setAttachmentModels(attachmentModels);
        return response;
    }

    public CarWrapperResponse saveCarCommon(CarWrapperRequest carWrapperRequest) throws TransporationsException {
        CarWrapperResponse carWrapperResponse = new CarWrapperResponse();
        Car car = setCarDataEntity(carWrapperRequest.getCarCommonModel());
        Car saved = carRepository.save(car);
        carWrapperResponse.setCarCommonModel(saved.toModel());
        if (saved.getId() != null) {
            if (carWrapperRequest.getMultiUploadRequest() != null && !carWrapperRequest.getMultiUploadRequest().getFileList().isEmpty()) {
                List<CarAttachment> attachments = createAttachment(carWrapperRequest.getMultiUploadRequest().getFileList(), saved);
                carWrapperResponse.setAttachmentModels(attachments.stream().map(CarAttachment::toModel).collect(Collectors.toList()));
            }
        }
        return carWrapperResponse;
    }
}
