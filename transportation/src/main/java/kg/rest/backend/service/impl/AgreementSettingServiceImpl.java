package kg.rest.backend.service.impl;

import kg.rest.backend.dto.AgreementSettingModel;
import kg.rest.backend.repository.AgreementSettingRepository;
import kg.rest.backend.service.AgreementSettingService;
import org.springframework.stereotype.Service;

@Service
public class AgreementSettingServiceImpl implements AgreementSettingService {
    private final AgreementSettingRepository agreementSettingRepository;

    public AgreementSettingServiceImpl(AgreementSettingRepository agreementSettingRepository) {
        this.agreementSettingRepository = agreementSettingRepository;
    }

    @Override
    public AgreementSettingModel getFirstAgreement() {
        return agreementSettingRepository.findFirstByResponseHtmlIsNotNull().toModel();
    }
}
