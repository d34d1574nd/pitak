package kg.rest.backend.service.impl;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.dto.CarAttachmentModel;
import kg.rest.backend.entities.AppFile;
import kg.rest.backend.entities.Car;
import kg.rest.backend.entities.CarAttachment;
import kg.rest.backend.entities.User;
import kg.rest.backend.enums.FileType;
import kg.rest.backend.enums.UploadType;
import kg.rest.backend.payload.request.FileMultiUploadRequest;
import kg.rest.backend.repository.CarAttachmentRepository;
import kg.rest.backend.repository.CarRepository;
import kg.rest.backend.repository.FileRepository;
import kg.rest.backend.repository.UserRepository;
import kg.rest.backend.service.CarAttachmentService;
import kg.rest.backend.service.FileService;
import kg.rest.backend.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarAttachmentServiceImpl implements CarAttachmentService {
    private final CarAttachmentRepository attachmentRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final CarRepository carRepository;
    private final FileRepository fileRepository;
    private final FileService fileService;

    public CarAttachmentServiceImpl(CarAttachmentRepository attachmentRepository, UserService userService, UserRepository userRepository, CarRepository carRepository, FileRepository fileRepository, FileService fileService) {
        this.attachmentRepository = attachmentRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.carRepository = carRepository;
        this.fileRepository = fileRepository;
        this.fileService = fileService;
    }

    @Override
    public Boolean deleteByMyCar(Long carId, Long fileId) throws TransporationsException {
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> curUser = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

        Boolean deleted = false;
        if (!curUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден!");
        Car car = carRepository.findCarByIdAndUserIdAndRemoveDatetimeIsNull(carId, curUser.get().getId());
        AppFile appFile = fileRepository.getOne(fileId);
        CarAttachment attachment = attachmentRepository.findFirstByCarAndAppFile(car, appFile);
        if (attachment != null) {
            attachmentRepository.delete(attachment);
            deleted = fileService.deleteFile(fileId);
        } else {
            throw new TransporationsException("Такой фотографии не существует");
        }

        return deleted;
    }

    @Override
    public List<CarAttachmentModel> addMyCarPhoto(FileMultiUploadRequest multiUploadRequest, Long carId) throws Exception {
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> curUser = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());

        AppFile uploadedFile = null;
        Car car = null;
        List<CarAttachmentModel> attachmentModels = new ArrayList<>();
        if (curUser.isPresent()) {
            User usr = curUser.get();
            car = carRepository.findCarByIdAndUserIdAndRemoveDatetimeIsNull(carId, usr.getId());
            if (car == null)
                throw new Exception("Машина не найдена");

            for (MultipartFile file : multiUploadRequest.getFileList()
            ) {
                CarAttachment attachment = new CarAttachment();
                uploadedFile = fileService.saveFile(file, FileType.IMG, UploadType.CAR);
                attachment.setCar(car);
                attachment.setAppFile(uploadedFile);
                attachment = attachmentRepository.save(attachment);
                attachmentModels.add(attachment.toModel());
            }

        }
        return attachmentModels;
    }
}
