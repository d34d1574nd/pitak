package kg.rest.backend.service.impl;

import kg.rest.backend.dto.CarModelModel;
import kg.rest.backend.entities.dictionary.CarBrand;
import kg.rest.backend.entities.dictionary.CarModel;
import kg.rest.backend.repository.CarBrandRepository;
import kg.rest.backend.repository.CarModelRepository;
import kg.rest.backend.service.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarModelServiceImpl implements CarModelService {
    @Autowired
    private CarBrandRepository carBrandRepository;
    @Autowired
    private CarModelRepository carModelRepository;

    @Override
    public List<CarModelModel> getAllByParent(Long parentId) {
        Optional<CarBrand> carBrand = carBrandRepository.findById(parentId);
        List<CarModel> carModelList = null;
        List<CarModelModel> carModelModelList = new ArrayList<>();
        if(carBrand.isPresent()){
            carModelList = carModelRepository.findAllByCarBrandAndRemovedDateIsNullOrderByName(carBrand.get());
        }

        for (CarModel el: carModelList
             ) {
            carModelModelList.add(el.toModel());
        }
        return carModelModelList;
    }

    @Override
    public CarModel getEntityById(Long id) {
        return carModelRepository.getOne(id);
    }

    @Override
    public boolean existById(Long id) {
        return carModelRepository.existsById(id);
    }
}
