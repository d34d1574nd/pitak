package kg.rest.backend.service.impl;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppAdvertAttachmentModel;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.entities.AppAdvert;
import kg.rest.backend.entities.AppAdvertAttachment;
import kg.rest.backend.entities.AppFile;
import kg.rest.backend.entities.User;
import kg.rest.backend.enums.FileType;
import kg.rest.backend.enums.UploadType;
import kg.rest.backend.payload.request.FileMultiUploadRequest;
import kg.rest.backend.repository.AdvertAttachmentRepository;
import kg.rest.backend.repository.AppAdvertRepository;
import kg.rest.backend.repository.FileRepository;
import kg.rest.backend.repository.UserRepository;
import kg.rest.backend.service.AdvertAttachmentService;
import kg.rest.backend.service.FileService;
import kg.rest.backend.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AdvertAttachmentServiceImpl implements AdvertAttachmentService {
    private final AdvertAttachmentRepository attachmentRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final AppAdvertRepository appAdvertRepository;
    private final FileRepository fileRepository;
    private final FileService fileService;

    public AdvertAttachmentServiceImpl(AdvertAttachmentRepository attachmentRepository, UserService userService, UserRepository userRepository, AppAdvertRepository appAdvertRepository, FileRepository fileRepository, FileService fileService) {
        this.attachmentRepository = attachmentRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.appAdvertRepository = appAdvertRepository;
        this.fileRepository = fileRepository;
        this.fileService = fileService;
    }

    @Override
    public Boolean deleteByMyAdvert(Long advertId, Long fileId) throws TransporationsException {
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> curUser = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());
        Boolean deleted = false;
        if (!curUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден!");
        AppAdvert appAdvert = appAdvertRepository.findFirstByIdAndCreatedBy(advertId, curUser.get());
        AppFile appFile = fileRepository.getOne(fileId);
        AppAdvertAttachment attachment = attachmentRepository.findFirstByAdvertAndAppFile(appAdvert, appFile);
        if (attachment != null) {
            attachmentRepository.delete(attachment);
            deleted = fileService.deleteFile(fileId);
        } else {
            throw new TransporationsException("Такой фотографии не существует");
        }

        return deleted;
    }

    @Override
    public List<AppAdvertAttachmentModel> addMyAdvertPhoto(FileMultiUploadRequest multiUploadRequest, Long advertId) throws Exception {
        Optional<AppUserModel> currentUser = userService.getCurrentUser();
        if (!currentUser.isPresent())
            throw new TransporationsException("Текущий пользователь не найден");
        Optional<User> user = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());
        AppFile uploadedFile = null;
        AppAdvert appAdvert = null;
        List<AppAdvertAttachmentModel> attachmentModels = new ArrayList<>();
        if (user.isPresent()) {
            User usr = user.get();
            appAdvert = appAdvertRepository.findFirstByIdAndCreatedBy(advertId, usr);
            if (appAdvert == null)
                throw new Exception("Объявление не найдено");

            for (MultipartFile file : multiUploadRequest.getFileList()
            ) {
                AppAdvertAttachment attachment = new AppAdvertAttachment();
                uploadedFile = fileService.saveFile(file, FileType.IMG, UploadType.ADVERT);
                attachment.setAdvert(appAdvert);
                attachment.setAppFile(uploadedFile);
                attachment = attachmentRepository.save(attachment);
                attachmentModels.add(attachment.toModel());
            }

        }
        return attachmentModels;
    }
}
