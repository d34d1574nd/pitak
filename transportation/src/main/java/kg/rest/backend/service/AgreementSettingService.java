package kg.rest.backend.service;

import kg.rest.backend.dto.AgreementSettingModel;
import kg.rest.backend.dto.CountryModel;
import kg.rest.backend.entities.dictionary.Country;

import java.util.List;

public interface AgreementSettingService {
    AgreementSettingModel getFirstAgreement();
}
