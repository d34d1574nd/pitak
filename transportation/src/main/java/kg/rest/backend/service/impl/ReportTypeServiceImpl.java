package kg.rest.backend.service.impl;

import kg.rest.backend.dto.ReportTypeModel;
import kg.rest.backend.entities.dictionary.ReportType;
import kg.rest.backend.repository.ReportTypeRepository;
import kg.rest.backend.service.ReportTypeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportTypeServiceImpl implements ReportTypeService {
    private final ReportTypeRepository reportTypeRepository;

    public ReportTypeServiceImpl(ReportTypeRepository reportTypeRepository) {
        this.reportTypeRepository = reportTypeRepository;
    }

    @Override
    public List<ReportTypeModel> getAllActiveReportType() {
        return reportTypeRepository.findAllByRemovedDateIsNullOrderByName().stream().map(ReportType::toModel).collect(Collectors.toList());
    }

    @Override
    public ReportType getEntityReportType(Long reportTypeId) {
        return reportTypeRepository.getOne(reportTypeId);
    }
}
