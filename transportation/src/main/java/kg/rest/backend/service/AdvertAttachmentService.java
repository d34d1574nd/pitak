package kg.rest.backend.service;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppAdvertAttachmentModel;
import kg.rest.backend.payload.request.FileMultiUploadRequest;

import java.util.List;

public interface AdvertAttachmentService {
    Boolean deleteByMyAdvert(Long advertId, Long fileId) throws TransporationsException;

    List<AppAdvertAttachmentModel> addMyAdvertPhoto(FileMultiUploadRequest multiUploadRequest, Long advertId) throws Exception;
}
