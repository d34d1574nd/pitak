package kg.rest.backend.service.impl;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.entities.AppFile;
import kg.rest.backend.enums.FileType;
import kg.rest.backend.enums.UploadType;
import kg.rest.backend.repository.FileRepository;
import kg.rest.backend.service.FileService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;

    @Value("${upload.profile.img.path}")
    private String uploadPathProfile;

    @Value("${upload.advert.img.path}")
    private String uploadPathAdvert;

    @Value("${upload.car.img.path}")
    private String uploadPathCar;

    public FileServiceImpl(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    @Override
    public AppFile saveFile(MultipartFile fileMultiPart, FileType fileType,UploadType uploadType) throws TransporationsException {
        AppFile file = null;
        if(fileMultiPart!=null){
            File uploadFile = uploadFile(fileMultiPart,uploadType);
            if(uploadFile!=null){
                file = new AppFile();
                file.setPath(uploadFile.getPath());
                file.setName(uploadFile.getName());
                file.setFileType(fileType);
                file = fileRepository.save(file);
            }
        }
        return file;
    }

    @Override
    public boolean deleteFile(long fileId) throws TransporationsException {
        boolean isDeleted = false;
        try {
            AppFile currentFile = fileRepository.getOne(fileId);
            File fileFromDir = new File(currentFile.getPath());
            boolean isDeletedFromDir = FileUtils.deleteQuietly(fileFromDir);
            if(isDeletedFromDir){
                fileRepository.delete(currentFile);
                isDeleted = true;
            }
            return isDeleted;
        }catch (Exception ex){
            throw new TransporationsException(ex.getMessage());
        }
    }

    private File uploadFile(MultipartFile file,UploadType uploadType) throws TransporationsException {
        try {
            if(file.getSize()<=0L){
                throw new TransporationsException("Файл пустой");
            }
            String path;
            if(uploadType.equals(UploadType.ADVERT)){
                path = uploadPathAdvert;
            }
            else if(uploadType.equals(UploadType.CAR)){
                path = uploadPathCar;
            }
            else if(uploadType.equals(UploadType.PROFILE_PHOTO)){
                path = uploadPathProfile;
            }
            else{
                throw new TransporationsException("Не удалось распознать тип файла для сохрананения");
            }
            File uploadDir = new File(path);

            if(!uploadDir.exists()){
                uploadDir.mkdir();
            }
            String uuidFilename = UUID.randomUUID() + uploadType.name() + ".jpg";
            File forTransfer = new File(path + "/" + uuidFilename);
            file.transferTo(forTransfer);
            return forTransfer;
        }catch (IOException ex){
            throw new TransporationsException(ex.getMessage());
        }
    }
}
