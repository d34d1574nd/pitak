package kg.rest.backend.service;

import kg.rest.backend.dto.CarModelModel;
import kg.rest.backend.entities.dictionary.CarModel;

import java.util.List;

public interface CarModelService {
    List<CarModelModel> getAllByParent(Long parentId);
    CarModel getEntityById(Long id);
    boolean existById(Long id);
}
