package kg.rest.backend.service.impl;

import kg.rest.backend.dto.CountryModel;
import kg.rest.backend.entities.dictionary.Country;
import kg.rest.backend.repository.CountryRepository;
import kg.rest.backend.service.CountryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryServiceImpl implements CountryService {
    private final CountryRepository countryRepository;

    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> listCountries() {
        return countryRepository.findAll();
    }

    @Override
    public List<CountryModel> getAllCountry() {
        return countryRepository.findAllByRemovedDateIsNullOrderByName().stream().map(Country::toModel).collect(Collectors.toList());
    }

    @Override
    public CountryModel getCountryById(Long id) {
        return countryRepository.getOne(id).toModel();
    }

}
