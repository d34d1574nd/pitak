package kg.rest.backend.service.impl;

import kg.rest.backend.dto.CarBrandModel;
import kg.rest.backend.entities.dictionary.CarBrand;
import kg.rest.backend.repository.CarBrandRepository;
import kg.rest.backend.service.CarBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarBrandServiceImpl implements CarBrandService {
    @Autowired
    private CarBrandRepository carBrandRepository;

    @Override
    public List<CarBrandModel> getAll() {
        List<CarBrand> carBrands = carBrandRepository.findAllByRemovedDateIsNullOrderByName();
        List<CarBrandModel> carBrandModels = new ArrayList<>();
        for (CarBrand el: carBrands
             ) {
            carBrandModels.add(el.toModel());
        }
        return carBrandModels;
    }

    @Override
    public CarBrandModel getById(Long id) {
        Optional<CarBrand> carBrand = carBrandRepository.findById(id);
        CarBrandModel carBrandModel = null;
        if(carBrand.isPresent())
            carBrandModel = carBrand.get().toModel();
        return carBrandModel;
    }

    @Override
    public CarBrand getEntityById(Long id) {
        return carBrandRepository.getOne(id);
    }

    @Override
    public boolean existById(Long id) {
        return carBrandRepository.existsById(id);
    }
}
