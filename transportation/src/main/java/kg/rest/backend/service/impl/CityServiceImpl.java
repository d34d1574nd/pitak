package kg.rest.backend.service.impl;

import kg.rest.backend.dto.CityModel;
import kg.rest.backend.entities.dictionary.City;
import kg.rest.backend.entities.dictionary.Country;
import kg.rest.backend.repository.CityRepository;
import kg.rest.backend.repository.CountryRepository;
import kg.rest.backend.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CityServiceImpl implements CityService {
    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Override
    @Transactional
    public List<CityModel> getByParent(Long parentId) {
        Country country = countryRepository.getOne(parentId);
        return cityRepository.findAllByCountryAndRemovedDateIsNullOrderByName(country).stream().map(City::toModel).collect(Collectors.toList());
    }
}
