package kg.rest.backend.dto;


import kg.rest.backend.enums.UserType;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.Collection;

@Data
@Builder
public class AppUserModel {
    private Long id;
    private String surname;
    private String name;
    private String patronymic;
    private String username;
    @Email
    private String email;
    private String password;
    private UserType userType;
    private LocalDateTime cts;
    private LocalDateTime uts;
    private Long editorId;
    private LocalDateTime removeDatetime;
    private Boolean enabled;
    private AppFileModel profilePhoto;
    private CountryModel countryModel;
    private CityModel cityModel;
}
