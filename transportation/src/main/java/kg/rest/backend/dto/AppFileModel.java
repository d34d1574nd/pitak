package kg.rest.backend.dto;


import kg.rest.backend.enums.FileType;
import kg.rest.backend.util.Constants;
import lombok.*;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppFileModel {
    private Long id;
    private String name;
    private String path;
    private FileType fileType;
    private LocalDateTime cts;
    private String url;

    public String getUrl() {
        Constants constants = new Constants();
        if(path.contains("profile"))
            return Constants.HOSTNAME + ":" + Constants.SERVER_PORT + Constants.PROFILE_IMG_PATH + "/" + this.name;
        else if(path.contains("advert"))
            return Constants.HOSTNAME + ":" + Constants.SERVER_PORT + Constants.ADVERT_IMG_PATH + "/" + this.name;
        else if(path.contains("car"))
            return Constants.HOSTNAME + ":" + Constants.SERVER_PORT + Constants.CAR_IMG_PATH + "/" + this.name;
        else
            return "Не удалось распознать тип файла для сохрананения";
    }
}
