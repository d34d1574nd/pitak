package kg.rest.backend.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Transient;

@Getter
@Setter
public class PushNotificationModel {
    private String to;
    private Notification notification;

    @Getter
    @Setter
    @JsonIgnoreProperties
    public class Notification{
        private String title;
        private String body;
    }
}
