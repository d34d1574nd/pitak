package kg.rest.backend.dto;


import kg.rest.backend.entities.dictionary.TypeService;
import kg.rest.backend.enums.AdvertStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppAdvertModel {
    private Long id;
    @NotNull
    private String title;
    @NotNull
    private String text;
    @NotNull
    private Integer amountPayment;
    @NotNull
    private Integer numberOfSeat;
    @NotNull
    private String fromPlace;
    @NotNull
    private String toPlace;
    @NotNull
    private TypeService typeService;
    @NotNull
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime sendDateTime;
    @NotNull
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime arrivalDateTime;
    private Long createdBy;
    private LocalDateTime cts;
    private LocalDateTime uts;
    private LocalDateTime removeDate;
    private Boolean enabled;
    private LocalDateTime checkedTime;
    private String mobileNumber;
    private CarCommonModel carCommonModel;
    private List<MultipartFile> fileList;
    private List<AppFileModel> appFiles;
    private String responseFcm;
    private AdvertStatus advertStatus;
    private Boolean isFavorite;
}
