package kg.rest.backend.dto;

import lombok.Data;

@Data
public class ResponseFcm {
    private Long message_id;
}
