package kg.rest.backend.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppAdvertAttachmentModel {
    @JsonIgnore
    private Long id;

    @JsonIgnore
    private AppAdvertModel advert;

    private AppFileModel appFile;
}
