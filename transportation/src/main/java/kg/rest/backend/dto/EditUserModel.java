package kg.rest.backend.dto;

import kg.rest.backend.enums.UserType;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class EditUserModel {
    @NotNull
    private Long id;

    @NotBlank
    @Size(min = 2, max = 40)
    private String surname;

    @NotBlank
    @Size(min = 2, max = 40)
    private String name;

    private String patronymic;

    @NotBlank
    @Size(min = 6, max = 20)
    private String username;

    @Size(max = 50)
    @Email
    private String email;

    @NotNull
    private UserType userType;

    private String newPassword;

    private CountryModel countryModel;

    private CityModel cityModel;

    CarCommonModel carCommonModel;
}
