package kg.rest.backend.payload.response;

import kg.rest.backend.dto.AppAdvertAttachmentModel;
import kg.rest.backend.dto.AppAdvertModel;
import lombok.Data;

import java.util.List;

@Data
public class AdvertWrapperResponse {
    private AppAdvertModel appAdvertModel;
    private List<AppAdvertAttachmentModel> attachmentModels;
}
