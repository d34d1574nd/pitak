package kg.rest.backend.payload.request;

import kg.rest.backend.dto.AppFileModel;
import kg.rest.backend.entities.dictionary.TypeService;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class AdvertSearchPatternRequest {
    private Long byCreatedUserId;
    private String fromPlace;
    private String toPlace;
    private LocalDateTime fromDateTime;
    private LocalDateTime toDateTime;
    private List<TypeService> type;
    private List<MultipartFile> fileList;
    private List<AppFileModel> appFiles;
    private Integer amountPayment;
    private String title;
    private String text;
    private Boolean enabled;
    private Boolean isNullRemoveDate;
}
