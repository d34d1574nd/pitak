package kg.rest.backend.payload.request;

import kg.rest.backend.dto.AppAdvertModel;
import lombok.Data;

@Data
public class AdvertWrapperRequest {
    private AppAdvertModel appAdvertModel;
    private FileMultiUploadRequest multiUploadRequest;
}
