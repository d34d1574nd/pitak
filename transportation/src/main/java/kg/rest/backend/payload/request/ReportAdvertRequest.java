package kg.rest.backend.payload.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ReportAdvertRequest {
    @NotNull
    private Long advertId;
    @NotNull
    private Long reportTypeId;
    private String additionalDescription;
}
