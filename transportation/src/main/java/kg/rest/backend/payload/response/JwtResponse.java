package kg.rest.backend.payload.response;

import kg.rest.backend.dto.AppFileModel;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.dto.CarCommonModel;
import kg.rest.backend.enums.UserType;
import lombok.Data;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
@Data
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String surname;
	private String name;
	private String patronymic;
	private String username;
	private String email;
	private String password;
	private UserType userType;
	private LocalDateTime cts;
	private LocalDateTime uts;
	private Long editorId;
	private LocalDateTime removeDatetime;
	private Boolean enabled;
	private AppFileModel profilePhoto;
	private List<String> roles;

	public JwtResponse(String accessToken, List<String> roles, AppUserModel appUserModel) {
		this.token = accessToken;
		this.id = appUserModel.getId();
		this.username = appUserModel.getUsername();
		this.name = appUserModel.getName();
		this.surname = appUserModel.getSurname();
		this.patronymic = appUserModel.getPatronymic();
		this.password = appUserModel.getPassword();
		this.cts = appUserModel.getCts();
		this.userType = appUserModel.getUserType();
		this.uts = appUserModel.getUts();
		this.editorId = appUserModel.getId();
		this.removeDatetime = appUserModel.getRemoveDatetime();
		this.enabled = appUserModel.getEnabled();
		this.profilePhoto = appUserModel.getProfilePhoto()!=null?appUserModel.getProfilePhoto():null;
		this.email = appUserModel.getEmail();
		this.roles = roles;
	}
}
