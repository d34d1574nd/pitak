package kg.rest.backend.payload.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class FileUploadRequest {
	@NotNull
	private MultipartFile file;
}
