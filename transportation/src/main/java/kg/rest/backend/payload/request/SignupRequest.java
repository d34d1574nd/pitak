package kg.rest.backend.payload.request;

import kg.rest.backend.dto.CarCommonModel;
import kg.rest.backend.dto.CityModel;
import kg.rest.backend.dto.CountryModel;
import kg.rest.backend.enums.UserType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignupRequest {
    @NotBlank
    @Size(min = 6, max = 20)
    private String username;
 
    @Size(max = 50)
    @Email
    private String email;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    @NotBlank
    @Size(min = 2, max = 40)
    private String surname;

    @NotBlank
    @Size(min = 2, max = 40)
    private String name;

    private String patronymic;

    @Enumerated
    @NotNull
    private UserType userType;

    private MultipartFile profilePhoto;

    private CarCommonModel carCommonModel;

    private Boolean enabled;

    private CityModel cityModel;

    private CountryModel countryModel;
}
