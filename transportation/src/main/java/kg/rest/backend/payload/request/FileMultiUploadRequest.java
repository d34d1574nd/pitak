package kg.rest.backend.payload.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class FileMultiUploadRequest {
	@NotNull
	private List<MultipartFile> fileList;
}
