package kg.rest.backend.payload.request;

import kg.rest.backend.dto.CarCommonModel;
import lombok.Data;

@Data
public class CarWrapperRequest {
    private CarCommonModel carCommonModel;
    private FileMultiUploadRequest multiUploadRequest;
}
