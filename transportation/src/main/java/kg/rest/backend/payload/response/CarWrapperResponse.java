package kg.rest.backend.payload.response;

import kg.rest.backend.dto.CarAttachmentModel;
import kg.rest.backend.dto.CarCommonModel;
import lombok.Data;

import java.util.List;

@Data
public class CarWrapperResponse {
    private CarCommonModel carCommonModel;
    private List<CarAttachmentModel> attachmentModels;
}
