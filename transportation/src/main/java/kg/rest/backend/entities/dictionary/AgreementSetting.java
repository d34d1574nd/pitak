package kg.rest.backend.entities.dictionary;

import kg.rest.backend.dto.AgreementSettingModel;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "AGREEMENT_SETTING")
@Data
public class AgreementSetting {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AGREEMENT_SETTING_SEQ")
    @SequenceGenerator(name = "AGREEMENT_SETTING_SEQ", sequenceName = "AGREEMENT_SETTING_SEQ", allocationSize = 1)
    private Long id;

    @Column(columnDefinition="TEXT")
    private String responseHtml;

    public AgreementSettingModel toModel() {
        return AgreementSettingModel.builder()
                .id(this.id)
                .responseHtml(this.responseHtml)
                .build();
    }
}
