package kg.rest.backend.entities.dictionary;

import kg.rest.backend.dto.CityModel;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "DICT_CITY")
@Data
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CITY_SEQ")
    @SequenceGenerator(name = "CITY_SEQ", sequenceName = "CITY_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "countryId", nullable = false)
    private Country country;

    @Column(name = "REMOVED_DATE")
    private LocalDateTime removedDate;

    public CityModel toModel() {
        return CityModel.builder()
                .id(this.id)
                .name(this.name)
                .countryId(this.country!=null?country.getId():null)
                .removedDate(this.removedDate!=null?this.removedDate:null)
                .build();
    }
}
