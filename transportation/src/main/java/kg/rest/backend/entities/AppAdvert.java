package kg.rest.backend.entities;


import kg.rest.backend.dto.AppAdvertModel;
import kg.rest.backend.entities.dictionary.TypeService;
import kg.rest.backend.enums.AdvertStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "APP_ADVERT")
@Data
@NoArgsConstructor
public class AppAdvert {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_ADVERT_SEQ")
    @SequenceGenerator(name = "APP_ADVERT_SEQ", sequenceName = "APP_ADVERT_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "AMOUNT_PAYMENT")
    private Integer amountPayment;

    @Column(name = "NUMBER_OF_SEAT")
    private Integer numberOfSeat;

    @Column(name = "FROM_PLACE")
    private String fromPlace;

    @Column(name = "TO_PLACE")
    private String toPlace;

    @OneToOne
    @JoinColumn(name = "TYPE_SERVICE_ID", referencedColumnName = "id", nullable = true)
    private TypeService typeService;

    @Column(name = "SEND_DATETIME")
    private LocalDateTime sendDateTime;

    @Column(name = "ARRIVAL_DATETIME")
    private LocalDateTime arrivalDateTime;

    @Column(name = "CTS")
    @CreationTimestamp
    private LocalDateTime cts;

    @Column(name = "UTS")
    @UpdateTimestamp
    private LocalDateTime uts;

    @Column(name = "REMOVE_DATE")
    private LocalDateTime removeDate;

    @Column(name = "ENABLED")
    private Boolean enabled;

    @Column(name="CHECKED_TIME")
    private LocalDateTime checkedTime;

    @Column(name = "response_fcm")
    private String responseFcm;

    @Enumerated
    @Column(name = "STATUS")
    private AdvertStatus advertStatus;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATED_BY_ID")
    private User createdBy;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name ="car_id")
    private Car car;

    public AppAdvert (AppAdvertModel appAdvertModel){
        this.setTitle(appAdvertModel.getTitle());
        this.setText(appAdvertModel.getText());
        this.setTypeService(appAdvertModel.getTypeService());
        this.setAmountPayment(appAdvertModel.getAmountPayment());
        this.setFromPlace(appAdvertModel.getFromPlace());
        this.setToPlace(appAdvertModel.getToPlace());
        this.setNumberOfSeat(appAdvertModel.getNumberOfSeat());
        this.setSendDateTime(appAdvertModel.getSendDateTime());
        this.setArrivalDateTime(appAdvertModel.getArrivalDateTime());
    }
    public AppAdvertModel toModel() {
        return AppAdvertModel.builder()
                .id(this.id)
                .title(this.title)
                .text(this.text)
                .amountPayment(this.amountPayment)
                .numberOfSeat(this.numberOfSeat)
                .fromPlace(this.fromPlace)
                .toPlace(this.toPlace)
                .typeService(this.typeService)
                .removeDate(this.removeDate)
                .enabled(this.enabled)
                .cts(this.cts)
                .uts(this.uts)
                .checkedTime(this.checkedTime)
                .createdBy(this.createdBy != null ? this.createdBy.getId() : null)
                .sendDateTime(this.sendDateTime)
                .arrivalDateTime(this.arrivalDateTime)
                .responseFcm(this.responseFcm)
                .advertStatus(this.advertStatus)
                .carCommonModel(this.car!=null?this.car.toModel():null)
                .build();
    }

    public AppAdvertModel toModel(Boolean hasFavorite) {
        return AppAdvertModel.builder()
                .id(this.id)
                .title(this.title)
                .text(this.text)
                .amountPayment(this.amountPayment)
                .numberOfSeat(this.numberOfSeat)
                .fromPlace(this.fromPlace)
                .toPlace(this.toPlace)
                .typeService(this.typeService)
                .removeDate(this.removeDate)
                .enabled(this.enabled)
                .cts(this.cts)
                .uts(this.uts)
                .checkedTime(this.checkedTime)
                .createdBy(this.createdBy != null ? this.createdBy.getId() : null)
                .sendDateTime(this.sendDateTime)
                .arrivalDateTime(this.arrivalDateTime)
                .responseFcm(this.responseFcm)
                .advertStatus(this.advertStatus)
                .isFavorite(hasFavorite)
                .carCommonModel(this.car!=null?this.car.toModel():null)
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppAdvert appAdvert = (AppAdvert) o;
        return id.equals(appAdvert.id) &&
                title.equals(appAdvert.title) &&
                text.equals(appAdvert.text) &&
                Objects.equals(amountPayment, appAdvert.amountPayment) &&
                Objects.equals(numberOfSeat, appAdvert.numberOfSeat) &&
                fromPlace.equals(appAdvert.fromPlace) &&
                toPlace.equals(appAdvert.toPlace) &&
                typeService == appAdvert.typeService &&
                sendDateTime.equals(appAdvert.sendDateTime) &&
                arrivalDateTime.equals(appAdvert.arrivalDateTime) &&
                cts.equals(appAdvert.cts) &&
                uts.equals(appAdvert.uts) &&
                Objects.equals(removeDate, appAdvert.removeDate) &&
                Objects.equals(enabled, appAdvert.enabled) &&
                Objects.equals(checkedTime, appAdvert.checkedTime) &&
                responseFcm.equals(appAdvert.responseFcm) &&
                advertStatus.equals(appAdvert.advertStatus) &&
                createdBy.equals(appAdvert.createdBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, text, amountPayment, numberOfSeat, fromPlace, toPlace, typeService, sendDateTime, cts, uts,
                removeDate, enabled, checkedTime, createdBy,responseFcm,advertStatus);
    }

    @Override
    public String toString() {
        return "AppAdvert{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", amountPayment='" + amountPayment + '\'' +
                ", numberOfSeat=" + numberOfSeat +
                ", fromPlace='" + fromPlace + '\'' +
                ", toPlace='" + toPlace + '\'' +
                ", advertType=" + typeService +
                ", sendDateTime=" + sendDateTime +
                ", arrivalDateTime=" + arrivalDateTime +
                ", cts=" + cts +
                ", uts=" + uts +
                ", removeDate=" + removeDate +
                ", enabled=" + enabled +
                ", checkedTime=" + checkedTime +
                ", createdBy=" + createdBy +
                ", responseFcm=" + responseFcm +
                ", advertStatus=" + advertStatus +
                '}';
    }
}
