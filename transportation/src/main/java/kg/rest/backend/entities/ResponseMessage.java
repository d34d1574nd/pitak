package kg.rest.backend.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import kg.rest.backend.enums.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseMessage<T> {
    private T result;
    private ResultCode resultCode;
    private String details;
}
