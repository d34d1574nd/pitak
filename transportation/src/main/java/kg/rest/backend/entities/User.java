package kg.rest.backend.entities;

import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.entities.dictionary.City;
import kg.rest.backend.entities.dictionary.Country;
import kg.rest.backend.enums.UserType;
import kg.rest.backend.payload.request.SignupRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_USER_SEQ")
	@SequenceGenerator(name = "APP_USER_SEQ", sequenceName = "APP_USER_SEQ", allocationSize = 1)
	private Long id;

	@NotBlank
	@Size(max = 20)
	private String username;


	@Size(max = 50)
	@Email
	private String email;

	@NotBlank
	@Size(max = 120)
	private String password;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "user_roles", 
				joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

	@Column(name = "SURNAME")
	private String surname;

	@Column(name = "NAME")
	private String name;

	@Column(name = "PATRONYMIC")
	private String patronymic;

	@Column(name = "TYPE")
	@Enumerated
	private UserType userType;



	@Column(name = "CTS")
	private LocalDateTime cts;

	@Column(name = "UTS")
	private LocalDateTime uts;

	@Column(name="EDITOR_ID")
	private Long editorId;

	@JoinColumn(name = "COUNTRY_ID")
	@ManyToOne(fetch = FetchType.EAGER)
	private Country country;

	@JoinColumn(name = "CITY_ID")
	@ManyToOne(fetch = FetchType.EAGER)
	private City city;

	@Column(name = "REMOVE_DATETIME")
	private LocalDateTime removeDatetime;

	@Column(name = "enabled")
	private Boolean enabled;

	@JoinColumn(name = "profile_photo_id")
	@ManyToOne(fetch = FetchType.EAGER)
	private AppFile profilePhoto;

	public User(SignupRequest signUpRequest){
		this.setUsername(signUpRequest.getUsername());
		this.setSurname(signUpRequest.getSurname());
		this.setName(signUpRequest.getName());
		this.setPatronymic(signUpRequest.getPatronymic());
		this.setEmail(signUpRequest.getEmail());
		this.setCts(LocalDateTime.now());
		this.setEnabled(true);
		this.setUserType(signUpRequest.getUserType());
	}

	public AppUserModel toModel() {
		return AppUserModel.builder()
				.id(this.id)
				.surname(this.surname)
				.name(this.name)
				.patronymic(this.patronymic)
				.username(this.username)
				.email(this.email)
				.password(this.password)
				.userType(this.userType)
				.cts(this.cts)
				.uts(this.uts)
				.editorId(this.editorId)
				.removeDatetime(this.removeDatetime)
				.enabled(this.enabled)
				.profilePhoto(this.profilePhoto!=null?this.profilePhoto.toModel():null)
				.cityModel(this.city!=null?this.city.toModel():null)
				.countryModel(this.country!=null?this.country.toModel():null)
				.build();
	}
}


