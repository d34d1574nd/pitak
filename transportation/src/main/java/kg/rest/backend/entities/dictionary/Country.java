package kg.rest.backend.entities.dictionary;

import kg.rest.backend.dto.CountryModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "DICT_COUNTRY")
@Data
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COUNTRY_SEQ")
    @SequenceGenerator(name = "COUNTRY_SEQ", sequenceName = "COUNTRY_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "REMOVED_DATE")
    private LocalDateTime removedDate;

    public CountryModel toModel() {
        return CountryModel.builder()
                .id(this.id)
                .name(this.name)
                .removedDate(this.removedDate!=null?this.removedDate:null)
                .build();
    }
}
