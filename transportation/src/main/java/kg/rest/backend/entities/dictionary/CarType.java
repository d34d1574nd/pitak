package kg.rest.backend.entities.dictionary;

import kg.rest.backend.dto.CarTypeModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "DICT_CAR_TYPE")
@Data
public class CarType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DICT_CAR_TYPE_SEQ")
    @SequenceGenerator(name = "DICT_CAR_TYPE_SEQ", sequenceName = "DICT_CAR_TYPE_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "REMOVED_DATE")
    private LocalDateTime removedDate;

    public CarTypeModel toModel() {
        return CarTypeModel.builder()
                .id(this.id)
                .name(this.name)
                .removedDate(this.removedDate!=null?this.removedDate:null)
                .build();
    }
}
