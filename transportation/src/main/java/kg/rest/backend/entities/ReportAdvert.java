package kg.rest.backend.entities;

import kg.rest.backend.dto.ReportAdvertModel;
import kg.rest.backend.entities.dictionary.ReportType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(	name = "REPORT_ADVERT")
public class ReportAdvert {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REPORT_ADVERT_SEQ")
	@SequenceGenerator(name = "REPORT_ADVERT_SEQ", sequenceName = "REPORT_ADVERT_SEQ", allocationSize = 1)
	private Long id;

	@JoinColumn(name = "CREATED_BY_ID")
	@ManyToOne(fetch = FetchType.EAGER)
	private User createdBy;

	@JoinColumn(name = "ADVERT_ID")
	@ManyToOne(fetch = FetchType.EAGER)
	private AppAdvert advert;

	@JoinColumn(name = "REPORT_TYPE_ID")
	@ManyToOne(fetch = FetchType.EAGER)
	private ReportType reportType;

	@Column(name = "ADD_DESCRIPTION")
	private String additionalDescription;

	@Column(name = "SOLVED")
	private Boolean solved;

	@Column(name = "CTS")
	private LocalDateTime cts;

	public ReportAdvertModel toModel() {
		return ReportAdvertModel.builder()
				.id(this.id)
				.createdBy(this.createdBy!=null?createdBy.toModel():null)
				.advert(this.advert!=null?advert.toModel():null)
				.reportType(this.reportType!=null?reportType.toModel():null)
				.additionalDescription(this.additionalDescription)
				.cts(this.cts)
				.solved(this.solved)
				.build();
	}
}


