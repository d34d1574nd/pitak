package kg.rest.backend.entities.dictionary;

import kg.rest.backend.dto.ReportTypeModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "DICT_REPORT_TYPE")
@Data
public class ReportType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REPORT_TYPE_SEQ")
    @SequenceGenerator(name = "REPORT_TYPE_SEQ", sequenceName = "REPORT_TYPE_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "REMOVED_DATE")
    private LocalDateTime removedDate;

    public ReportTypeModel toModel() {
        return ReportTypeModel.builder()
                .id(this.id)
                .name(this.name)
                .removedDate(this.removedDate!=null?this.removedDate:null)
                .build();
    }
}
