package kg.rest.backend.entities;


import kg.rest.backend.dto.AppFileModel;
import kg.rest.backend.enums.FileType;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "FILE")
@Data
public class AppFile {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILE_SEQ")
    @SequenceGenerator(name = "FILE_SEQ", sequenceName = "FILE_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PATH")
    private String path;

    @Enumerated
    @Column(name = "FILE_TYPE")
    private FileType fileType;

    @Column(name = "CTS")
    @CreationTimestamp
    private LocalDateTime cts;

    public AppFileModel toModel(){
        return AppFileModel.builder()
                .id(this.id)
                .name(this.name)
                .path(this.path)
                .fileType(this.fileType)
                .cts(this.cts)
                .build();
    }

    @Override
    public String toString() {
        return "AppFile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", fileType=" + fileType +
                ", cts=" + cts +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppFile appFile = (AppFile) o;
        return id.equals(appFile.id) &&
                name.equals(appFile.name) &&
                path.equals(appFile.path) &&
                fileType == appFile.fileType &&
                cts.equals(appFile.cts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, path, fileType, cts);
    }
}
