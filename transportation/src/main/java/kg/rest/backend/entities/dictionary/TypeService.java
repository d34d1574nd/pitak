package kg.rest.backend.entities.dictionary;

import kg.rest.backend.dto.TypeServiceModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "DICT_TYPE_SERVICE")
@Data
public class TypeService {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DICT_TYPE_SERVICE_SEQ")
    @SequenceGenerator(name = "DICT_TYPE_SERVICE_SEQ", sequenceName = "DICT_TYPE_SERVICE_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "REMOVED_DATE")
    private LocalDateTime removedDate;

    public TypeServiceModel toModel() {
        return TypeServiceModel.builder()
                .id(this.id)
                .name(this.name)
                .removedDate(this.removedDate!=null?this.removedDate:null)
                .build();
    }
}
