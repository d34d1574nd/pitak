package kg.rest.backend.entities;

public enum ERole {
    ROLE_DRIVER,
    ROLE_PASSENGER,
}
