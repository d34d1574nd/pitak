package kg.rest.backend.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "FAVORITES")
public class Favorites {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FAVORITES_SEQ")
    @SequenceGenerator(name = "FAVORITES_SEQ", sequenceName = "FAVORITES_SEQ", allocationSize = 1)
    private Long id;

    @NotNull
    private Long userId;

    @NotNull
    private Long advertId;
}
