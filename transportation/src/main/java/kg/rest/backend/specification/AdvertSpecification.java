package kg.rest.backend.specification;

import kg.rest.backend.entities.AppAdvert;
import kg.rest.backend.payload.request.AdvertSearchPatternRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class AdvertSpecification  implements Specification<AppAdvert> {
    private AdvertSearchPatternRequest patternRequest;

    public AdvertSpecification(AdvertSearchPatternRequest request){
        this.patternRequest = request;
    }
    @Override
    public Predicate toPredicate(Root<AppAdvert> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        final List<Predicate> predicates = new ArrayList<>();

        if(patternRequest.getTitle()!=null && !patternRequest.getTitle().isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(
                    root.get("title"))
                    ,getContainsLikePattern(patternRequest.getTitle())));
        }

        if(patternRequest.getText()!=null && !patternRequest.getText().isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(
                    root.get("text"))
                    ,getContainsLikePattern(patternRequest.getText())));
        }

        if(patternRequest.getAmountPayment()!=null){
            predicates.add(criteriaBuilder.equal(
                    root.get("amountPayment"),
                    patternRequest.getAmountPayment()
                    )
            );
        }

        if(patternRequest.getFromDateTime()!=null && patternRequest.getToDateTime()!=null){
            predicates.add(criteriaBuilder.between(
                    root.get("cts")
                    ,patternRequest.getFromDateTime()
                    ,patternRequest.getToDateTime())
            );
        }
        else if(patternRequest.getFromDateTime()!=null){
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    root.get("cts")
                    ,patternRequest.getFromDateTime())
            );
        }
        else if(patternRequest.getToDateTime()!=null){
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    root.get("cts")
                    ,patternRequest.getToDateTime())
            );
        }

        if(patternRequest.getFromPlace()!=null && !patternRequest.getFromPlace().isEmpty()){
predicates.add(criteriaBuilder.like(criteriaBuilder.lower(
        root.get("fromPlace"))
        ,getContainsLikePattern(patternRequest.getFromPlace())));
        }

        if(patternRequest.getToPlace()!=null && !patternRequest.getToPlace().isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(
                    root.get("toPlace"))
                    ,getContainsLikePattern(patternRequest.getToPlace())));
        }

        if(patternRequest.getType()!=null){
            Expression<String> exp = root.get("typeService");
            predicates.add(exp.in(patternRequest.getType()));
        }

        if(patternRequest.getByCreatedUserId()!=null){
            predicates.add(criteriaBuilder.equal(root.get("createdBy").get("id"),patternRequest.getByCreatedUserId()));
        }

        if(patternRequest.getEnabled()!=null){
            predicates.add(criteriaBuilder.equal(root.get("enabled"),patternRequest.getEnabled()));
        }
        if(patternRequest.getIsNullRemoveDate()!=null){
            if(patternRequest.getIsNullRemoveDate()){
                predicates.add(criteriaBuilder.isNull(root.get("removeDate")));
            }
            else
                predicates.add(criteriaBuilder.isNotNull(root.get("removeDate")));
        }






        return  criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
    public static String getContainsLikePattern(String searchTerm) {
        return searchTerm == null || searchTerm.isEmpty() ? "%" : "%" + searchTerm.toLowerCase() + "%";
    }
}
