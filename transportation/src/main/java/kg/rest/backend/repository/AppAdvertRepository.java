package kg.rest.backend.repository;

import kg.rest.backend.entities.AppAdvert;
import kg.rest.backend.entities.User;
import kg.rest.backend.entities.dictionary.TypeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppAdvertRepository extends JpaRepository<AppAdvert,Long>, JpaSpecificationExecutor<AppAdvert> {
    List<AppAdvert> findAllByIdIn(List<Long> advertId);
    List<AppAdvert> findAllByTitleContainsAndIdIn(String title, List<Long> advertId);
    Boolean existsByIdAndEnabled(Long id, Boolean enabled);
    Boolean existsByIdAndRemoveDateIsNotNull(Long id);
    List<AppAdvert> findAllByResponseFcmIsNullAndEnabled(Boolean enabled);
    Page<AppAdvert> findAllByTypeServiceAndEnabled(TypeService typeService, Boolean enabled, Pageable pageable);
    AppAdvert findFirstByIdAndCreatedBy(long advertId, User createdBy);
}
