package kg.rest.backend.repository;

import kg.rest.backend.entities.Favorites;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavoritesRepository extends JpaRepository<Favorites,Long>, JpaSpecificationExecutor<Favorites> {
      List<Favorites> findAllByUserId(Long userId);
      Integer removeByAdvertIdAndUserId(Long advertId,Long userId);
      List<Favorites> findAllByAdvertIdAndUserId(Long advertId, Long userId);
      Boolean existsAllByAdvertIdAndUserId(Long advertId, Long userId);
}
