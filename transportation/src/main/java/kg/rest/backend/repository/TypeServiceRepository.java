package kg.rest.backend.repository;

import kg.rest.backend.entities.dictionary.TypeService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypeServiceRepository extends JpaRepository<TypeService, Long> {
    List<TypeService> findAllByRemovedDateIsNullOrderByName();
    List<TypeService> findAllByIdIsNotIn(Long id);
}
