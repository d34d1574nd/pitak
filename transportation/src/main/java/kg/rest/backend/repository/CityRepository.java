package kg.rest.backend.repository;

import kg.rest.backend.entities.dictionary.City;
import kg.rest.backend.entities.dictionary.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends JpaRepository<City,Long>, JpaSpecificationExecutor<City> {
    List<City> findAllByCountryAndRemovedDateIsNullOrderByName(Country country);
}
