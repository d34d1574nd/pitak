package kg.rest.backend.repository;

import kg.rest.backend.entities.dictionary.CarType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarTypeRepository extends JpaRepository<CarType, Long> {
    List<CarType> findAllByRemovedDateIsNullOrderByName();
}
