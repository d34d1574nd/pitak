package kg.rest.backend.repository;

import kg.rest.backend.entities.dictionary.CarBrand;
import kg.rest.backend.entities.dictionary.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarModelRepository extends JpaRepository<CarModel, Long> {
	List<CarModel> findAllByCarBrandAndRemovedDateIsNullOrderByName(CarBrand carBrand);
}
