package kg.rest.backend.repository;

import kg.rest.backend.entities.AppFile;
import kg.rest.backend.entities.Car;
import kg.rest.backend.entities.CarAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarAttachmentRepository extends JpaRepository<CarAttachment, Long>, JpaSpecificationExecutor<CarAttachment> {
    CarAttachment findFirstByCarAndAppFile(Car car, AppFile file);

    List<CarAttachment> findAllByCar(Car car);
}
