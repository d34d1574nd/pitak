package kg.rest.backend.repository;

import kg.rest.backend.entities.AppAdvert;
import kg.rest.backend.entities.AppAdvertAttachment;
import kg.rest.backend.entities.AppFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdvertAttachmentRepository extends JpaRepository<AppAdvertAttachment, Long>, JpaSpecificationExecutor<AppAdvertAttachment> {
    AppAdvertAttachment findFirstByAdvertAndAppFile(AppAdvert advert, AppFile file);

    List<AppAdvertAttachment> findAllByAdvert(AppAdvert advert);
}
