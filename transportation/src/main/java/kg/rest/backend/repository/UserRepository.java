package kg.rest.backend.repository;

import java.util.Optional;

import kg.rest.backend.enums.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import kg.rest.backend.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsernameAndUserType(String username, UserType userType);

    Boolean existsByUsernameAndUserType(String username, UserType userType);

    Boolean existsByEmail(String email);
}
