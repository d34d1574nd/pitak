package kg.rest.backend.repository;

import kg.rest.backend.entities.dictionary.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends JpaRepository<Country,Long>, JpaSpecificationExecutor<Country> {
    List<Country> findAllByRemovedDateIsNullOrderByName();
}
