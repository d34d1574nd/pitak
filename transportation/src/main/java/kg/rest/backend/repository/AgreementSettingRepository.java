package kg.rest.backend.repository;

import kg.rest.backend.entities.dictionary.AgreementSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AgreementSettingRepository extends JpaRepository<AgreementSetting,Long>, JpaSpecificationExecutor<AgreementSetting> {
    AgreementSetting findFirstByResponseHtmlIsNotNull();
}
