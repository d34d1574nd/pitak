package kg.rest.backend.repository;

import kg.rest.backend.entities.dictionary.CarBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarBrandRepository extends JpaRepository<CarBrand, Long> {
    List<CarBrand> findAllByRemovedDateIsNullOrderByName();
}
