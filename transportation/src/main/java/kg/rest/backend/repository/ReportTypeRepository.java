package kg.rest.backend.repository;

import kg.rest.backend.entities.dictionary.ReportType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportTypeRepository extends JpaRepository<ReportType,Long>, JpaSpecificationExecutor<ReportType> {
    List<ReportType> findAllByRemovedDateIsNullOrderByName();
}
