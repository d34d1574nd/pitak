package kg.rest.backend.repository;

import kg.rest.backend.entities.Car;
import kg.rest.backend.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long>, JpaSpecificationExecutor<Car> {
    List<Car> findAllByUserAndRemoveDatetimeIsNullOrderByCarBrandAscCarModelAsc(User user);
    Car findCarByIdAndUserIdAndRemoveDatetimeIsNull(Long carId, Long userId);
}
