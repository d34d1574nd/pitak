package kg.rest.backend.security.jwt;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import kg.rest.backend.entities.User;
import kg.rest.backend.enums.UserType;
import kg.rest.backend.security.services.UserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.*;

@Component
public class JwtUtils {
	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	@Value("${transportations.app.jwtSecret}")
	private String jwtSecret;

	@Value("${transportations.app.jwtExpirationMs}")
	private int jwtExpirationMs;

	public String generateJwtToken(Authentication authentication) {

        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
        HashMap<String, Object> claims = new HashMap<>();
        claims.put("userType", userPrincipal.getUserType().name());
        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .claim("userType", userPrincipal.getUserType().name())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    public HashMap<String, Object> getUsernameAndUserTypeFromJWToken(String token) {
        HashMap<String, Object> response = new HashMap<>();
        response.put("username", Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject());
        String userType = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().get("userType").toString();
        response.put("userType", UserType.valueOf(userType));
        return response;
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
			logger.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}
}
