package kg.rest.backend.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Value("${upload.profile.img.path}")
    private String uploadPathProfile;

    @Value("${upload.advert.img.path}")
    private String uploadPathAdvert;

    @Value("${upload.car.img.path}")
    private String uploadPathCar;

    @Value("${img.url.advert}")
    public String ADVERT_IMG_PATH;

    @Value("${img.url.profile}")
    public String PROFILE_IMG_PATH;

    @Value("${img.url.car}")
    public String CAR_IMG_PATH;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(PROFILE_IMG_PATH + "/**")
                .addResourceLocations("file:/"+uploadPathProfile + "/");

        registry.addResourceHandler(ADVERT_IMG_PATH + "/**")
                .addResourceLocations("file:/"+uploadPathAdvert + "/");

        registry.addResourceHandler(CAR_IMG_PATH + "/**")
                .addResourceLocations("file:/"+uploadPathCar + "/");
    }
}
