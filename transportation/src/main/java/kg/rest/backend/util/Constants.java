package kg.rest.backend.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Constants {
    public static String SERVER_PORT;
    public static String HOSTNAME;
    public static String ADVERT_IMG_PATH;
    public static String CAR_IMG_PATH;
    public static String PROFILE_IMG_PATH;

    @Value("${server.port}")
    public void setSERVER_PORT(String SERVER_PORT) {
        this.SERVER_PORT = SERVER_PORT;
    }

    @Value("${custom.server.hostname}")
    public void setHOSTNAME(String HOSTNAME) {
        this.HOSTNAME = HOSTNAME;
    }

    @Value("${img.url.advert}")
    public void setADVERT_IMG_PATH(String ADVERT_IMG_PATH) {
        this.ADVERT_IMG_PATH = ADVERT_IMG_PATH;
    }

    @Value("${img.url.car}")
    public void setCAR_IMG_PATH(String CAR_IMG_PATH) {
        this.CAR_IMG_PATH = CAR_IMG_PATH;
    }

    @Value("${img.url.profile}")
    public void setPROFILE_IMG_PATH(String PROFILE_IMG_PATH) {
        this.PROFILE_IMG_PATH = PROFILE_IMG_PATH;
    }
}
