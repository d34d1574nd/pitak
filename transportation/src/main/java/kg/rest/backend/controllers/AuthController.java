package kg.rest.backend.controllers;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.dto.CarCommonModel;
import kg.rest.backend.entities.ResponseMessage;
import kg.rest.backend.enums.ResultCode;
import kg.rest.backend.enums.UserType;
import kg.rest.backend.payload.request.LoginRequest;
import kg.rest.backend.payload.request.SignupRequest;
import kg.rest.backend.payload.response.JwtResponse;
import kg.rest.backend.repository.RoleRepository;
import kg.rest.backend.repository.UserRepository;
import kg.rest.backend.security.jwt.JwtUtils;
import kg.rest.backend.security.services.UserDetailsImpl;
import kg.rest.backend.service.CarService;
import kg.rest.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	UserService userService;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	CarService carService;

	@PostMapping("/signin")
	public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws Exception {
		try {
			Arrays.asList(new SimpleGrantedAuthority(loginRequest.getUserType().name()));
			List<GrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority(loginRequest.getUserType().name()));

			Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword(), authorities));

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);

			UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
			List<String> roles = userDetails.getAuthorities().stream()
					.map(item -> item.getAuthority())
					.collect(Collectors.toList());
			AppUserModel model = userService.getAppUserByUsernameAndType(userDetails.getUsername(), loginRequest.getUserType());
			if (model == null)
				throw new Exception("пользователь не найден");
			return ResponseEntity.ok(new JwtResponse(jwt,
					roles,
					model));
		}catch (Exception ex){
			throw ex;
		}

	}

	@PostMapping("/signup")
	@Transactional
	public ResponseEntity<ResponseMessage<AppUserModel>> registerUser(@Valid @ModelAttribute SignupRequest signUpRequest) throws TransporationsException {
		String regex = "\\d+";
		if (!signUpRequest.getUsername().matches(regex)) {
			return ResponseEntity
					.badRequest()
					.body(new ResponseMessage(null, ResultCode.FAIL, "Ошибка: Номер должен состоять только из цифр!"));
		}

		if (userRepository.existsByUsernameAndUserType(signUpRequest.getUsername(), signUpRequest.getUserType())) {
			return ResponseEntity
					.badRequest()
					.body(new ResponseMessage(null, ResultCode.FAIL, "Ошибка: Номер уже существует!"));
		}

		/*if (signUpRequest.getEmail() != null && userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new ResponseMessage<>(null, ResultCode.FAIL, "Ошибка: Email уже используется!"));
		}*/

		if(signUpRequest.getUserType().equals(UserType.DRIVER)){
			if(signUpRequest.getCarCommonModel()==null){
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Заполните поля, относящиеся к вашей машине!"));
			}
			CarCommonModel carCommonModel = signUpRequest.getCarCommonModel();

			if(carCommonModel.getCarModel()==null || carCommonModel.getCarBrand()==null
					|| carCommonModel.getCarType()==null || carCommonModel.getCarNumber()==null || carCommonModel.getCarNumber().isEmpty()){
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Заполните поля, относящиеся к вашей машине!"));
			}

			if(carCommonModel.getCarModel().getId()==null || carCommonModel.getCarBrand().getId()==null
					|| carCommonModel.getCarType().getId()==null || carCommonModel.getCarNumber()==null || carCommonModel.getCarNumber().isEmpty()){
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Заполните поля, относящиеся к вашей машине!"));
			}

			if(signUpRequest.getCityModel()==null || signUpRequest.getCityModel().getId()==null)
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Выберите город!"));

			if(signUpRequest.getCountryModel()==null || signUpRequest.getCountryModel().getId()==null)
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Выберите страну!"));
		}

		try {
			AppUserModel model = userService.createNewUser(signUpRequest);

			if(model.getUserType().equals(UserType.DRIVER)){
				if(model.getId()!=null){
					signUpRequest.getCarCommonModel().setUserId(model.getId());
					carService.saveCarCommon(signUpRequest.getCarCommonModel());
				}
				else{
					return ResponseEntity
							.badRequest()
							.body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Пользователь не получил ID!"));
				}
			}

			return ResponseEntity.ok(new ResponseMessage<>(model,ResultCode.SUCCESS,""));
		}
		catch (Exception ex){
			return ResponseEntity.ok(new ResponseMessage<>(null,ResultCode.FAIL,ex.getMessage()));
		}

	}
}
