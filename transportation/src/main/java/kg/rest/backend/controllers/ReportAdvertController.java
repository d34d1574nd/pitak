package kg.rest.backend.controllers;

import kg.rest.backend.dto.ReportAdvertModel;
import kg.rest.backend.entities.ResponseMessage;
import kg.rest.backend.enums.ResultCode;
import kg.rest.backend.payload.request.ReportAdvertRequest;
import kg.rest.backend.service.ReportAdvertService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/report")
public class ReportAdvertController {
    private final ReportAdvertService reportAdvertService;

    public ReportAdvertController(ReportAdvertService reportAdvertService) {
        this.reportAdvertService = reportAdvertService;
    }

    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    @PostMapping(value = "/advert/create")
    ResponseEntity<ResponseMessage<ReportAdvertModel>> createReportAdvert(@Valid @RequestBody ReportAdvertRequest request){
        ReportAdvertModel reportAdvertModel =null;
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL,"Ошибка сохранения жалобы");
        try {
            reportAdvertModel = reportAdvertService.reportForAdvert(request);
            if(reportAdvertModel.getId()!=null){
                responseMessage.setResult(reportAdvertModel);
                responseMessage.setResultCode(ResultCode.SUCCESS);
                responseMessage.setDetails("Ваша жалоба принята!");
            }
        }
        catch (Exception ex){
            responseMessage.setDetails(ex.getMessage());
            responseMessage.setResultCode(ResultCode.EXCEPTION);
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

}
