package kg.rest.backend.controllers;

import kg.rest.backend.dto.*;
import kg.rest.backend.entities.ResponseMessage;
import kg.rest.backend.enums.ResultCode;
import kg.rest.backend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/dictionary")
public class DictionaryController {

    @Autowired
    private CarBrandService carBrandService;

    @Autowired
    private CarModelService carModelService;

    @Autowired
    private CarTypeService carTypeService;

    @Autowired
    private TypeServiceService typeServiceService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CityService cityService;

    @Autowired
    private AgreementSettingService agreementSettingService;

    @Autowired
    private ReportTypeService reportTypeService;

    @GetMapping("/cartype/get/all")
    public ResponseEntity<ResponseMessage<List<CarTypeModel>>> getAllCarType(){
        List<CarTypeModel> carTypeModels = carTypeService.getAll();
        return ResponseEntity.ok(new ResponseMessage<>(carTypeModels,ResultCode.SUCCESS,""));
    }

    @GetMapping("/typeService/get/all")
    public ResponseEntity<ResponseMessage<List<TypeServiceModel>>> getAllTypeService(){
        List<TypeServiceModel> typeServiceModels = typeServiceService.getAll();
        return ResponseEntity.ok(new ResponseMessage<>(typeServiceModels,ResultCode.SUCCESS,""));
    }

    @GetMapping("/carbrand/get/all")
    public ResponseEntity<ResponseMessage<List<CarBrandModel>>> getAllCarBrand(){
        List<CarBrandModel> allBrand = carBrandService.getAll();
        return ResponseEntity.ok(new ResponseMessage<>(allBrand,ResultCode.SUCCESS,""));
    }

    @GetMapping("/carmodel/get/byParent")
    public ResponseEntity<ResponseMessage<List<CarModelModel>>> getAllByCarBrand(@RequestParam Long parentId){
        List<CarModelModel> carModelModelList = carModelService.getAllByParent(parentId);
        return ResponseEntity.ok(new ResponseMessage<>(carModelModelList,ResultCode.SUCCESS,""));
    }

    @GetMapping("/country/get/all")
    public ResponseEntity<ResponseMessage<List<CountryModel>>> getAllCountry(){
        List<CountryModel> countryModelList= countryService.getAllCountry();
        return ResponseEntity.ok(new ResponseMessage<>(countryModelList,ResultCode.SUCCESS,""));
    }

    @GetMapping("/city/get/byParent")
    public ResponseEntity<ResponseMessage<List<CityModel>>> getAllCityByCountry(@RequestParam Long parentId){
        List<CityModel> cityModels = cityService.getByParent(parentId);
        return ResponseEntity.ok(new ResponseMessage<>(cityModels,ResultCode.SUCCESS,""));
    }

    @GetMapping("/agreement/html/get")
    public ResponseEntity<ResponseMessage<AgreementSettingModel>> getAgreementHtml(){
        AgreementSettingModel agreementSettingModel = agreementSettingService.getFirstAgreement();
        return ResponseEntity.ok(new ResponseMessage<>(agreementSettingModel,ResultCode.SUCCESS,""));
    }

    @GetMapping("/reporttype/get/all")
    public ResponseEntity<ResponseMessage<List<ReportTypeModel>>> getReportTypeList(){
        return ResponseEntity.ok(new ResponseMessage<>(reportTypeService.getAllActiveReportType(),ResultCode.SUCCESS,""));
    }
}
