package kg.rest.backend.controllers;

import kg.rest.backend.dto.AppAdvertAttachmentModel;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.entities.Favorites;
import kg.rest.backend.entities.ResponseMessage;
import kg.rest.backend.entities.dictionary.TypeService;
import kg.rest.backend.enums.ResultCode;
import kg.rest.backend.payload.request.AdvertSearchPatternRequest;
import kg.rest.backend.payload.request.AdvertWrapperRequest;
import kg.rest.backend.payload.request.FileMultiUploadRequest;
import kg.rest.backend.payload.response.AdvertWrapperResponse;
import kg.rest.backend.service.AdvertAttachmentService;
import kg.rest.backend.service.AppAdvertService;
import kg.rest.backend.service.TypeServiceService;
import kg.rest.backend.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/advert")
public class AppAdvertController {
    private final AppAdvertService appAdvertService;
    private final TypeServiceService typeServiceService;
    private final AdvertAttachmentService attachmentService;
    private final UserService userService;

    public AppAdvertController(AppAdvertService appAdvertService, TypeServiceService typeServiceService, AdvertAttachmentService attachmentService, UserService userService) {
        this.appAdvertService = appAdvertService;
        this.typeServiceService = typeServiceService;
        this.attachmentService = attachmentService;
        this.userService = userService;
    }

    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    @PostMapping(value = "/save")
    ResponseEntity<ResponseMessage<AdvertWrapperResponse>> saveAdvert(@Valid @ModelAttribute AdvertWrapperRequest request) {
        AdvertWrapperResponse response = null;
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка сохранения");
        try {
            response = appAdvertService.save(request);
            if (response.getAppAdvertModel().getId() != null) {
                responseMessage.setResult(response);
                responseMessage.setResultCode(ResultCode.SUCCESS);
                responseMessage.setDetails("Успешно");
            }
        } catch (Exception ex) {
            responseMessage.setDetails(ex.getMessage());
            responseMessage.setResultCode(ResultCode.EXCEPTION);
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    @GetMapping(value = "/get/{advertId}")
    ResponseEntity<ResponseMessage<AdvertWrapperResponse>> getAdvertById(@PathVariable Long advertId) {
        if (!appAdvertService.existEnabledAdvert(advertId)) {
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseMessage<>(null, ResultCode.FAIL, "Ошибка: Объявление не найдено!"));
        }

        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибку получения объявления");
        try {
            AdvertWrapperResponse wrapperResponse = appAdvertService.getById(advertId);
            responseMessage.setResultCode(ResultCode.SUCCESS);
            responseMessage.setDetails("Успешно");
            responseMessage.setResult(wrapperResponse);
        }catch (Exception ex) {
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseMessage<>(null, ResultCode.EXCEPTION, ex.getMessage()));
        }

        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('PASSENGER') or hasRole('DRIVER')")
    @GetMapping(value = "/get/myadverts")
    ResponseEntity<ResponseMessage<Page<AdvertWrapperResponse>>> getMyAdverts(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка получения списка моих объявлений");
        try {
            AdvertSearchPatternRequest patternRequest = new AdvertSearchPatternRequest();
            patternRequest.setByCreatedUserId(userService.getCurrentUser().get().getId());
            patternRequest.setIsNullRemoveDate(true);
            Page<AdvertWrapperResponse> responses = appAdvertService.search(patternRequest, pageable);
            responseMessage.setResult(responses);
            responseMessage.setResultCode(ResultCode.SUCCESS);
            responseMessage.setDetails("Успешно");
        } catch (Exception ex) {
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('PASSENGER') or hasRole('DRIVER')")
    @GetMapping(value = "/get/myadvert/{id}")
    ResponseEntity<ResponseMessage<AdvertWrapperResponse>> getMyAdvertById(@PathVariable("id") Long advertId) {
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка получения объявлений");
        try {
            if (!appAdvertService.existAdvert(advertId)) {
                return ResponseEntity
                        .badRequest()
                        .body(new ResponseMessage<>(null, ResultCode.FAIL, "Ошибка: Объявление не найдено!"));
            }

            if (appAdvertService.isDeletedAd(advertId)) {
                return ResponseEntity
                        .badRequest()
                        .body(new ResponseMessage<>(null, ResultCode.FAIL, "Ошибка: Объявление удалено уже!"));
            }

            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            if (currentUser.isPresent()) {
                AdvertWrapperResponse response = appAdvertService.getByIdAndUserId(advertId, currentUser.get().getId());
                responseMessage.setResult(response);
                responseMessage.setResultCode(ResultCode.SUCCESS);
                responseMessage.setDetails("Успешно");
            } else {
                responseMessage.setDetails("Текущий пользователь не найден.");
            }
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('PASSENGER') or hasRole('DRIVER')")
    @DeleteMapping(value = "/remove/myadvert/{id}")
    ResponseEntity<ResponseMessage<AdvertWrapperResponse>> removeMyAdvert(@PathVariable("id") Long advertId) {
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка удаления объявления");
        try {
            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            if (currentUser.isPresent()) {
                AdvertWrapperResponse advertModel = appAdvertService.removeByIdAndUserId(advertId, currentUser.get().getId());
                responseMessage.setResult(advertModel);
                responseMessage.setResultCode(ResultCode.SUCCESS);
                responseMessage.setDetails("Успешно");
            } else {
                responseMessage.setDetails("Текущий пользователь не найден.");
            }
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('PASSENGER')")
    @PostMapping(value = "/driver/search")
    ResponseEntity<ResponseMessage<Page<AdvertWrapperResponse>>> searchDriverAdvert(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
                                                                                    @RequestBody AdvertSearchPatternRequest searchPatternRequest) {
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка поиска объявлений водителей");
        try {

            if (searchPatternRequest.getType() == null) {
                List<TypeService> typeServices = typeServiceService.getEntitiesByWithOutId(1l);
                searchPatternRequest.setType(typeServices);
            }
            searchPatternRequest.setEnabled(true);
            Page<AdvertWrapperResponse> advertModelList = appAdvertService.search(searchPatternRequest, pageable);
            responseMessage.setResult(advertModelList);
            responseMessage.setResultCode(ResultCode.SUCCESS);
            responseMessage.setDetails("Успешно");
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PostMapping(value = "/all/adverts")
    ResponseEntity<ResponseMessage<Page<AdvertWrapperResponse>>> activeAdverts(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable, @RequestBody AdvertSearchPatternRequest searchPatternRequest) {
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка поиска объявлений");
        try {
            searchPatternRequest.setEnabled(true);
            Page<AdvertWrapperResponse> responses = appAdvertService.activeAdverts(searchPatternRequest, pageable);
            responseMessage.setResult(responses);
            responseMessage.setResultCode(ResultCode.SUCCESS);
            responseMessage.setDetails("Успешно");
        } catch (Exception ex) {
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DRIVER')")
    @PostMapping(value = "/passenger/search")
    ResponseEntity<ResponseMessage<Page<AdvertWrapperResponse>>> searchPassengerAdvert(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
                                                                                       @RequestBody AdvertSearchPatternRequest searchPatternRequest) {
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка поиска объявлений клиентов");
        try {
            if (searchPatternRequest.getType() == null) {
                List<TypeService> typeServices = typeServiceService.getEntitiesByWithOutId(2l);
                searchPatternRequest.setType(typeServices);
            }
            searchPatternRequest.setEnabled(true);
            Page<AdvertWrapperResponse> advertModelList = appAdvertService.search(searchPatternRequest, pageable);
            responseMessage.setResult(advertModelList);
            responseMessage.setResultCode(ResultCode.SUCCESS);
            responseMessage.setDetails("Успешно");
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }

        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @DeleteMapping("/myadvert/photo")
    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    public ResponseEntity<ResponseMessage<Boolean>> removeAdvertPhoto(@RequestParam Long advertId, @RequestParam Long fileId){
        try {
            return ResponseEntity
                    .ok(new ResponseMessage<>(attachmentService.deleteByMyAdvert(advertId, fileId), ResultCode.SUCCESS, ""));
        }
        catch (Exception ex){
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ResponseMessage<>(false,ResultCode.EXCEPTION,ex.getMessage()));
        }
    }

    @PutMapping("/myadvert/photo/{advertId}")
    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    public ResponseEntity<ResponseMessage<List<AppAdvertAttachmentModel>>> updateAdvertPhoto(@Valid @ModelAttribute FileMultiUploadRequest profilePhoto, @PathVariable Long advertId) {
        try {
            return ResponseEntity
                    .ok(new ResponseMessage<>(attachmentService.addMyAdvertPhoto(profilePhoto, advertId), ResultCode.SUCCESS, ""));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ResponseMessage<>(null, ResultCode.EXCEPTION, ex.getMessage()));
        }
    }

    @PreAuthorize("hasRole('PASSENGER') or hasRole('DRIVER')")
    @GetMapping(value = "/similar/ads")
    ResponseEntity<ResponseMessage<Page<AdvertWrapperResponse>>> searchSimilarAdvert(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
                                                                                     @RequestParam(value = "advert_id") @NotBlank Long advertId) {
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка получения списка объявления!");
        try {
            Page<AdvertWrapperResponse> advertModelList = appAdvertService.getSimilarAds(advertId, pageable);
            responseMessage.setResult(advertModelList);
            responseMessage.setResultCode(ResultCode.SUCCESS);
            responseMessage.setDetails("Успешно");
        } catch (Exception ex) {
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }

        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    @PostMapping(value = "/favourite/{advertId}")
    ResponseEntity<ResponseMessage<Favorites>>  createFavourite(@PathVariable Long advertId){
        if (!appAdvertService.existEnabledAdvert(advertId)) {
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseMessage(null,ResultCode.FAIL,"Ошибка: Объявление не найдено!"));
        }
        ResponseMessage responseMessage  = new ResponseMessage(null,ResultCode.FAIL,"Ошибка добавления в избранное");
        try {
           Favorites favorites = appAdvertService.createFavouriteAdvert(advertId);
           if(favorites!=null){
               responseMessage.setResult(favorites);
               responseMessage.setResultCode(ResultCode.SUCCESS);
               responseMessage.setDetails("Успешно");
           }
           else{
               responseMessage.setResultCode(ResultCode.FAIL);
               responseMessage.setDetails("Уже добавлено в избранное!");
           }
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    @DeleteMapping(value = "/favourite/{advertId}")
    ResponseEntity<ResponseMessage<Boolean>> deleteFavourite(@PathVariable Long advertId){
        if (!appAdvertService.existEnabledAdvert(advertId)) {
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Объявление не найдено!"));
        }
        ResponseMessage responseMessage = new ResponseMessage(null,ResultCode.FAIL,"Ошибка: удаления из избранных!");
        try{
            Boolean deleted = appAdvertService.deleteFavouriteAdvert(advertId);
            if(deleted){
                responseMessage.setResultCode(ResultCode.SUCCESS);
                responseMessage.setResult(deleted);
                responseMessage.setDetails("Успешно");
            }
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    @GetMapping(value = "/favourite/get/list")
    ResponseEntity<ResponseMessage<List<AdvertWrapperResponse>>> getFavourites() {
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка получения списка избранных");
        try {
            List<AdvertWrapperResponse> list = appAdvertService.getFavouriteAdvertList();
            responseMessage.setResult(list);
            responseMessage.setResultCode(ResultCode.SUCCESS);
            responseMessage.setDetails("Успешно");
        } catch (Exception ex) {
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('PASSENGER') or hasRole('DRIVER')")
    @GetMapping(value = "/favourite/get/my/byTitle")
    ResponseEntity<ResponseMessage<Page<AdvertWrapperResponse>>> searchFavouriteAdvert(@RequestParam(value = "title") String title) {
        ResponseMessage responseMessage = new ResponseMessage(null, ResultCode.FAIL, "Ошибка получения списка избранных объявлений!");
        try {
            List<AdvertWrapperResponse> advertModelList = appAdvertService.getMyFavouriteByTitle(title);
            responseMessage.setResult(advertModelList);
            responseMessage.setResultCode(ResultCode.SUCCESS);
            responseMessage.setDetails("Успешно");
        } catch (Exception ex) {
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }

        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

}
