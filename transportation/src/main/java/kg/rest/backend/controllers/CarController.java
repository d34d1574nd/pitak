package kg.rest.backend.controllers;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.dto.CarAttachmentModel;
import kg.rest.backend.dto.CarCommonModel;
import kg.rest.backend.entities.ResponseMessage;
import kg.rest.backend.entities.User;
import kg.rest.backend.enums.ResultCode;
import kg.rest.backend.payload.request.CarWrapperRequest;
import kg.rest.backend.payload.request.FileMultiUploadRequest;
import kg.rest.backend.payload.response.CarWrapperResponse;
import kg.rest.backend.repository.UserRepository;
import kg.rest.backend.service.CarAttachmentService;
import kg.rest.backend.service.CarService;
import kg.rest.backend.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/car")
public class CarController {
    private final CarService carService;
    private final UserService userService;
    private final UserRepository userRepository;
    private final CarAttachmentService attachmentService;

    public CarController(CarService carService, UserService userService, UserRepository userRepository, CarAttachmentService attachmentService) {
        this.carService = carService;
        this.userService = userService;
        this.userRepository = userRepository;
        this.attachmentService = attachmentService;
    }

    @PreAuthorize("hasRole('DRIVER')")
    @PostMapping(value = "/create")
    ResponseEntity<ResponseMessage<CarCommonModel>> createCar(@Valid @ModelAttribute CarWrapperRequest carWrapperRequest){
        CarWrapperResponse carWrapperResponse = null;
        ResponseMessage responseMessage = new ResponseMessage(null,ResultCode.FAIL,"Ошибка сохранения");
        try {
            carWrapperResponse = carService.create(carWrapperRequest);
            if(carWrapperResponse!=null){
                responseMessage.setResult(carWrapperResponse);
                responseMessage.setResultCode(ResultCode.SUCCESS);
                responseMessage.setDetails("Успешно");
            }
        }
        catch (Exception ex){
            responseMessage.setDetails(ex.getMessage());
            responseMessage.setResultCode(ResultCode.EXCEPTION);
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DRIVER')")
    @PostMapping("/edit")
    public ResponseEntity editCar(@Valid @ModelAttribute CarWrapperRequest carWrapperRequest) {
        ResponseMessage responseMessage = new ResponseMessage(null,ResultCode.FAIL,"Ошибка");

        CarCommonModel carCommonModel = carWrapperRequest.getCarCommonModel();
        if(carCommonModel.getCarType()==null || carCommonModel.getCarBrand()==null || carCommonModel.getCarModel()==null ||
                carCommonModel.getCarType()==null || carCommonModel.getCarNumber().isEmpty()){
            return ResponseEntity.badRequest()
                    .body(new ResponseMessage<>(null,ResultCode.FAIL,"Ошибка: Марка,тип, номер и модель машины не может быть пустым!"));
        }
        try {
            CarWrapperResponse carWrapperResponse = carService.editCar(carWrapperRequest);
            responseMessage.setResult(carWrapperResponse);
            responseMessage.setResultCode(ResultCode.EXCEPTION);
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }

        return ResponseEntity.ok(responseMessage);
    }

    @PreAuthorize("hasRole('DRIVER')")
    @DeleteMapping("/remove")
    public ResponseEntity removeCar(@RequestParam Long carId) {
        ResponseMessage responseMessage = new ResponseMessage(null,ResultCode.FAIL,"Ошибка");
        try {
            CarWrapperResponse model = carService.removeCar(carId);
            responseMessage.setResult(model);
            responseMessage.setResultCode(ResultCode.EXCEPTION);
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }

        return ResponseEntity.ok(responseMessage);
    }

    @DeleteMapping("/remove/photo")
    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    public ResponseEntity<ResponseMessage<Boolean>> removeCarPhoto(@RequestParam Long carId, @RequestParam Long fileId){
        try {
            return ResponseEntity
                    .ok(new ResponseMessage<>(attachmentService.deleteByMyCar(carId, fileId), ResultCode.SUCCESS, ""));
        }
        catch (Exception ex){
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ResponseMessage<>(false,ResultCode.EXCEPTION,ex.getMessage()));
        }
    }

    @PutMapping("/update/photo/{carId}")
    @PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
    public ResponseEntity<ResponseMessage<List<CarAttachmentModel>>> updateCarPhoto(@Valid
           @ModelAttribute FileMultiUploadRequest carPhoto, @PathVariable Long carId) {
        try {
            return ResponseEntity
                    .ok(new ResponseMessage<>(attachmentService.addMyCarPhoto(carPhoto, carId), ResultCode.SUCCESS, ""));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ResponseMessage<>(null, ResultCode.EXCEPTION, ex.getMessage()));
        }
    }

    @PreAuthorize("hasRole('DRIVER')")
    @GetMapping(value = "/mycars")
    ResponseEntity<ResponseMessage<CarCommonModel>> getMyCars(){
        ResponseMessage responseMessage = new ResponseMessage(null,ResultCode.FAIL,"Ошибка получения списка моих объявлений");
        try {
            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            if (!currentUser.isPresent())
                throw new TransporationsException("Пользователь не найден");

            Optional<User> user = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());
            if (user.isPresent()) {
                List<CarWrapperResponse> carWrapperResponses = carService.getCarCommonModels(user.get());
                responseMessage.setResult(carWrapperResponses);
                responseMessage.setResultCode(ResultCode.SUCCESS);
                responseMessage.setDetails("Успешно");
            } else {
                responseMessage.setDetails("Текущий пользователь не найден.");
            }
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
            responseMessage.setDetails(ex.getMessage());
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DRIVER')")
    @GetMapping(value = "/mycar")
    ResponseEntity<ResponseMessage<CarCommonModel>> getMyCarById(@RequestParam Long id){
        ResponseMessage responseMessage = new ResponseMessage(null,ResultCode.FAIL,"Ошибка получения машины");
        try {
            Optional<AppUserModel> currentUser = userService.getCurrentUser();
            if (!currentUser.isPresent())
                throw new TransporationsException("Пользователь не найден");
            Optional<User> user = userRepository.findByUsernameAndUserType(currentUser.get().getUsername(), currentUser.get().getUserType());
            if (user.isPresent()) {
                CarWrapperResponse carWrapperResponse = carService.getMyCar(id, user.get().getId());
                responseMessage.setResult(carWrapperResponse);
                responseMessage.setResultCode(ResultCode.SUCCESS);
                responseMessage.setDetails("Успешно");
            } else {
                responseMessage.setDetails("Текущий пользователь не найден.");
            }
        }catch (Exception ex){
            responseMessage.setResultCode(ResultCode.EXCEPTION);
        }
        return new ResponseEntity(responseMessage, HttpStatus.OK);
    }
}
