package kg.rest.backend.controllers;

import kg.rest.backend.TransporationsException;
import kg.rest.backend.dto.AppFileModel;
import kg.rest.backend.dto.AppUserModel;
import kg.rest.backend.dto.EditUserModel;
import kg.rest.backend.entities.ResponseMessage;
import kg.rest.backend.enums.ResultCode;
import kg.rest.backend.payload.request.FileUploadRequest;
import kg.rest.backend.repository.RoleRepository;
import kg.rest.backend.repository.UserRepository;
import kg.rest.backend.security.jwt.JwtUtils;
import kg.rest.backend.service.CarService;
import kg.rest.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UserController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	UserService userService;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	CarService carService;

	@GetMapping("/me")
	@PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
	public ResponseEntity<ResponseMessage<AppUserModel>> me() {
		ResponseMessage responseMessage = new ResponseMessage(null,ResultCode.FAIL,"Ошибка");
		try {
			Optional<AppUserModel> user = userService.getCurrentUser();
			if(user.isPresent()){
				responseMessage.setResult(user.get());
				responseMessage.setResultCode(ResultCode.SUCCESS);
				responseMessage.setDetails("");
			}else{
				responseMessage.setResultCode(ResultCode.FAIL);
				responseMessage.setDetails("Пользователь не найден");
			}
		}
		catch (Exception ex){
			responseMessage.setResultCode(ResultCode.EXCEPTION);
			responseMessage.setDetails(ex.getMessage());
		}

		return ResponseEntity.ok(responseMessage);
	}

	@PostMapping("/driver/edit")
	@PreAuthorize("hasRole('DRIVER')")
	public ResponseEntity<ResponseMessage<AppUserModel>> editDriver(@Valid @RequestBody EditUserModel editUserModel) throws TransporationsException {
		ResponseMessage responseMessage = new ResponseMessage(null,ResultCode.FAIL,"Ошибка");
			ResponseEntity hasError = validate(editUserModel);
			if(hasError!=null)
				return hasError;

			if(editUserModel.getCarCommonModel()==null ||
					editUserModel.getCarCommonModel().getCarBrand()==null ||
					editUserModel.getCarCommonModel().getCarModel()==null ||
					editUserModel.getCarCommonModel().getCarType()==null ||
					editUserModel.getCarCommonModel().getCarNumber().isEmpty()

			){
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null,ResultCode.FAIL,"Ошибка: Марка,тип, номер и модель машины не может быть пустым!"));
			}

			if(editUserModel.getCarCommonModel()==null ||
					editUserModel.getCarCommonModel().getCarBrand().getId()==null ||
					editUserModel.getCarCommonModel().getCarModel().getId()==null ||
					editUserModel.getCarCommonModel().getCarType().getId()==null ||
					editUserModel.getCarCommonModel().getCarNumber().isEmpty()
			){
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null,ResultCode.FAIL,"Ошибка: Марка,тип, номер и модель машины не может быть пустым!"));
			}

			if(editUserModel.getCountryModel()==null || editUserModel.getCountryModel().getId()==null)
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Выберите страну!"));

			if(editUserModel.getCityModel()==null || editUserModel.getCityModel().getId()==null)
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Выберите город!"));


			try {
				AppUserModel model = userService.editUser(editUserModel);
				if(model.getId()!=null){
					carService.saveCarCommon(editUserModel.getCarCommonModel());
				}
				else{
					return ResponseEntity
							.badRequest()
							.body(new ResponseMessage<>(null, ResultCode.FAIL,"Ошибка: Получить ID пользователя не удалось!"));
				}

				responseMessage.setResult(model);
				responseMessage.setResultCode(ResultCode.EXCEPTION);
			}catch (Exception ex){
				responseMessage.setResultCode(ResultCode.EXCEPTION);
				responseMessage.setDetails(ex.getMessage());
			}

		return ResponseEntity.ok(responseMessage);
	}

	@PostMapping("/passenger/edit")
	@PreAuthorize("hasRole('PASSENGER')")
	public ResponseEntity<ResponseMessage<AppUserModel>> editPassenger(@Valid @RequestBody EditUserModel editUserModel) throws TransporationsException {
		ResponseMessage responseMessage = new ResponseMessage(null,ResultCode.FAIL,"Ошибка");
		try {
			ResponseEntity hasError = validate(editUserModel);
			if(hasError!=null)
				return hasError;

			AppUserModel model = userService.editUser(editUserModel);
			responseMessage.setResultCode(ResultCode.SUCCESS);
			responseMessage.setResult(model);
			responseMessage.setDetails("");

		}
		catch (Exception ex){
			responseMessage.setResultCode(ResultCode.EXCEPTION);
			responseMessage.setDetails(ex.getMessage());
		}

		return ResponseEntity.ok(responseMessage);
	}

	@PostMapping("/remove/profile/photo")
	@PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
	public ResponseEntity<ResponseMessage<Boolean>> removeProfilePhoto(){
		try {
			return ResponseEntity
					.ok(new ResponseMessage<>(userService.removeProfilePhoto(),ResultCode.SUCCESS,""));
		}
		catch (Exception ex){
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseMessage<>(false,ResultCode.EXCEPTION,ex.getMessage()));
		}
	}

	@PostMapping("/upload/profile/photo")
	@PreAuthorize("hasRole('DRIVER') or hasRole('PASSENGER')")
	public ResponseEntity<ResponseMessage<AppFileModel>> updateProfilePhoto(@Valid @ModelAttribute FileUploadRequest profilePhoto){
		try {
			AppFileModel updated = userService.updateProfilePhoto(profilePhoto);
			if(updated==null){
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage(null,ResultCode.FAIL,"Ошибка: Фотографию не удалось загрузить"));
			}

			return ResponseEntity
					.ok()
					.body(new ResponseMessage<>(updated,ResultCode.SUCCESS,""));
		}catch (Exception ex){
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseMessage(null,ResultCode.EXCEPTION,ex.getMessage()));
		}
	}

	private ResponseEntity validate(EditUserModel editUserModel){
		Optional<AppUserModel> user = userService.getCurrentUser();
		String regex = "\\d+";

		if(!editUserModel.getUsername().matches(regex)){
			return ResponseEntity
					.badRequest()
					.body(new ResponseMessage(null,ResultCode.FAIL,"Ошибка: Номер должен состоять только из цифр!"));
		}

		if(user.isPresent()){
			AppUserModel currentUser = user.get();
			if(!currentUser.getUsername().equals(editUserModel.getUsername())) {
                if (userRepository.existsByUsernameAndUserType(editUserModel.getUsername(), editUserModel.getUserType())) {
                    return ResponseEntity
                            .badRequest()
                            .body(new ResponseMessage<>(null, ResultCode.FAIL, "Error: Номер уже существует!"));
                }
            }
		}

        if(editUserModel.getNewPassword()!=null && !editUserModel.getNewPassword().trim().isEmpty()){
			if (editUserModel.getNewPassword().length()<=6){
				return ResponseEntity
						.badRequest()
						.body(new ResponseMessage<>(null,ResultCode.FAIL,"Error: Пароль должен быть больше 6 символов!"));
			}
		}

		return null;
	}
}
