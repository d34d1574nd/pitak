create table dict_car_type
(
	id bigserial not null,
	name varchar
);

create unique index dict_car_type_id_uindex
	on dict_car_type (id);

alter table dict_car_type
	add constraint dict_car_type_pk
		primary key (id);

