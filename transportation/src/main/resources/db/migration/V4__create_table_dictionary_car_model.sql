create table dict_car_model
(
    id bigserial not null,
    parent_id bigint not null
        constraint dict_car_model_dict_car_brand_id_fk
            references dict_car_brand,
    name varchar(100)
);

create unique index dict_car_model_id_uindex
    on dict_car_model (id);

alter table dict_car_model
    add constraint dict_car_model_pk
        primary key (id);

