create table dict_car_brand
(
	id bigserial not null,
	name varchar(100)
);

create unique index dict_car_brand_id_uindex
	on dict_car_brand (id);

alter table dict_car_brand
	add constraint dict_car_brand_pk
		primary key (id);

